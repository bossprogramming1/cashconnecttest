export type colorOptions =
  | "primary"
  | "secondary"
  | "warn"
  | "error"
  | "auto"
  | "inherit";

export type deviceOptions = "xs" | "sm" | "md" | "lg";

export type shadowOptions =
  | "small"
  | "regular"
  | "large"
  | "badge"
  | "border"
  | "none";
