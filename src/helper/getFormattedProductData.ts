import { BASE_URL } from "@data/constants";
import _ from "lodash";
function getFormattedProductData(productData = [], type?: string) {
  console.log("dicountdsdftype", type, productData);
  if (_.isArray(productData)) {
    var formattedProductData: any[] = productData?.map((data) => {
      let productUrl = {};
      if (type === "Arrivals") {
        productUrl = { productUrl: `/product/${data?.id}` };
      } else if (type === "TopRated") {
        productUrl = { productUrl: `/product/${data?.id}` };
      } else if (type === "bigdiscount") {
        productUrl = { productUrl: `/product/${data?.id}` };
      } else if (type === "FeaturedBrands") {
        productUrl = { productUrl: `/product/${data?.id}` };
      }

      // if (type === "bigdiscount") {
      //   return {
      //     ...productUrl,
      //     orginalPrice: Number(data?.product?.unit_price) || null,
      //     id: data?.product?.id || null,
      //     price: Number(data?.discounted_price) || null,
      //     title: data?.product?.name,
      //     imgUrl: `${
      //       data?.product?.thumbnail
      //         ? `${BASE_URL}${data?.product?.thumbnail}`
      //         : data?.product?.image
      //         ? `${BASE_URL}${data?.product?.image}`
      //         : ""
      //     }`,
      //     // imgUrl: `${BASE_URL}${data?.product?.thumbnail || data?.product?.image || ""}`,
      //     category: data?.product?.category || null,
      //     brand: _.isObject(data?.product?.brand)
      //       ? data?.product?.brand?.name
      //       : data?.product?.brand || null,
      //     rating: data?.product?.rating || null,
      //     reviewCount: data?.product?.num_reviews || null,
      //     stock: data?.product?.is_in_stock === true ? true : false,
      //     condition:
      //       data?.condition === "used" ||
      //       data?.condition === "Used" ||
      //       data?.condition === "USED"
      //         ? "pre owned"
      //         : data?.condition
      //         ? data.condition
      //         : null,
      //     offPercent: data?.discount_percent || null,
      //     offValue: data?.discount_value || null,
      //   };
      // }
      if (data?.discounted_price && type !== "bigdiscount") {
        return {
          ...productUrl,
          orginalPrice: Number(data?.unit_price) || null,
          id: data?.id || null,
          price: Number(data?.discounted_price) || null,
          title: data?.name,
          imgUrl: `${
            data?.thumbnail
              ? `${BASE_URL}${data?.thumbnail}`
              : data?.image
              ? `${BASE_URL}${data?.image}`
              : ""
          }`,
          // imgUrl: `${BASE_URL}${data?.thumbnail || data?.image || ""}`,
          category: data?.category || null,
          brand: _.isObject(data?.brand)
            ? data?.brand?.name
            : data?.brand || null,
          rating: data?.rating || null,
          reviewCount: data?.num_reviews || null,
          stock: data?.is_in_stock === true ? true : false,
          condition:
            data?.condition == "used" ||
            data?.condition == "Used" ||
            data?.condition == "USED"
              ? "pre owned"
              : data?.condition
              ? data.condition
              : null,
          offPercent: data?.discount_percent || null,
          offValue: data?.discount_value || null,
        };
      } else if (data?.discounted_price && type === "bigdiscount") {
        return {
          ...productUrl,
          orginalPrice: Number(data?.product?.unit_price) || null,
          id: data?.product?.id || null,
          price: Number(data?.discounted_price) || null,
          title: data?.product?.name,
          imgUrl: `${
            data?.product?.thumbnail
              ? `${BASE_URL}${data?.product?.thumbnail}`
              : data?.product?.image
              ? `${BASE_URL}${data?.product?.image}`
              : ""
          }`,
          // imgUrl: `${BASE_URL}${data?.product?.thumbnail || data?.product?.image || ""}`,
          category: data?.product?.category || null,
          brand: _.isObject(data?.product?.brand)
            ? data?.product?.brand?.name
            : data?.product?.brand || null,
          rating: data?.product?.rating || null,
          reviewCount: data?.product?.num_reviews || null,
          stock: data?.product?.is_in_stock === true ? true : false,
          condition:
            data?.condition == "used" ||
            data?.condition == "Used" ||
            data?.condition == "USED"
              ? "pre owned"
              : data?.condition
              ? data.condition
              : null,
          offPercent: data?.discount_percent || null,
          offValue: data?.discount_value || null,
        };
      } else {
        return {
          ...productUrl,
          id: data?.id || null,
          price: Number(data?.unit_price) || null,
          title: data?.name,
          imgUrl: `${
            data?.thumbnail
              ? `${BASE_URL}${data?.thumbnail}`
              : data?.image
              ? `${BASE_URL}${data?.image}`
              : ""
          }`,
          // imgUrl: `${BASE_URL}${data?.thumbnail || data?.image || ""}`,
          category: data?.category || null,
          stock: data?.is_in_stock
            ? data?.is_in_stock
            : data?.is_in_stock
            ? data?.is_in_stock
            : false,
          brand: _.isObject(data?.brand)
            ? data?.brand?.name
            : data?.brand || null,
          rating: data?.rating || null,
          reviewCount: data?.num_reviews || null,
          offPercent: null,
          offValue: null,
          condition:
            data?.condition == "used" ||
            data?.condition == "Used" ||
            data?.condition == "USED"
              ? "pre owned"
              : data?.condition
              ? data.condition
              : null,
        };
      }
    });
  } else {
    var formattedProductData = [];
  }

  return formattedProductData;
}

export default getFormattedProductData;
