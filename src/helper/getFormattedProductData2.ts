import { BASE_URL } from "@data/constants";
import _ from "lodash";
function getFormattedProductData2(productData = []) {
  if (_.isArray(productData)) {
    var formattedProductData: any[] = productData?.map((data) => {
      let productUrl = {};

      productUrl = { productUrl: `/product/${data?.id}` };

      return {
        ...productUrl,
        orginalPrice: Number(data?.unit_price) || null,
        id: data?.id || null,
        price: Number(data?.discounted_price) || null,
        title: data?.name,
        imgUrl: `${
          data?.thumbnail
            ? `${BASE_URL}${data?.thumbnail}`
            : data?.image
            ? `${BASE_URL}${data?.image}`
            : ""
        }`,
        // imgUrl: `${BASE_URL}${data?.thumbnail || data?.image || ""}`,
        category: data?.category || null,
        brand: _.isObject(data?.brand)
          ? data?.brand?.name
          : data?.brand || null,
        rating: data?.rating || null,
        reviewCount: data?.num_reviews || null,
        stock: data?.is_in_stock === true ? true : false,
        condition:
          data?.condition == "used" ||
          data?.condition == "Used" ||
          data?.condition == "USED"
            ? "pre owned"
            : data?.condition
            ? data.condition
            : null,
        offPercent: data?.discount_percent || null,
        offValue: data?.discount_value || null,
      };
    });
  } else {
    var formattedProductData = [];
  }

  return formattedProductData;
}

export default getFormattedProductData2;
