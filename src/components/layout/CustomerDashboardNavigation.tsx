import Box from "@component/Box";
import { useAppContext } from "@context/app/AppContext";
import useWindowSize from "@hook/useWindowSize";
import { useRouter } from "next/router";
import React, { Fragment } from "react";
import FlexBox from "../FlexBox";
import Icon from "../icon/Icon";
import { H1, H5 } from "../Typography";
import {
  DashboardNavigationWrapper,
  StyledDashboardNav,
} from "./DashboardStyle";
import useUserInf from "@customHook/useUserInf";

type CustomerDashboardNavigationProps = {
  toggleSidenav?: () => void;
};

const CustomerDashboardNavigation: React.FC<
  CustomerDashboardNavigationProps
> = ({ toggleSidenav }) => {
  const { pathname } = useRouter();
  const { dispatch } = useAppContext();

  const width = useWindowSize();
  const isMobile = width < 769;
  const { user_name, isLogin } = useUserInf();

  return (
    <DashboardNavigationWrapper px="0px" pb="1.5rem" color="gray.900">
      {linkList.map((item) => (
        <Fragment key={item.id}>
          <H1
            p="26px 30px 1rem"
            color={item.title === "DASHBOARD" ? "gray.900" : "text.muted"}
            fontSize={item.title === "DASHBOARD" ? "16px" : "14px"}
          >
            {item.title === "DASHBOARD" ? user_name?.toUpperCase() : item.title}
          </H1>
          {item.list.map((item) => {
            return !isMobile && item.title === "Logout" ? null : (
              <StyledDashboardNav
                isCurrentPath={pathname.includes(item.href)}
                href={isLogin ? item.href : "/login"}
                key={item.id}
                px="1.5rem"
                mb="1.25rem"
                onClick={() => {
                  toggleSidenav();
                  if (item.title === "Logout") {
                    dispatch({
                      type: "CHANGE_ALERT",
                      payload: {
                        alertValue: "logout success...",
                        alerType: "successLogout",
                      },
                    });
                    localStorage.removeItem("UserId");
                    localStorage.removeItem("jwt_access_token");
                    sessionStorage.removeItem("fbssls_5515163185212209");
                    localStorage.removeItem("OrderId");
                    localStorage.removeItem("userType");
                  }
                }}
              >
                <FlexBox alignItems="center">
                  <Box className="dashboard-nav-icon-holder">
                    <Icon variant="small" defaultcolor="currentColor" mr="10px">
                      {item.iconName}
                      {/* 5555 */}
                    </Icon>
                  </Box>

                  <H5 className="dashboard-nav-title">{item.title}</H5>
                </FlexBox>
              </StyledDashboardNav>
            );
          })}
        </Fragment>
      ))}
    </DashboardNavigationWrapper>
  );
};

const linkList = [
  {
    id: 10,
    title: "DASHBOARD",
    list: [
      {
        id: 1,
        href: "/profile",
        title: "DASHBOARD",
        iconName: "menu-header-four",
        count: 95,
      },
      {
        id: 2,
        href: "/orders",
        title: "Orders",
        iconName: "bag",
        count: 5,
      },
      // {
      //   href: "/wish-list",
      //   title: "Wishlist",
      //   iconName: "heart",
      //   count: 19,
      // },
      {
        id: 3,
        href: "/support-tickets",
        title: "Support Tickets",
        iconName: "customer-service",
        count: 1,
      },
      {
        id: 4,
        href: "/new-sells",
        title: "New Sell",
        iconName: "upload",
      },
      {
        id: 5,
        href: "/sells",
        title: "Sells",
        iconName: "shopping-cart",
        count: 40,
      },
    ],
  },
  {
    id: 20,
    title: "ACCOUNT SETTINGS",
    list: [
      {
        id: 6,
        href: "/profile/edit",
        title: "Edit Profile",
        iconName: "edit-profile",
      },
      // {
      //   href: "/address",
      //   title: "Addresses",
      //   iconName: "pin",
      //   count: 16,
      // },
      // {
      //   href: "/payment-methods",
      //   title: "Payment Methods",
      //   iconName: "credit-card",
      //   count: 4,
      // },
      {
        id: 7,
        href: "/",
        title: "Logout",
        iconName: "logout",
      },
    ],
  },
];

export default CustomerDashboardNavigation;
