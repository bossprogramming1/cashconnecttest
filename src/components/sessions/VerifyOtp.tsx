import { useAppContext } from "@context/app/AppContext";
import {
  Get_phone_varification_code_for_reset_password,
  Get_phone_varification_code_for_verify,
} from "@data/constants";
import axios from "axios";
import { useFormik } from "formik";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import * as yup from "yup";
import Button from "../buttons/Button";

import TextField from "../text-field/TextField";
import Typography, { H2 } from "../Typography";
import { StyledSessionCard } from "./SessionStyle";
import Image from "@component/Image";
import FlexBox from "@component/FlexBox";

interface SignupProps {
  type?: string;
}

const VerifyOtp: React.FC<SignupProps> = () => {
  const [message, setMessage] = useState("");
  const storedCountdown =
    typeof localStorage !== "undefined"
      ? localStorage.getItem("countdown")
      : null;
  const storedIsResendEnabled =
    typeof localStorage !== "undefined"
      ? localStorage.getItem("isResendEnabled") === "true" || true
      : true;

  const [countdown, setCountdown] = useState(
    parseInt(storedCountdown, 10) ?? 0
  );
  const [_isResendEnabled, setIsResendEnabled] = useState(
    storedIsResendEnabled as boolean
  );

  const router = useRouter();

  const { dispatch } = useAppContext();

  const [loading, setLoading] = useState(false);

  const handleLoadingComplete = () => {
    setLoading(false);
  };

  useEffect(() => {
    router.events.on("routeChangeComplete", handleLoadingComplete);
  }, [router.events]);

  const handleOtpVerifySubmit = async () => {
    setLoading(true);
    axios
      .post(`${Get_phone_varification_code_for_verify}`, values)
      .then((res) => {
        if (res.status === 200) {
          router.push(
            `/resetPassword?phone=${values.primary_phone}&otp=${values.otp}`
          );
          dispatch({
            type: "CHANGE_ALERT",
            payload: {
              alertValue: res.data.detail,
            },
          });
        } else {
          dispatch({
            type: "CHANGE_ALERT",
            payload: {
              alertValue: res.data.detail,
            },
          });
        }
      })
      .catch((error) => {
        setLoading(false);

        if (error.response.status !== 200) {
          // The request was made and the server responded with a status code
          setFieldError("otp", error.response.data.detail);
        }
      });
  };

  useEffect(() => {
    let timer;

    if (countdown > 0) {
      timer = setTimeout(() => {
        setCountdown(countdown - 1);
        localStorage.setItem("countdown", `${countdown - 1}`);
      }, 1000);
    } else {
      setIsResendEnabled(true);
    }

    return () => {
      clearTimeout(timer);
    };
  }, [countdown]);

  useEffect(() => {
    if (message) {
      setTimeout(() => {
        setMessage("");
      }, 3000);
    }
  }, [message]);

  const handleResendClick = () => {
    // Add your logic to resend the OTP here
    setIsResendEnabled(false);
    setCountdown(120);

    if (typeof localStorage !== "undefined") {
      localStorage.setItem("countdown", "120");
      localStorage.setItem("isResendEnabled", "false");
    }

    axios
      .get(
        `${Get_phone_varification_code_for_reset_password}?primary_phone=${values.primary_phone}`
      )
      .then((res) => {
        if (res.status === 200) {
          setCountdown(120);
          setMessage("OTP resend successflly");
        } else {
          dispatch({
            type: "CHANGE_ALERT",
            payload: {
              alertValue: res.data.detail,
            },
          });
        }
      })
      .catch((error) => {
        setLoading(false);

        if (error.response.status !== 200) {
          // The request was made and the server responded with a status code
          setFieldError("otp", error.response.data.detail);
        }
      });
    // Reset the countdown to 2 minutes
  };

  const {
    values,
    errors,
    handleBlur,
    handleChange,
    handleSubmit,
    setFieldValue,
    setFieldError,
  } = useFormik({
    onSubmit: handleOtpVerifySubmit,
    initialValues,
    validationSchema: formSchema,
  });
  const phoneNumber = router.query?.phone;

  useEffect(() => {
    if (phoneNumber) {
      setFieldValue("primary_phone", phoneNumber);
    }
  }, [phoneNumber]);

  return (
    <>
      {loading && (
        <div
          style={{
            position: "fixed",
            height: "100%",
            width: "100%",
            top: "0px",
            left: "0px",
            display: "flex",
            justifyContent: "center",
            backgroundColor: " rgb(0 0 0 / 50%)",
            alignItems: "center",
            zIndex: 100,
          }}
        >
          <Image
            style={{
              height: "100px",
              width: "100px",
              marginTop: "100pz",
            }}
            src="/assets/images/gif/loading.gif"
          />
        </div>
      )}

      <StyledSessionCard mx="auto" my="2rem" boxShadow="large">
        <H2 textAlign="center" mb="0.5rem" mt="2.5rem" color="#e02043">
          OTP
        </H2>

        <form
          className="content"
          style={{ paddingTop: "0px" }}
          onSubmit={handleSubmit}
        >
          <TextField
            mb="0.75rem"
            mt="1rem"
            name="primary_phone"
            // placeholder="Obtional"
            label="Phone Number"
            fullwidth
            onBlur={handleBlur}
            onChange={handleChange}
            value={values.primary_phone || ""}
            errorText={errors.primary_phone || ""}
          />
          <TextField
            mb="0.75rem"
            mt="1rem"
            name="otp"
            // placeholder="Obtional"
            label="An OTP has been sent to your phone"
            fullwidth
            onBlur={handleBlur}
            onChange={handleChange}
            value={values.otp || ""}
            errorText={errors.otp || ""}
          />

          <FlexBox
            style={{
              justifyContent: "space-between",
              alignItems: "flex-start",
            }}
          >
            <Typography fullwidth ml="0px" fontSize="12px" fontWeight={600}>
              Didn't get the OTP?
            </Typography>

            {countdown > 0 && (
              <Typography
                fullwidth
                ml="0px"
                style={{
                  cursor: "not-allowed",
                  fontSize: "12px",
                  opacity: 0.5,
                }}
              >
                Resend ({Math.floor(countdown / 60)}:
                {countdown % 60 < 10 ? `0${countdown % 60}` : countdown % 60})
              </Typography>
            )}

            {countdown === 0 && (
              <Typography
                fullwidth
                ml="0px"
                style={{
                  cursor: "pointer",
                  fontSize: "12px",
                  borderColor: "gray.900",
                  borderBottom: "1px solid",
                  opacity: 1,
                }}
                onClick={handleResendClick}
              >
                Resend
              </Typography>
            )}
          </FlexBox>

          {message && (
            <Typography fullwidth ml="0px" color="green" fontSize="12px">
              {message}
            </Typography>
          )}

          <Button
            mt="2.65rem"
            mb="2.65rem"
            variant="contained"
            color="primary"
            fullwidth
            type="submit"
            disabled={countdown === 0}
          >
            Verify OTP
          </Button>
        </form>
      </StyledSessionCard>
    </>
  );
};

const initialValues = {
  primary_phone: "",
  otp: "",
};

const formSchema = yup.object().shape({
  primary_phone: yup
    .string()
    .min(11, "Phone number must be at least 11 digits")
    .max(11, "Phone number must be at most 11 digits")
    .required("Please enter your phone number"),
  otp: yup
    .string()
    .required("Please enter your OTP")
    .min(6, "OTP must be exactly 6 digits")
    .max(6, "OTP must be exactly 6 digits"),
});

export default VerifyOtp;
