import FlexBox from "@component/FlexBox";
import LoginPopup from "@component/LoginPopup";
import { useAppContext } from "@context/app/AppContext";
import {
  Get_phone_varification_code_by_customer,
  Get_phone_varification_code_for_resend_otp,
} from "@data/constants";
import axios from "axios";
import { useFormik } from "formik";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import * as yup from "yup";
import Button from "../buttons/Button";

import TextField from "../text-field/TextField";
import Typography, { H2 } from "../Typography";
import { StyledSessionCard } from "./SessionStyle";
import Image from "@component/Image";

interface SignupProps {
  type?: string;
  primary_phone?: string;
  closeSignupDialog?: any;
}

const Otp: React.FC<SignupProps> = () => {
  const router = useRouter();

  const [openLogin, setOpenLogin] = useState(false);
  const [_otpError, setOtpError] = useState("");
  const phoneNumber = router.query?.phoneNumber;

  // counting
  const [message, setMessage] = useState("");
  const storedCountdown =
    typeof localStorage !== "undefined"
      ? localStorage.getItem("countdownSignUpOtp")
      : null;
  const storedIsResendEnabled =
    typeof localStorage !== "undefined"
      ? localStorage.getItem("isResendEnabledSignUpOtp") === "true" || true
      : true;

  const [countdown, setCountdown] = useState(
    parseInt(storedCountdown, 10) ?? 0
  );
  const [_isResendEnabled, setIsResendEnabled] = useState(
    storedIsResendEnabled as boolean
  );
  const { dispatch } = useAppContext();

  const closeLoginTab = () => {
    setOpenLogin(false);
  };
  const [loading, setLoading] = useState(false);

  const handleLoadingComplete = () => {
    setLoading(false);
  };

  useEffect(() => {
    router.events.on("routeChangeComplete", handleLoadingComplete);
  }, [router.events]);

  const handleOtpSubmit = async () => {
    setLoading(true);

    axios
      .get(
        `${Get_phone_varification_code_by_customer}?primary_phone=${values.primary_phone}&phone_otp=${values.otp}`
      )
      .then((res) => {
        if (res.status === 200) {
          router.push("/login");
          dispatch({
            type: "CHANGE_ALERT",
            payload: {
              alertValue: res.data.detail,
            },
          });
        } else {
          dispatch({
            type: "CHANGE_ALERT",
            payload: {
              alertValue: res.data.detail,
            },
          });
        }
      })
      .catch((error) => {
        setLoading(false);

        if (error.response.status !== 200) {
          // The request was made and the server responded with a status code
          setOtpError(error.response.data.detail);
        }
      });
  };

  //Resend Otp
  const handleResendClick = () => {
    // Add your logic to resend the OTP here
    setIsResendEnabled(false);
    setCountdown(120);

    if (typeof localStorage !== "undefined") {
      localStorage.setItem("countdownSignUpOtp", "120");
      localStorage.setItem("isResendEnabledSignUpOtp", "false");
    }
    axios
      .get(
        `${Get_phone_varification_code_for_resend_otp}?primary_phone=${phoneNumber}`
      )
      .then((res) => {
        if (res.status === 200) {
          dispatch({
            type: "CHANGE_ALERT",
            payload: {
              alertValue: res.data.detail,
            },
          });
        } else {
          dispatch({
            type: "CHANGE_ALERT",
            payload: {
              alertValue: res.data.detail,
            },
          });
        }
      })
      .catch(() => {
        setLoading(false);
      });
  };

  //counting condition
  useEffect(() => {
    let timer;

    if (countdown > 0) {
      timer = setTimeout(() => {
        setCountdown(countdown - 1);
        localStorage.setItem("countdownSignUpOtp", `${countdown - 1}`);
      }, 1000);
    } else {
      setIsResendEnabled(true);
    }

    return () => {
      clearTimeout(timer);
    };
  }, [countdown]);

  useEffect(() => {
    if (message) {
      setTimeout(() => {
        setMessage("");
      }, 3000);
    }
  }, [message]);

  const {
    values,
    errors,
    handleBlur,
    handleChange,
    handleSubmit,
    setFieldValue,
  } = useFormik({
    onSubmit: handleOtpSubmit,
    initialValues,
    validationSchema: formSchema,
  });

  useEffect(() => {
    if (phoneNumber) {
      setFieldValue("primary_phone", phoneNumber);
    }
  }, [phoneNumber]);

  return (
    <>
      <LoginPopup open={openLogin} closeLoginDialog={closeLoginTab} />
      {loading && (
        <div
          style={{
            position: "fixed",
            height: "100%",
            width: "100%",
            top: "0px",
            left: "0px",
            display: "flex",
            justifyContent: "center",
            backgroundColor: " rgb(0 0 0 / 50%)",
            alignItems: "center",
            zIndex: 100,
          }}
        >
          <Image
            style={{
              height: "100px",
              width: "100px",
              marginTop: "100pz",
            }}
            src="/assets/images/gif/loading.gif"
          />
        </div>
      )}

      <StyledSessionCard mx="auto" my="2rem" boxShadow="large">
        <H2 textAlign="center" mb="0.5rem" mt="2.5rem" color="#e02043">
          OTP
        </H2>

        <form
          className="content"
          style={{ paddingTop: "0px" }}
          onSubmit={handleSubmit}
        >
          <TextField
            mb="0.75rem"
            mt="1rem"
            name="primary_phone"
            // placeholder="Obtional"
            label="Phone Number"
            fullwidth
            onBlur={handleBlur}
            onChange={handleChange}
            value={values.primary_phone || ""}
            errorText={errors.primary_phone || ""}
          />
          <TextField
            mb="0.75rem"
            mt="1rem"
            name="otp"
            // placeholder="Obtional"
            label="An OTP has been sent to your phone"
            fullwidth
            onBlur={handleBlur}
            onChange={handleChange}
            value={values.otp || ""}
            errorText={errors.otp || ""}
          />

          <FlexBox
            style={{
              justifyContent: "space-between",
              alignItems: "flex-start",
            }}
          >
            <Typography fullwidth ml="0px" fontSize="12px" fontWeight={600}>
              Didn't get the OTP?
            </Typography>

            {countdown > 0 && (
              <Typography
                fullwidth
                ml="0px"
                style={{
                  cursor: "not-allowed",
                  fontSize: "12px",
                  opacity: 0.5,
                }}
              >
                Resend ({Math.floor(countdown / 60)}:
                {countdown % 60 < 10 ? `0${countdown % 60}` : countdown % 60})
              </Typography>
            )}

            {countdown === 0 && (
              <Typography
                fullwidth
                ml="0px"
                style={{
                  cursor: "pointer",
                  fontSize: "12px",
                  borderColor: "gray.900",
                  borderBottom: "1px solid",
                  opacity: 1,
                }}
                onClick={handleResendClick}
              >
                Resend
              </Typography>
            )}
          </FlexBox>

          {message && (
            <Typography fullwidth ml="0px" color="green" fontSize="12px">
              {message}
            </Typography>
          )}

          <Button
            mt="2.65rem"
            mb="2.65rem"
            variant="contained"
            color="primary"
            fullwidth
            type="submit"
            disabled={countdown === 0}
          >
            Verify OTP
          </Button>
        </form>
      </StyledSessionCard>
    </>
  );
};

const initialValues = {
  otp: "",
  primary_phone: "",
};

const formSchema = yup.object().shape({
  otp: yup.string().required("otp is required"),
});

export default Otp;
