import Box from "@component/Box";
// import useFormattedProductData from "@customHook/useFormattedProductData";
import { Product_Flash_Deals } from "@data/constants";
import getFormattedProductData2 from "@helper/getFormattedProductData";
import axios from "axios";
import _ from "lodash";
import React, { useEffect, useState } from "react";
import useWindowSize from "../../hooks/useWindowSize";
import Carousel from "../carousel/Carousel";
import CategorySectionCreator from "../CategorySectionCreator";
import ProductCard1 from "../product-cards/ProductCard1";

interface Section2Props {
  topRatedList: any[];
}

const Section2: React.FC<Section2Props> = ({ topRatedList }) => {
  //old
  // const [topRatedListLists, setTopRatedListLists] = useFormattedProductData(
  //   []
  // );
  //new
  const [topRatedListLists, setTopRatedListLists] = useState([]);
  const [page, setPage] = useState(1);
  const [pageEnd, setpageEnd] = useState(false);
  const [visibleSlides, setVisibleSlides] = useState(5);
  const width = useWindowSize();

  useEffect(() => {
    if (width < 370) setVisibleSlides(2);
    else if (width < 650) setVisibleSlides(2);
    else if (width < 950) setVisibleSlides(4);
    else setVisibleSlides(5);
  }, [width]);

  useEffect(() => {
    setTopRatedListLists(topRatedList);
  }, [topRatedList]);

  const getMoreItem = () => {
    if (!pageEnd) {
      axios
        .get(`${Product_Flash_Deals}?page=${page + 1}&size=${6}`)
        .then((res) => {
          const newItem = getFormattedProductData2(res.data.products);
          if (res.data.total_pages > 1) {
            const topRatedListState = topRatedList;
            setTopRatedListLists(topRatedListState.concat(newItem));

            setPage(page + 1);
          }
          if (res.data.total_pages == page + 1) {
            setpageEnd(true);
          }
        });
    } else {
    }
  };

  useEffect(() => {
    getMoreItem();
  }, []);

  const product_list = (
    <CategorySectionCreator
      iconName="light"
      title="Top Ratings"
      seeMoreLink="/product/search/top_ratings_all"
    >
      <Box>
        <Carousel
          totalSlides={topRatedListLists?.length}
          visibleSlides={visibleSlides}
          step={visibleSlides}
          getMoreItem={getMoreItem}
          //old
          // autoPlay={topCategoryList.length > 5 ? true : false}
          autoPlay={false}
          showArrow={width > 600 && topRatedListLists.length > 5 ? true : false}
        >
          {topRatedListLists?.map((item) => (
            <Box py="0.25rem" key={item.id}>
              <ProductCard1 {...item} flash="flash" />
            </Box>
          ))}
        </Carousel>
      </Box>
    </CategorySectionCreator>
  );

  const returnableData = _.isEmpty(topRatedList) ? null : product_list;

  return returnableData;
};

export default Section2;
