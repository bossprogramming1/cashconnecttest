// import useUserInf from "@customHook/useUserInf";
import React from "react";
import WhatsAppWidget from "../whatsApp/index";

const Section14: React.FC = () => {
  // const { authTOKEN } = useUserInf();

  //

  // useEffect(() => {
  //   const token = authTOKEN.headers.Authorization.slice(7);

  //   var ws = new WebSocket(`192.168.0.151:8001/ws/chat/?token=${token}`);

  //   ws.onopen = function () {
  //     ws.send(
  //       JSON.stringify({
  //         message: "Hi , Hello baror Islam ,, vai biya korben ni!!!",
  //       })
  //     );
  //   };
  //   ws.onmessage = function () {};
  //   ws.onerror = function () {};
  //   ws.onclose = function () {};
  // }, []);

  /// text

  return (
    <div>
      {/* <Chat /> */}
      {/* <Launcher
        agentProfile={{
          teamName: "react-chat-window",
          imageUrl:
            "https://a.slack-edge.com/66f9/img/avatars-teams/ava_0001-34.png",
        }}
        // onMessageWasSent={this._onMessageWasSent.bind(this)}
        // messageList={this.state.messageList}
        showEmoji
      /> */}
      <WhatsAppWidget
        phoneNo="8801852041302"
        position="right"
        widgetWidth="300px"
        widgetWidthMobile="260px"
        autoOpen={true}
        autoOpenTimer={5000}
        messageBox={true}
        messageBoxTxt="Hi Team, is there any related service available ?"
        messageBoxColor="white"
        iconSize="40"
        iconColor="white"
        iconBgColor="#e02043"
        headerIcon="https://www.pdapps.net.in/_next/static/media/android-chrome-192x192.9a39c2c7.png"
        headerIconColor="pink"
        headerTxtColor="black"
        headerBgColor="#f0f2f5"
        headerTitle="Cash Connect"
        headerCaption="Online"
        bodyBgColor="#efeae2"
        chatPersonName="Support"
        chatMessage={
          <>
            Hi there 👋 <br />
            <br /> How can I help you?
          </>
        }
        footerBgColor="#f0f2f5"
        placeholder="Type a message.."
        btnBgColor="#54656f"
        btnTxt="Start Chat"
        btnTxtColor="white"
      />
    </div>
  );
};

export default Section14;
