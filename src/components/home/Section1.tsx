// import Box from "@component/Box";
import CarouselCard1 from "@component/carousel-cards/CarouselCard1";
import Carousel from "@component/carouselSlider/Carousel";
// import Container from "@component/Container";
// import useWindowSize from "@hook/useWindowSize";
import React from "react";

const Section1 = ({ sliderList }) => {
  // const width = useWindowSize();
  // const isMobile = width <= 768;
  return (
    <>
      {/* <Box bg="gray.white" mb="3.75rem"> */}
      {/* <Container slider> */}
      <Carousel
        totalSlides={sliderList?.length}
        visibleSlides={1}
        infinite={true}
        autoPlay={true}
        showDots={true}
        showArrow={false}
        spacing="0px"
      >
        {sliderList.map((data) => (
          <CarouselCard1
            key={data.id}
            title={data.title}
            details={data.details}
            link={data.link}
            image={data.image}
          />
        ))}
      </Carousel>
      {/* </Container> */}
      {/* </Box> */}
    </>
  );
};

export default Section1;
