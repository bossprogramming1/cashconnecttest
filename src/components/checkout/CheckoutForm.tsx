import LoginPopup from "@component/LoginPopup";
import useUserInf from "@customHook/useUserInf";
import {
  City_All_BY_COUNTRY_ID,
  Country_All,
  Customer_Shpping_Billing_Address_By_UserId,
  Customer_Order_Blling_Address,
  Thana_All_BY_CITY_ID,
  Customer_Order_Blling_Address_Update_By_Order_id,
  Customer_Order_Shipping_Address,
  Customer_Order_Shipping_Address_Update_By_Order_id,
  User_By_Id,
} from "@data/constants";
// import { required } from "@data/data";
import axios from "axios";
import { FormikErrors, useFormik } from "formik";
// import Link from "next/link";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import * as yup from "yup";
import Button from "../buttons/Button";
import { Card1 } from "../Card1";
import CheckBox from "../CheckBox";
import Grid from "../grid/Grid";
import Select from "../Select";
import TextField from "../text-field/TextField";
import Typography from "../Typography";
import Icon from "@component/icon/Icon";
import FlexBox from "@component/FlexBox";

// function areAddressesEqual(address1: any, address2: any): boolean {
//   const fieldsToCompare = [
//     "name",
//     "email",
//     "phone",
//     "street_address",
//     "thana",
//     "city",
//     "country",
//     "zip_code",
//   ];

//   for (const field of fieldsToCompare) {
//     if (field === "thana" || field === "city" || field === "country") {
//       if (!areObjectsEqual(address1[field], address2[field])) {
//         return false;
//       }
//     } else {
//       if (address1[field] !== address2[field]) {
//         return false;
//       }
//     }
//   }

//   return true;
// }

// function areObjectsEqual(obj1: any, obj2: any): boolean {
//   if (
//     obj1 === null ||
//     obj2 === null ||
//     typeof obj1 !== "object" ||
//     typeof obj2 !== "object"
//   ) {
//     return false;
//   }

//   const keys1 = Object.keys(obj1);
//   const keys2 = Object.keys(obj2);

//   if (keys1.length !== keys2.length) {
//     return false;
//   }

//   for (const key of keys1) {
//     if (key !== "id" && obj1[key] !== obj2[key]) {
//       return false;
//     }
//   }

//   return true;
// }

const CheckoutForm = ({ handleEnableSubmit, confurmSubmitForm }) => {
  const router = useRouter();

  // const [Id, setShippingId] = useState(0);
  const [thanas, setThanas] = useState([]);
  const [cities, setCities] = useState([]);
  const [thanasShipping, setThanasShipping] = useState([]);
  const [citiesShipping, setCitiesShipping] = useState([]);

  const [countries, setCountries] = useState([]);
  const [openLogin, setOpenLogin] = useState(false);
  const { user_id, authTOKEN, order_Id } = useUserInf();
  const [billingAddress, setBillingAddress] = useState<any>({});
  const [shippingAddress, setShippingAddress] = useState<any>({});
  const [openBillingForm, setOpenBillingForm] = useState(false);
  const [openShippingForm, setOpenShippingForm] = useState(false);
  const [updateForm, setUpdateForm] = useState(false);
  const [getBillingByOrder, setGetBillingByOrder] = useState(false);
  const [isChecked, setIsChecked] = useState(false);
  const [createAdress, setCreateAddress] = useState(false);
  // const [getShippingByOrder, setGetShippingByOrder] = useState(false);

  const closeLoginTab = () => {
    setOpenLogin(false);
  };

  useEffect(() => {
    if (confurmSubmitForm === true) {
      router.push("/payment");
    }
  }, [confurmSubmitForm]);

  const handleFormSubmit = async (_values) => {};

  const handleCheckboxChange =
    (setFieldValue: {
      (field: string, value: any, shouldValidate?: boolean):
        | Promise<void>
        | Promise<FormikErrors<any>>;
      (field: string, value: any, shouldValidate?: boolean):
        | Promise<void>
        | Promise<FormikErrors<any>>;
      (arg0: string, arg1: any): void;
    }) =>
    (event) => {
      const { checked } = event.target;
      setFieldValue("is_different_shipping_address", checked);
      setIsChecked(checked);
    };
  //billing address first time call
  const handleSaveBillingAddressFirstTime = () => {
    // if (!isChecked) {
    const billingData = {
      is_different_shipping_address: false,
      user: user_id,
      order: order_Id,
      name: values.name,
      email: values.email,
      phone: `+88${values.phone}`,
      street_address: values.street_address,
      zip_code: values.zip_code,
      thana:
        typeof values.thana != "object" ? values?.thana : values?.thana?.id,
      city: typeof values.city != "object" ? values?.city : values?.city?.id,
      country:
        typeof values.country != "object"
          ? values?.country
          : values?.country?.id,
    };
    axios
      .post(`${Customer_Order_Blling_Address}`, billingData, authTOKEN)
      .then(() => {
        setCreateAddress(true);
      });
    // }
    // else {
    //   const billingData = {
    //     is_different_shipping_address: isChecked ? true : false,
    //     user: user_id,
    //     order: order_Id,
    //     name: values.name,
    //     email: values.email,
    //     phone: `+88${values.phone}`,
    //     street_address: values.street_address,
    //     zip_code: values.zip_code,
    //     thana:
    //       typeof values.thana != "object" ? values?.thana : values?.thana?.id,
    //     city: typeof values.city != "object" ? values?.city : values?.city?.id,
    //     country:
    //       typeof values.country != "object"
    //         ? values?.country
    //         : values?.country?.id,
    //   };
    //   axios
    //     .post(`${Customer_Order_Blling_Address}`, billingData, authTOKEN)
    //     .then(() => {
    //       setCreateAddress(true);
    //     });

    //   axios
    //     .post(`${Customer_Order_Shipping_Address}`, billingData, authTOKEN)
    //     .then(() => {
    //       setCreateAddress(true);
    //     })
    //     .catch(() => {});
    // }
  };
  ///billing Address save and update
  const handleSaveBillingAddress = () => {
    const billingData = {
      is_different_shipping_address: isChecked ? true : false,
      user: user_id,
      order: order_Id,
      name: values.name,
      email: values.email,
      phone: `+88${values.phone}`,
      street_address: values.street_address,
      zip_code: values.zip_code,
      thana:
        typeof values.thana != "object" ? values?.thana : values?.thana?.id,
      city: typeof values.city != "object" ? values?.city : values?.city?.id,
      country:
        typeof values.country != "object"
          ? values?.country
          : values?.country?.id,
    };
    axios
      .post(`${Customer_Order_Blling_Address}`, billingData, authTOKEN)
      .then(() => {
        setOpenBillingForm(false);
        setUpdateForm(true);
      });
  };
  const handleEditBillingAddress = () => {
    const billingData = {
      is_different_shipping_address: isChecked ? true : false,
      user: user_id,
      order: order_Id,
      name: values.name,
      email: values.email,
      phone: `+88${values.phone}`,
      street_address: values.street_address,
      zip_code: values.zip_code,
      thana:
        typeof values.thana != "object" ? values?.thana : values?.thana?.id,
      city: typeof values.city != "object" ? values?.city : values?.city?.id,
      country:
        typeof values.country != "object"
          ? values?.country
          : values?.country?.id,
    };

    axios
      .put(
        `${Customer_Order_Blling_Address_Update_By_Order_id}${order_Id}`,
        billingData,
        authTOKEN
      )
      .then(() => {
        setOpenBillingForm(false);
        setUpdateForm(true);
        // router.push("/payment");
      });
  };
  /// end billing Address save and update

  /// start shipping Address save and update
  const handleSaveShippingAddress = () => {
    const shippingData = {
      is_different_shipping_address: isChecked ? true : false,
      user: user_id,
      order: order_Id,
      name: values.name_shipping,
      email: values.email_shipping,
      phone: `+88${values.phone_shipping}`,
      street_address: values.street_address_shipping,
      zip_code: values.zip_code_shipping,
      thana:
        typeof values.thana_shipping != "object"
          ? values?.thana_shipping
          : values?.thana_shipping?.id,
      city:
        typeof values.city_shipping != "object"
          ? values?.city_shipping
          : values?.city_shipping?.id,
      country:
        typeof values.country_shipping != "object"
          ? values?.country_shipping
          : values?.country_shipping?.id,
    };
    axios
      .post(`${Customer_Order_Shipping_Address}`, shippingData, authTOKEN)
      .then(() => {
        setOpenShippingForm(false);
        setUpdateForm(true);
      })
      .catch(() => {});
  };
  const handleEditShippingAddress = () => {
    const shippingData = {
      is_different_shipping_address: isChecked ? true : false,
      user: user_id,
      order: order_Id,
      name: values.name_shipping,
      email: values.email_shipping,
      phone: `+88${values.phone_shipping}`,
      street_address: values.street_address_shipping,
      zip_code: values.zip_code_shipping,
      thana:
        typeof values.thana_shipping != "object"
          ? values?.thana_shipping
          : values?.thana_shipping?.id,
      city:
        typeof values.city_shipping != "object"
          ? values?.city_shipping
          : values?.city_shipping?.id,
      country:
        typeof values.country_shipping != "object"
          ? values?.country_shipping
          : values?.country_shipping?.id,
    };

    axios
      .put(
        `${Customer_Order_Shipping_Address_Update_By_Order_id}${order_Id}`,
        shippingData,
        authTOKEN
      )
      .then(() => {
        setOpenShippingForm(false);
        setUpdateForm(true);
        // router.push("/payment");
      });
  };
  /// end billing Address save and update
  useEffect(() => {
    if (user_id && order_Id) {
      axios
        .get(`${Customer_Shpping_Billing_Address_By_UserId}`, authTOKEN)
        .then((user) => {
          const { data } = user;

          ///for checkBOx
          // const areEqual = areAddressesEqual(
          //   data.billing_address,
          //   data?.shipping_address
          // );
          if (Object.keys(data.billing_address).length !== 0) {
            handleEnableSubmit(true);
            // if (areEqual === true) {
            //   setIsChecked(false);
            // } else {
            //   setIsChecked(true);
            // }
          } else {
            handleEnableSubmit(false);
            // setIsChecked(false);
          }
          if (data?.billing_address && data?.shipping_address) {
            // if (data.billing_address) {
            setGetBillingByOrder(true);
            setBillingAddress(data?.billing_address);
            handleCity(data?.billing_address?.country);
            handleThana(data?.billing_address?.city?.id);

            setFieldValue("name", `${data?.billing_address?.name}`);
            setFieldValue("email", `${data?.billing_address?.email}`);
            setFieldValue("phone", `${data?.billing_address?.phone.slice(3)}`);

            setFieldValue("city", data?.billing_address?.city?.id);
            setFieldValue("country", data?.billing_address?.country?.id);
            setFieldValue("thana", data?.billing_address?.thana?.id);
            setFieldValue("zip_code", data?.billing_address?.zip_code);
            setFieldValue(
              "street_address",
              `${data?.billing_address?.street_address}`
            );

            setUpdateForm(false);
            //shipping Address
            setShippingAddress(data?.shipping_address);
            handleCityShipping(data?.shipping_address?.country.id);
            handleThanaShipping(data?.shipping_address?.city?.id);

            setFieldValue("name_shipping", data?.shipping_address?.name);
            setFieldValue("email_shipping", data?.shipping_address?.email);
            setFieldValue(
              "phone_shipping",
              data?.shipping_address?.phone.slice(3)
            );

            setFieldValue("city_shipping", data?.shipping_address?.city?.id);
            setFieldValue(
              "country_shipping",
              data?.shipping_address?.country?.id
            );
            setFieldValue("thana_shipping", data?.shipping_address?.thana?.id);
            setFieldValue(
              "zip_code_shipping",
              data?.shipping_address?.zip_code
            );
            setFieldValue(
              "street_address_shipping",
              data?.shipping_address?.street_address
            );

            setUpdateForm(false);
            // }

            //* shipping address value set and other condition

            // setGetShippingByOrder(true);
            // if (data?.shipping_address) {

            // }
          } else {
            setFieldError("street_address", "Need address");
          }
        })
        .catch(() => {
          // for 1st time call
          setGetBillingByOrder(false);
          handleEnableSubmit(false);
          setOpenBillingForm(true);
          axios
            .get(`${User_By_Id}`, authTOKEN)
            .then((user) => {
              const { data } = user;
              //
              setFieldValue("name", `${data.first_name} ${data.last_name}`);
              setFieldValue("email", `${data.email}`);
              setFieldValue("phone", `${data.primary_phone.slice(3)}`);
              setFieldValue(
                "name_shipping",
                `${data.first_name} ${data.last_name}`
              );
              setFieldValue("email_shipping", `${data.email}`);
              setFieldValue("phone_shipping", `${data.primary_phone.slice(3)}`);
            })
            .catch(() => {
              //
            });
        });
    }
  }, [user_id, authTOKEN, order_Id, updateForm]);

  const handleCity = (country: { id: any }) => {
    fetch(`${City_All_BY_COUNTRY_ID}${country.id ? country.id : country}`)
      .then((res) => res.json())
      .then((data) => {
        setCities(data);
      })
      .catch(() => {});
  };

  const handleThana = (city: { id: any }) => {
    fetch(`${Thana_All_BY_CITY_ID}${city.id || city}`)
      .then((res) => res.json())
      .then((data) => {
        setThanas(data);
      })
      .catch(() => {});
  };
  const handleCityShipping = (country: { id: any }) => {
    fetch(`${City_All_BY_COUNTRY_ID}${country.id ? country.id : country}`)
      .then((res) => res.json())
      .then((data) => {
        setCitiesShipping(data);
      })
      .catch(() => {});
  };
  const handleThanaShipping = (city: { id: any }) => {
    fetch(`${Thana_All_BY_CITY_ID}${city.id || city}`)
      .then((res) => res.json())
      .then((data) => {
        setThanasShipping(data);
      })
      .catch(() => {});
  };

  useEffect(() => {
    fetch(`${Country_All}`)
      .then((res) => res.json())
      .then((data) => {
        setCountries(data.countries);

        const getPspIssPlace = data.countries.find(
          (data: { name: string }) =>
            data.name === "Bangladesh" || data.name === "bangladesh"
        )?.id;

        setFieldValue("country", getPspIssPlace);
        setFieldValue("country_shipping", getPspIssPlace);
        data.countries.find((data: { name: string; id: any }) =>
          data.name === "Bangladesh" ? handleCity(data.id) : ""
        );
        data.countries.find((data: { name: string; id: any }) =>
          data.name === "Bangladesh" ? handleCityShipping(data.id) : ""
        );
      })

      .catch(() => {});
  }, [order_Id]);

  const {
    values,
    errors,
    touched,
    handleChange,
    handleBlur,
    handleSubmit,
    setFieldValue,
    setFieldError,
  } = useFormik({
    initialValues: initialValues,
    validationSchema: checkoutSchema,
    onSubmit: handleFormSubmit,
  });

  ///  billing Address save
  useEffect(() => {
    if (values.street_address && !createAdress && !openBillingForm) {
      handleSaveBillingAddressFirstTime();
    }
  }, [values.street_address, createAdress, openBillingForm]);

  return (
    <>
      <LoginPopup open={openLogin} closeLoginDialog={closeLoginTab} />
      <form onSubmit={handleSubmit}>
        <Card1 mb="2rem">
          <div>
            <Grid container spacing={4}>
              <Grid item sm={12} lg={6} xs={12}>
                {Object.keys(billingAddress).length !== 0 &&
                  !openBillingForm && (
                    <>
                      <FlexBox mb="1rem">
                        <Icon style={{ color: "white" }}>address-card</Icon>
                        <Typography fontWeight="600" pt="2px" ml="5px">
                          Billing Address
                        </Typography>
                      </FlexBox>

                      <Card1
                        mb="2rem"
                        style={{
                          borderColor: "green",
                          border: "1px solid green",
                          position: "relative",

                          background: "#eff5ef",
                        }}
                      >
                        <Icon
                          style={{
                            position: "absolute",
                            top: "-5px",
                            right: "-5px",
                            color: "green",
                          }}
                        >
                          done-solid
                        </Icon>
                        <Grid container spacing={1}>
                          <Grid item sm={3} lg={3} xs={3}>
                            <Typography fontWeight="300" color="disabled">
                              Name:
                            </Typography>
                          </Grid>
                          <Grid item sm={9} lg={9} xs={9}>
                            <Typography fontWeight="600">
                              {billingAddress?.name}
                            </Typography>
                          </Grid>
                          <Grid item sm={3} lg={3} xs={3}>
                            <Typography fontWeight="300" color="disabled">
                              Phone:
                            </Typography>
                          </Grid>
                          <Grid item sm={9} lg={9} xs={9}>
                            <Typography fontWeight="600">
                              {billingAddress?.phone}
                            </Typography>
                          </Grid>
                          <Grid item sm={3} lg={3} xs={3}>
                            <Typography fontWeight="300" color="disabled">
                              Email:
                            </Typography>
                          </Grid>
                          <Grid item sm={9} lg={9} xs={9}>
                            <Typography fontWeight="600">
                              {billingAddress?.email || "No Email Found"}
                            </Typography>
                          </Grid>
                          {/* <Grid item sm={3} lg={3} xs={3}>
                        <Typography fontWeight="300" color="disabled">
                          Email:
                        </Typography>
                      </Grid>
                      <Grid item sm={9} lg={9} xs={9}>
                        <Typography fontWeight="600">
                          {billingAddress?.email}
                        </Typography>
                      </Grid> */}
                          <Grid item sm={3} lg={3} xs={3}>
                            <Typography fontWeight="300" color="disabled">
                              Address:
                            </Typography>
                          </Grid>
                          <Grid item sm={9} lg={9} xs={9}>
                            <Typography fontWeight="600">
                              {`${billingAddress?.street_address}, ${billingAddress?.thana?.name},${billingAddress?.city?.name}-${billingAddress?.zip_code} `}
                            </Typography>
                          </Grid>

                          <Grid item sm={3} lg={3} xs={3}>
                            <Button
                              variant="outlined"
                              color="primary"
                              size="extraSmall"
                              style={{ color: "black" }}
                              onClick={() => setOpenBillingForm(true)}
                            >
                              Edit
                            </Button>
                          </Grid>
                        </Grid>
                      </Card1>
                    </>
                  )}
              </Grid>
            </Grid>
          </div>

          {openBillingForm && (
            <>
              <div>
                <Typography fontWeight="600" mb="1rem">
                  Billing Address
                </Typography>
              </div>
              <Grid container spacing={7}>
                <Grid item sm={6} xs={12}>
                  <TextField
                    name="name"
                    label="Full Name"
                    fullwidth
                    mb="1rem"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    value={values.name || ""}
                    errorText={touched.name && errors.name}
                  />
                  <TextField
                    name="phone"
                    label="Phone Number"
                    fullwidth
                    mb="1rem"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    value={values.phone || ""}
                    errorText={touched.phone && errors.phone}
                  />
                  <Select
                    mb="1rem"
                    label="City"
                    placeholder="Select City"
                    options={cities}
                    value={values.city || ""}
                    onChange={(city: any) => {
                      setFieldValue("city", city?.id);
                      setFieldValue("thana", "");
                      handleThana(city);
                    }}
                    errorText={touched.city && errors.city}
                  />
                  <TextField
                    name="zip_code"
                    label="Zip Code"
                    type="number"
                    fullwidth
                    mb="1rem"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    value={values.zip_code || ""}
                    errorText={touched.zip_code && errors.zip_code}
                  />
                </Grid>
                <Grid item sm={6} xs={12}>
                  <TextField
                    name="email"
                    label="Email Address"
                    type="email"
                    fullwidth
                    mb="1rem"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    value={values.email || ""}
                    errorText={touched.email && errors.email}
                  />

                  <Select
                    mb="1rem"
                    label="Country"
                    placeholder="Select Country"
                    options={countries}
                    value={values.country || ""}
                    onChange={(country: any) => {
                      setFieldValue("country", country?.id);
                      setFieldValue("city", "");
                      setFieldValue("thana", "");
                      handleCity(country);
                    }}
                    errorText={touched.country && errors.country}
                  />

                  <Select
                    mb="1rem"
                    label="Thana"
                    placeholder="Select Thana"
                    options={thanas}
                    value={values.thana || ""}
                    onChange={(thana: any) => {
                      setFieldValue("thana", thana?.id);
                    }}
                    errorText={touched.thana && errors.thana}
                  />
                  <TextField
                    name="street_address"
                    label="Address"
                    fullwidth
                    onBlur={handleBlur}
                    onChange={handleChange}
                    value={values.street_address || ""}
                    errorText={touched.street_address && errors.street_address}
                  />
                </Grid>
              </Grid>
              <div
                style={{
                  display: "flex",
                  justifyContent: "flex-end",
                  marginTop: "10px",
                }}
              >
                <Button
                  variant="outlined"
                  color="primary"
                  onClick={() => setOpenBillingForm(false)}
                >
                  Cancel
                </Button>
                <Button
                  variant="contained"
                  color="primary"
                  ml="5px"
                  disabled={!values.street_address}
                  onClick={() => {
                    if (getBillingByOrder) {
                      handleEditBillingAddress();
                    } else {
                      handleSaveBillingAddress();
                    }
                  }}
                >
                  Save
                </Button>
              </div>
            </>
          )}

          {!openBillingForm && (
            <div>
              <CheckBox
                label={`I have a different`}
                address="Shipping Addres"
                color="primary"
                disabled={openBillingForm}
                // m={sameAsBillingAddress ? "0.5rem" : "0.5rem"}
                onChange={handleCheckboxChange(setFieldValue)}
                checked={isChecked}
                mb="20px"
              />
            </div>
          )}

          <div>
            <Grid container spacing={4}>
              <Grid item sm={12} lg={6} xs={12}>
                {Object.keys(shippingAddress).length !== 0 &&
                  !openShippingForm &&
                  isChecked && (
                    <>
                      <FlexBox mb="1rem">
                        <Icon style={{ color: "white" }}>address-card</Icon>
                        <Typography fontWeight="600" pt="2px" ml="5px">
                          Shipping Address
                        </Typography>
                      </FlexBox>

                      <Card1
                        mb="2rem"
                        style={{
                          borderColor: "green",
                          border: "1px solid green",
                          position: "relative",

                          background: "#eff5ef",
                        }}
                      >
                        <Icon
                          style={{
                            position: "absolute",
                            top: "-5px",
                            right: "-5px",
                            color: "green",
                          }}
                        >
                          done-solid
                        </Icon>
                        <Grid container spacing={1}>
                          <Grid item sm={3} lg={3} xs={3}>
                            <Typography fontWeight="300" color="disabled">
                              Name:
                            </Typography>
                          </Grid>
                          <Grid item sm={9} lg={9} xs={9}>
                            <Typography fontWeight="600">
                              {shippingAddress?.name}
                            </Typography>
                          </Grid>
                          <Grid item sm={3} lg={3} xs={3}>
                            <Typography fontWeight="300" color="disabled">
                              Phone:
                            </Typography>
                          </Grid>
                          <Grid item sm={9} lg={9} xs={9}>
                            <Typography fontWeight="600">
                              {shippingAddress?.phone}
                            </Typography>
                          </Grid>
                          <Grid item sm={3} lg={3} xs={3}>
                            <Typography fontWeight="300" color="disabled">
                              Email:
                            </Typography>
                          </Grid>
                          <Grid item sm={9} lg={9} xs={9}>
                            <Typography fontWeight="600">
                              {shippingAddress?.email || "No Email Found"}
                            </Typography>
                          </Grid>
                          {/* <Grid item sm={3} lg={3} xs={3}>
                        <Typography fontWeight="300" color="disabled">
                          Email:
                        </Typography>
                      </Grid>
                      <Grid item sm={9} lg={9} xs={9}>
                        <Typography fontWeight="600">
                          {billingAddress?.email}
                        </Typography>
                      </Grid> */}
                          <Grid item sm={3} lg={3} xs={3}>
                            <Typography fontWeight="300" color="disabled">
                              Address:
                            </Typography>
                          </Grid>
                          <Grid item sm={9} lg={9} xs={9}>
                            <Typography fontWeight="600">
                              {`${shippingAddress?.street_address}, ${shippingAddress?.thana?.name},${shippingAddress?.city?.name}-${shippingAddress?.zip_code} `}
                            </Typography>
                          </Grid>

                          <Grid item sm={3} lg={3} xs={3}>
                            <Button
                              variant="outlined"
                              color="primary"
                              size="extraSmall"
                              style={{ color: "black" }}
                              onClick={() => {
                                setOpenShippingForm(true);
                              }}
                            >
                              Edit
                            </Button>
                          </Grid>
                        </Grid>
                      </Card1>
                    </>
                  )}
              </Grid>
            </Grid>
          </div>

          {openShippingForm && isChecked && (
            <>
              <div>
                <Typography fontWeight="600" mb="1rem">
                  Shipping Address
                </Typography>
              </div>
              <Grid container spacing={7}>
                <Grid item sm={6} xs={12}>
                  <TextField
                    name="name_shipping"
                    label="Full Name"
                    fullwidth
                    mb="1rem"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    value={values.name_shipping || ""}
                    errorText={touched.name_shipping && errors.name_shipping}
                  />
                  <TextField
                    name="phone_shipping"
                    label="Phone Number"
                    fullwidth
                    mb="1rem"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    value={values.phone_shipping || ""}
                    errorText={touched.phone_shipping && errors.phone_shipping}
                  />
                  <Select
                    mb="1rem"
                    label="City"
                    placeholder="Select City"
                    options={citiesShipping}
                    value={values.city_shipping || ""}
                    onChange={(city: any) => {
                      setFieldValue("city_shipping", city?.id);
                      setFieldValue("thana_shipping", "");
                      handleThanaShipping(city);
                    }}
                    errorText={touched.city_shipping && errors.city_shipping}
                  />
                  <TextField
                    name="zip_code_shipping"
                    label="Zip Code"
                    type="number"
                    fullwidth
                    mb="1rem"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    value={values.zip_code_shipping || ""}
                    errorText={
                      touched.zip_code_shipping && errors.zip_code_shipping
                    }
                  />
                </Grid>
                <Grid item sm={6} xs={12}>
                  <TextField
                    name="email_shipping"
                    label="Email Address"
                    type="email"
                    fullwidth
                    mb="1rem"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    value={values.email_shipping || ""}
                    errorText={touched.email_shipping && errors.email_shipping}
                  />
                  {/* <TextField
                name="company"
                label="Company"
                fullwidth
                mb="1rem"
                onBlur={handleBlur}
                onChange={handleChange}
                value={values.company || ""}
                errorText={touched.company && errors.company}
              /> */}
                  <Select
                    mb="1rem"
                    label="Country"
                    placeholder="Select Country"
                    options={countries}
                    value={values.country_shipping || ""}
                    onChange={(country: any) => {
                      setFieldValue("country_shipping", country?.id);
                      setFieldValue("city_shipping", "");
                      setFieldValue("thana_shipping", "");
                      handleCityShipping(country);
                    }}
                    errorText={
                      touched.country_shipping && errors.country_shipping
                    }
                  />

                  <Select
                    mb="1rem"
                    label="Thana"
                    placeholder="Select Thana"
                    options={thanasShipping}
                    value={values.thana_shipping || ""}
                    onChange={(thana: any) => {
                      setFieldValue("thana_shipping", thana?.id);
                    }}
                    errorText={touched.thana_shipping && errors.thana_shipping}
                  />
                  <TextField
                    name="street_address_shipping"
                    label="Address"
                    fullwidth
                    onBlur={handleBlur}
                    onChange={handleChange}
                    value={values.street_address_shipping || ""}
                    errorText={
                      touched.street_address_shipping &&
                      errors.street_address_shipping
                    }
                  />
                </Grid>
              </Grid>
              <div
                style={{
                  display: "flex",
                  justifyContent: "flex-end",
                  marginTop: "10px",
                }}
              >
                <Button
                  variant="outlined"
                  color="primary"
                  onClick={() => setOpenShippingForm(false)}
                >
                  Cancel
                </Button>
                <Button
                  variant="contained"
                  color="primary"
                  ml="5px"
                  onClick={() => {
                    if (getBillingByOrder) {
                      handleEditShippingAddress();
                    } else {
                      handleSaveShippingAddress();
                    }
                  }}
                >
                  Save
                </Button>
              </div>
            </>
          )}
        </Card1>
      </form>
    </>
  );
};

const initialValues = {
  name: "",
  email: "",
  phone: "",
  // company: "",
  zip_code: "",
  country: "",
  city: "",
  thana: "",
  street_address: "",
  is_different_shipping_address: false,
};

const checkoutSchema = yup.object().shape({
  street_address: yup.string().required("Address is required"),
  email: yup.string().required("Email is required"),
  zip_code: yup.string().required("Zip Code is required"),
  phone: yup.string().required("Phone Number is required"),
});

export default CheckoutForm;
