import Currency from "@component/Currency";
import React, { useEffect } from "react";
import Button from "../buttons/Button";
import { Card1 } from "../Card1";
import Divider from "../Divider";
import FlexBox from "../FlexBox";
import TextField from "../text-field/TextField";
import Typography from "../Typography";
import Grid from "@component/grid/Grid";
import Box from "@component/Box";
import { useFormik } from "formik";
import useUserInf from "@customHook/useUserInf";
import axios from "axios";
import { Customer_Order_Comment } from "@data/constants";
import TextArea from "@component/textarea/TextArea";

export interface CheckoutSummaryProps {
  Subtotal: string | number;
  Shipping: string | number;
  Tax: string | number;
  Discount: string | number;
  commentText: string;
  confurmSubmitForm: boolean;
}

const CheckoutSummary: React.FC<CheckoutSummaryProps> = ({
  Subtotal,
  Shipping,
  Tax,
  Discount,
  commentText,
  confurmSubmitForm,
}) => {
  const { order_Id, authTOKEN, isLogin } = useUserInf();

  const initialValues = {
    comment: "",
  };
  const checkoutSchema = null;
  const handleFormSubmit = () => {};

  const {
    values,
    errors,
    touched,
    handleChange,
    handleBlur,
    // handleSubmit,
    setFieldValue,
  } = useFormik({
    initialValues: initialValues,
    validationSchema: checkoutSchema,
    onSubmit: handleFormSubmit,
  });

  useEffect(() => {
    if (order_Id) {
      setFieldValue("comment", commentText || "");
    }
  }, [order_Id, setFieldValue, commentText]);

  useEffect(() => {
    if (confurmSubmitForm === true) {
      addComment();
    }
  }, [confurmSubmitForm]);
  const addComment = () => {
    if (isLogin) {
      if (order_Id && values.comment) {
        const comment = {
          comment: values.comment,
        };

        axios
          .post(`${Customer_Order_Comment}${order_Id}`, comment, authTOKEN)
          .then(() => {})
          .catch(() => {});
      }
    }
  };
  return (
    <Card1>
      <FlexBox justifyContent="space-between" alignItems="center" mb="0.5rem">
        <Typography color="text.hint">Subtotal:</Typography>
        <FlexBox alignItems="flex-end">
          <Typography fontSize="18px" fontWeight="600" lineHeight="1">
            {Subtotal ? <Currency>{Subtotal}</Currency> : `_`}
          </Typography>
          <Typography fontWeight="600" fontSize="14px" lineHeight="1">
            {/* 00 */}
          </Typography>
        </FlexBox>
      </FlexBox>
      <FlexBox justifyContent="space-between" alignItems="center" mb="0.5rem">
        <Typography color="text.hint">Shipping:</Typography>
        <FlexBox alignItems="flex-end">
          <Typography fontSize="18px" fontWeight="600" lineHeight="1">
            {Shipping ? <Currency>{Shipping}</Currency> : `_`}
          </Typography>
        </FlexBox>
      </FlexBox>
      <FlexBox justifyContent="space-between" alignItems="center" mb="0.5rem">
        <Typography color="text.hint">Tax:</Typography>
        <FlexBox alignItems="flex-end">
          <Typography fontSize="18px" fontWeight="600" lineHeight="1">
            {Tax ? <Currency>{Tax}</Currency> : `_`}
          </Typography>
          <Typography fontWeight="600" fontSize="14px" lineHeight="1">
            {/* 00 */}
          </Typography>
        </FlexBox>
      </FlexBox>
      <FlexBox justifyContent="space-between" alignItems="center" mb="1rem">
        <Typography color="text.hint">Discount:</Typography>
        <FlexBox alignItems="flex-end">
          <Typography fontSize="18px" fontWeight="600" lineHeight="1">
            {Discount ? <Currency>{Discount}</Currency> : `_`}
          </Typography>
        </FlexBox>
      </FlexBox>

      <Divider mb="1rem" />
      <FlexBox justifyContent="flex-end">
        <Typography
          fontSize="25px"
          fontWeight="600"
          lineHeight="1"
          // textAlign="right"
          mb="1.5rem"
        >
          {Subtotal ? <Currency>{Subtotal}</Currency> : `_`}
        </Typography>
      </FlexBox>
      <Grid container spacing={4}>
        <Grid sm={6} lg={8} xs={6}>
          <TextField placeholder="Voucher" fullwidth />{" "}
        </Grid>
        <Grid sm={6} lg={4} xs={6}>
          <Button
            variant="outlined"
            color="primary"
            // mt="1rem"
            mb="30px"
            ml="10px"
            fullwidth
          >
            Apply Voucher
          </Button>
        </Grid>
        {/* <Grid sm={12} lg={12} xs={12}>
          
        </Grid> */}
        {/* <Grid sm={12} lg={12} xs={12}>
          
        </Grid>
        <Grid sm={6} lg={6} xs={6}>
        
        </Grid> */}
      </Grid>
      <FlexBox alignItems="center" mb="1rem">
        <Typography fontWeight="600" mr="10px">
          Additional Comments
        </Typography>
        <Box p="3px 10px" bg="primary.light" borderRadius="3px">
          <Typography fontSize="12px" color="primary.main">
            Note
          </Typography>
        </Box>
      </FlexBox>
      <TextArea
        rows={4}
        name="comment"
        onBlur={handleBlur}
        onChange={handleChange}
        value={values.comment || ""}
        errorText={touched.comment && errors.comment}
        fullwidth
        mb="1rem"
      />
      {/* <Button
        variant="contained"
        color="primary"
        disabled={!values.comment}
        onClick={addComment}
        fullwidth
      >
        Add Comment
      </Button> */}
    </Card1>
  );
};

export default CheckoutSummary;
