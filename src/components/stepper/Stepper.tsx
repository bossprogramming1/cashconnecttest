import useWindowSize from "@hook/useWindowSize";
import React, { Fragment, useEffect, useState } from "react";
import Box from "../Box";
import { Chip } from "../Chip";
import FlexBox from "../FlexBox";

type Step = {
  title: string;
  disabled: boolean;
};

type StepperProps = {
  selectedStep?: number;
  stepperList: Step[];
  onChange?: (Step, index) => void;
};

const Stepper: React.FC<StepperProps> = ({
  selectedStep,
  stepperList,
  onChange,
}) => {
  const [selected, setSelected] = useState(selectedStep - 1);
  const width = useWindowSize();
  const isMobile = width < 842;

  const handleStepClick = (step: Step, ind) => () => {
    if (!step.disabled) {
      setSelected(ind);
      if (onChange) onChange(step, ind);
    }
  };

  useEffect(() => {
    setSelected(selectedStep - 1);
  }, [selectedStep]);

  return (
    <FlexBox
      alignItems="center"
      flexWrap="wrap"
      justifyContent="center"
      my="-4px"
    >
      {stepperList.map((step, ind) => (
        <Fragment key={ind}>
          <Chip
            style={{ borderRadius: isMobile ? "13px" : "300px" }}
            bg={ind <= selected ? "primary.main" : "primary.light"}
            color={ind <= selected ? "white" : "primary.main"}
            p={isMobile ? "0.5rem .5rem" : "0.5rem 1.5rem"}
            fontSize={!isMobile ? "14px" : "10px"}
            fontWeight="600"
            my="4px"
            cursor={step.disabled ? "not-allowed" : "pointer"}
            onClick={handleStepClick(step, ind)}
          >
            {step.title}
          </Chip>
          {ind < stepperList.length - 1 && (
            <Box
              width={!isMobile ? "50px" : "27px"}
              height="4px"
              bg={ind < selected ? "primary.main" : "primary.light"}
            />
          )}
        </Fragment>
      ))}
    </FlexBox>
  );
};

Stepper.defaultProps = {
  selectedStep: 1,
};

export default Stepper;
