import { shadowOptions } from "interfaces";
import styled, { css } from "styled-components";
import {
  border,
  BorderProps,
  color,
  ColorProps,
  space,
  SpaceProps,
} from "styled-system";
import { getTheme } from "../utils/utils";

interface TableRowProps extends SpaceProps, ColorProps, BorderProps {
  boxShadow?: shadowOptions;
  conditionalStyle?: boolean;
}

const conditionalStyles = css`
  & > :nth-child(1) {
    flex: 1 1 calc(2% - 4px);
  }
  & > :nth-child(2),
  & > :nth-child(3) {
    flex: 1 1 calc(10% - 4px);
  }
  & > :nth-child(4) {
    flex: 1 1 calc(40% - 4px);
  }
`;
const TableRow = styled.div<TableRowProps>`
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  border-radius: 10px;
  box-shadow: ${({ boxShadow }) => getTheme(`shadows.${boxShadow || "small"}`)};

  & > * {
    flex: 1 1 0;
  }

  .pre {
    white-space: pre;
  }
  /* Adjust the width for each item */
  ${(props) => props.conditionalStyle && conditionalStyles}

  ${space}
  ${color}
  ${border}
`;

TableRow.defaultProps = {
  bg: "body.paper",
};

export default TableRow;
