import React, { useState } from "react";

import TableRow from "../TableRow";
import { H6 } from "../Typography";

export interface BranchRowProps {
  item: {
    name?: any;
    map_location?: any;
    street_address_one?: any;
    street_address_two?: any;
    thana: {
      name: any;
    };
    city: {
      name: any;
    };
    country: {
      name: any;
    };
  };
  ind: any;
}

const BranchRow: React.FC<BranchRowProps> = ({ item, ind }) => {
  const [isHover, setIsHover] = useState(false);

  const bgStyle = {
    backgroundColor: isHover ? "#eee6e8" : "#ffffff",
  };
  const handleMouseEnter = () => {
    setIsHover(true);
  };
  const handleMouseLeave = () => {
    setIsHover(false);
  };
  console.log("itemBrnach", item);
  return (
    <TableRow
      as="a"
      my="1rem"
      padding="6px 18px"
      style={bgStyle}
      onMouseEnter={handleMouseEnter}
      onMouseLeave={handleMouseLeave}
      conditionalStyle={true}
    >
      <H6 m="6px" textAlign="left">
        {ind + 1}
      </H6>
      <H6 m="6px" textAlign="left">
        {item.name}
      </H6>
      <H6 m="6px" textAlign="left">
        {`${item.street_address_one} ${item.street_address_two || ""}`}
      </H6>
      {item?.map_location ? (
        <iframe
          src={item?.map_location}
          height="150"
          style={{ border: 0 }}
          loading="lazy"
          referrerPolicy="no-referrer-when-downgrade"
        ></iframe>
      ) : (
        <span style={{ width: 162 }}>No location found</span>
      )}

      {/* <H6 className="flex-grow pre" m="6px" textAlign="left">
          {item?.created_at &&
            format(new Date(item?.created_at), "MMM dd, yyyy")}
        </H6> */}

      {/* <Hidden flex="0 0 0 !important" down={769}>
        <Typography textAlign="center" color="text.muted">
          <IconButton size="small">
            <Icon variant="small" defaultcolor="currentColor">
              arrow-right
            </Icon>
          </IconButton>
        </Typography>
      </Hidden> */}
    </TableRow>
  );
};

export default BranchRow;
