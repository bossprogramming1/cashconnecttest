import PaginationRow from "@component/pagination/PaginationRow";
import ShowingItemNumber from "@component/pagination/ShowingItemNumber";
import { Branch_All } from "@data/constants";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import FlexBox from "../FlexBox";
import Hidden from "../hidden/Hidden";
import DashboardPageHeader from "../layout/DashboardPageHeader";
import Pagination from "../pagination/Pagination";
import TableRow from "../TableRow";
import { H5, SemiSpan } from "../Typography";
import BranchRow from "./BranchRow";

export interface BranchListProps {
  isFind: boolean;
  nearestBranch: any;
}

const BranchList: React.FC<BranchListProps> = ({ isFind, nearestBranch }) => {
  const [branchsList, setBranchList] = useState([]);
  const [totalBranch, setTotalBranch] = useState(0);
  const [totalPage, setTotalPage] = useState(0);

  const router = useRouter();

  const { size, page } = router.query;

  const product_per_page_options = [
    { id: 10, name: 10 },
    { id: 30, name: 30 },
    { id: 50, name: 50 },
  ];

  useEffect(() => {
    console.log("Fetching data...");
    if (!isFind) {
      fetch(`${Branch_All}?size=${size || 10}&page=${page || 1}`, {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          Authorization: localStorage.getItem("jwt_access_token"),
        },
      })
        .then((response) => {
          if (!response.ok) {
            throw new Error(`HTTP error! Status: ${response.status}`);
          }
          return response.json();
        })
        .then((branch) => {
          console.log("API response:", branch);
          if (branch?.branches) {
            console.log("Setting data to state...");
            setBranchList(branch?.branches);
            setTotalBranch(branch?.total_elements);
            setTotalPage(branch?.total_pages);
          }
        })
        .catch((error) => {
          console.error("Error fetching data:", error);
        });
    } else {
      if (nearestBranch.length !== 0) {
        setBranchList(nearestBranch);
      }
    }
  }, [size, page, isFind, nearestBranch]);

  console.log("branchsList", branchsList);
  return (
    <div>
      <DashboardPageHeader title="Branch List" iconName="map-pin-2" />

      <Hidden down={769}>
        <TableRow
          padding="0px 18px"
          boxShadow="none"
          bg="none"
          conditionalStyle={true}
        >
          <H5 color="text.muted" my="0px" mx="6px" textAlign="left">
            SL No
          </H5>
          <H5 color="text.muted" my="0px" mx="6px" textAlign="left">
            Name
          </H5>
          <H5 color="text.muted" my="0px" mx="6px" textAlign="left">
            Addess
          </H5>
          <H5 color="text.muted" my="0px" mx="6px" textAlign="left">
            Map
          </H5>
        </TableRow>
      </Hidden>

      {branchsList?.map((item, ind) => (
        <BranchRow item={item} ind={ind} key={item?.id || ind} />
      ))}

      <FlexBox
        flexWrap="wrap"
        justifyContent="space-around"
        alignItems="center"
        mt="32px"
      >
        <SemiSpan>
          Showing{" "}
          <ShowingItemNumber initialNumber={10} totalItem={totalBranch} /> of{" "}
          {totalBranch} Branchs
        </SemiSpan>

        <Pagination pageCount={totalPage} />

        <PaginationRow
          product_per_page_option={product_per_page_options}
          name="Branch"
        />
      </FlexBox>
    </div>
  );
};

export default BranchList;
