// import useFormattedProductData from "@customHook/useFormattedProductData";
import getFormattedProductData from "@helper/getFormattedProductData";
import React from "react";
import Box from "../Box";
import Grid from "../grid/Grid";
import ProductCard1 from "../product-cards/ProductCard1";
import { H3 } from "../Typography";
import { useRouter } from "next/router";

export interface RelatedProductsProps {
  relatedProduct: any[];
}

const RelatedProducts: React.FC<RelatedProductsProps> = ({
  relatedProduct,
}) => {
  const params = useRouter();
  // Check if params.query.id is an array and extract the first element
  const id = Array.isArray(params.query.id)
    ? params.query.id[0]
    : params.query.id;

  // Convert id to a number
  const parsedId: number = parseInt(id);
  const filteredRelatedProducts = relatedProduct?.filter(
    (product) => product.id !== parsedId
  );
  const relatedProducts = getFormattedProductData(filteredRelatedProducts);

  return (
    <Box mb="2rem">
      <H3 mb="1.5rem">Related Products</H3>
      <Grid container spacing={6}>
        {relatedProducts?.map((item, ind) => (
          <Grid item lg={2.4} md={4} sm={6} xs={6} key={item?.id || ind}>
            <ProductCard1 {...item} />
          </Grid>
        ))}
      </Grid>
    </Box>
  );
};

export default RelatedProducts;
