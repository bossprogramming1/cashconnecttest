import PaginationRow from "@component/pagination/PaginationRow";
import ShowingItemNumber from "@component/pagination/ShowingItemNumber";
import useFormattedProductData from "@customHook/useFormattedProductData";
import React, { useEffect } from "react";
import FlexBox from "../FlexBox";
import Grid from "../grid/Grid";
import Pagination from "../pagination/Pagination";
import ProductCard1 from "../product-cards/ProductCard1";
import { SemiSpan } from "../Typography";
import Image from "@component/Image";

export interface ProductCard1ListProps {
  productList?: any;
  totalPage?: number;
  totalProduct?: number;
}

const ProductCard1List: React.FC<ProductCard1ListProps> = ({
  productList,
  totalPage,
  totalProduct,
}) => {
  const [formattedProductData, setFormattedProductData] =
    useFormattedProductData([]);

  useEffect(() => {
    setFormattedProductData(productList);
  }, [productList]);

  return (
    <div>
      <Grid container spacing={6}>
        {productList.length !== 0 ? (
          formattedProductData?.map((item, ind) => {
            console.log("formattedProductData", item);
            return (
              <Grid item lg={3} md={3} sm={4} xs={6} key={item?.id || ind}>
                <ProductCard1 {...item} />
              </Grid>
            );
          })
        ) : (
          <Grid item lg={12} md={12} sm={12} xs={12} key={1}>
            <Image
              maxWidth={"220px"}
              margin="auto"
              src={"/assets/images/sorry.png"}
              alt="sorry_image"
            />
          </Grid>
        )}
      </Grid>

      {productList.length !== 0 && (
        <FlexBox
          flexWrap="wrap"
          justifyContent="space-around"
          alignItems="center"
          mt="32px"
        >
          <SemiSpan>
            Showing{" "}
            <ShowingItemNumber initialNumber={12} totalItem={totalProduct} /> of{" "}
            {totalProduct} Products
          </SemiSpan>

          <Pagination pageCount={totalPage} />

          <PaginationRow />
        </FlexBox>
      )}
    </div>
  );
};

export default ProductCard1List;
