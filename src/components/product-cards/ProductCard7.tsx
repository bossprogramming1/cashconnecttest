// import Box from "@component/Box";
import Currency from "@component/Currency";
// import Image from "@component/Image";
// import LoginPopup from "@component/LoginPopup";
import { useAppContext } from "@context/app/AppContext";
import useUserInf from "@customHook/useUserInf";
import {
  BASE_URL,
  Check_Stock,
  Customer_decrease_Quantity,
  Customer_Increase_Quantity,
  Customer_Order_Remove_Item,
} from "@data/constants";
import axios from "axios";
import Link from "next/link";
import React, { useCallback, useEffect, useState } from "react";
import { SpaceProps } from "styled-system";
import Button from "../buttons/Button";
// import IconButton from "../buttons/IconButton";
import { useRouter } from "next/router";
import FlexBox from "../FlexBox";
import Icon from "../icon/Icon";
import Typography, { H2, Tiny } from "../Typography";
import { StyledProductCard7 } from "./ProductCardStyle";
import Image from "@component/Image";
import Grid from "@component/grid/Grid";
import useWindowSize from "@hook/useWindowSize";
import Modal from "@component/modal/Modal";
import Card from "@component/Card";
import Box from "@component/Box";

export interface ProductCard7Props {
  id: string | number;
  quantity: any;
  price: number;
  size: any;
  color_image: any;
  color: any;
  product: any;
  condition: string;
  runReloadCart: () => void;
  openModal: () => void;
  // removeCart: () => void;
}

const ProductCard7: React.FC<ProductCard7Props & SpaceProps> = ({
  id,
  quantity,
  price,
  product,
  condition,
  size,
  color_image,
  color,
  runReloadCart,
  openModal,
  // removeCart,
  ...props
}) => {
  const { dispatch, state } = useAppContext();

  // const [openLogin, setOpenLogin] = useState(false);
  const [open, setOpen] = useState(false);
  const [stock, setStock] = useState(true);
  const [stockQuantity, setStockQuantity] = useState(0);
  const [loading, setLoading] = useState(false);
  const router = useRouter();

  const cartCanged = state.cart.chartQuantity;

  const { user_id, order_Id, isLogin, authTOKEN } = useUserInf();
  const width = useWindowSize();
  const isMobile = width < 768;

  // const toggleModal = useCallback(() => {
  //   setOpen((open) => !open);
  // }, []);

  useEffect(() => {
    axios
      .get(`${Check_Stock}${product?.id}`)
      .then((res) => {
        setStockQuantity(res.data.in_stock);
        setStock(res.data.is_in_stock);
      })
      .catch(() => {});
  }, [cartCanged]);

  // const closeLoginTab = () => {
  //   setOpenLogin(false);
  // };

  const handleCartAmountChange = useCallback(
    (action) => () => {
      if (isLogin) {
        const item_id = id;
        const orderData = {
          product_id: product?.id,
          quantity: 1,
          price: price,
          branch_id: 1,
          user_id: user_id,
          size: size?.id,
          color: color?.id,
        };
        console.log("size and color", size, color);
        if (action == "remove") {
          axios
            .delete(
              `${Customer_Order_Remove_Item}${order_Id}/${item_id}`,
              authTOKEN
            )
            .then(() => {
              runReloadCart();
              // toggleDialog();

              dispatch({
                type: "CHANGE_CART_QUANTITY",
                payload: {
                  chartQuantity: Math.random(),
                  prductId: product?.id,
                },
              });
              openModal();
            })
            .catch(() => {});
        } else if (action == "increase") {
          setLoading(true);

          axios
            .put(
              `${Customer_Increase_Quantity}${order_Id}/${item_id}`,
              orderData,
              authTOKEN
            )
            .then(() => {
              runReloadCart();
              dispatch({
                type: "CHANGE_CART_QUANTITY",
                payload: {
                  chartQuantity: Math.random(),
                  prductId: product?.id,
                },
              });
              setLoading(false);
            })
            .catch(() => {
              setLoading(false);
            });
        } else if (action == "decrease") {
          setLoading(true);

          axios
            .put(
              `${Customer_decrease_Quantity}${order_Id}/${item_id}`,
              orderData,
              authTOKEN
            )
            .then(() => {
              runReloadCart();
              dispatch({
                type: "CHANGE_CART_QUANTITY",
                payload: {
                  chartQuantity: Math.random(),
                  prductId: product?.id,
                },
              });
              setLoading(false);
            })
            .catch(() => {
              setLoading(false);
            });
        }
      } else {
        router.push("/");
      }
    },

    [user_id, order_Id, isLogin, authTOKEN]
  );

  return (
    <>
      {loading && (
        <div
          style={{
            position: "fixed",
            height: "100%",
            width: "100%",
            top: "0px",
            left: "0px",
            display: "flex",
            justifyContent: "center",
            // backgroundColor: "#ffffff",
            alignItems: "center",
            zIndex: 100,
          }}
        >
          <Image
            style={{
              height: "100px",
              width: "100px",
              marginTop: "100pz",
            }}
            src="/assets/images/gif/loading.gif"
          />
        </div>
      )}
      {/* <LoginPopup open={openLogin} closeLoginDialog={closeLoginTab} /> */}
      <StyledProductCard7 {...props}>
        <Grid container spacing={30}>
          <Grid item sm={4} xs={4} lg={3}>
            <Image
              style={{
                width: "100px",
                margin: "auto",
                height: "100px",
              }}
              alt={product.name}
              src={`${BASE_URL}${
                color_image.image
                  ? color_image.image
                  : product?.product?.thumbnail
              }`}
            />
          </Grid>
          <Grid item sm={8} xs={8} lg={9}>
            <div>
              <Link
                href={`/product/${product.id}`}
                as={`/product/${product.id}`}
              >
                <a>
                  <Typography
                    className="title"
                    fontWeight="600"
                    fontSize="18px"
                    mb="0.5rem"
                    style={{ overflowWrap: "break-word" }}
                  >
                    {product.name.length > 25
                      ? `${product.name?.slice(0, 25)}..`
                      : product.name}
                  </Typography>
                </a>
              </Link>
            </div>
            <div>
              <FlexBox
                // width="100%"
                alignItems="flex-end"
              >
                <FlexBox flexWrap="wrap" alignItems="center">
                  <Typography color="gray.600" mr="0.5rem" display="flex">
                    <Currency>{Number(price).toFixed(2)}</Currency> x {quantity}
                  </Typography>
                  <Typography fontWeight={600} color="primary.main" mr="1rem">
                    <Currency>{(price * quantity).toFixed(2)}</Currency>
                  </Typography>
                </FlexBox>
              </FlexBox>
              {color?.name && (
                <Tiny color="text.muted">
                  {`${color?.name} ${
                    color?.name && size?.name ? `[${size?.name}]` : ""
                  }`}
                </Tiny>
              )}
            </div>
            <div>
              <FlexBox alignItems="center" mt="5px">
                <Button
                  variant="outlined"
                  color="primary"
                  padding="5px"
                  size="none"
                  borderColor="primary.light"
                  onClick={handleCartAmountChange("decrease")}
                  disabled={quantity === 1}
                >
                  <Icon variant="small">minus</Icon>
                </Button>
                <Typography mx="0.5rem" fontWeight="600" fontSize="15px">
                  {quantity}
                </Typography>
                <Button
                  variant="outlined"
                  color="primary"
                  padding="5px"
                  size="none"
                  borderColor="primary.light"
                  disabled={!stock || stockQuantity == 0}
                  onClick={handleCartAmountChange("increase")}
                >
                  <Icon variant="small">plus</Icon>
                </Button>
                <Icon
                  ml="30px"
                  color="primary"
                  variant="medium"
                  style={{ cursor: "pointer" }}
                  onClick={() => {
                    setOpen(true);
                  }}
                >
                  delete
                </Icon>
              </FlexBox>
            </div>
            {/* <FlexBox
                className="product-details"
                flexDirection="column"
                justifyContent="center"
                // minWidth="0px"
                // width="100%"
              >
                <Link href={`/product/${product.id}`}  as={`/product/${product.id}`}>
                  <a>
                    <Typography
                      className="title"
                      fontWeight="600"
                      fontSize="18px"
                      mb="0.5rem"
                    >
                      {product.name}
                    </Typography>
                  </a>
                </Link>
                

                <H4
                  display="flex"
                  className="title"
                  fontSize="13px"
                  fontWeight="600"
                  color={
                    condition === "new" ||
                    condition === "New" ||
                    condition === "NEW"
                      ? "primary.main"
                      : "secondary.main"
                  }
                >
                  {condition || ""}
                </H4>

                <FlexBox
                  // width="100%"
                  alignItems="flex-end"
                >
                  <FlexBox flexWrap="wrap" alignItems="center">
                    <Typography color="gray.600" mr="0.5rem" display="flex">
                      <Currency>{Number(price).toFixed(2)}</Currency> x{" "}
                      {quantity}
                    </Typography>
                    <Typography fontWeight={600} color="primary.main" mr="1rem">
                      <Currency>{(price * quantity).toFixed(2)}</Currency>
                    </Typography>
                  </FlexBox>

                  <FlexBox alignItems="center">
                    <Button
                      variant="outlined"
                      color="primary"
                      padding="5px"
                      size="none"
                      borderColor="primary.light"
                      onClick={handleCartAmountChange("decrease")}
                      disabled={quantity === 1}
                    >
                      <Icon variant="small">minus</Icon>
                    </Button>
                    <Typography mx="0.5rem" fontWeight="600" fontSize="15px">
                      {quantity}
                    </Typography>
                    <Button
                      variant="outlined"
                      color="primary"
                      padding="5px"
                      size="none"
                      borderColor="primary.light"
                      disabled={!stock || stockQuantity == 0}
                      onClick={handleCartAmountChange("increase")}
                    >
                      <Icon variant="small">plus</Icon>
                    </Button>
                  </FlexBox>
                </FlexBox>
              </FlexBox> */}
          </Grid>
        </Grid>
        <Modal open={open} onClose={() => setOpen(false)}>
          <Card p="1rem" position="relative">
            <>
              <H2 mt="10px" fontSize={isMobile ? "20px" : "25px"}>
                Are you sure want to remove?
              </H2>
              <FlexBox alignItems="center" justifyContent="center" mt="30px">
                <Button
                  color="secondary"
                  bg="secondary.light"
                  px="1rem"
                  onClick={handleCartAmountChange("remove")}
                >
                  Remove
                </Button>
                <Button
                  color="primary"
                  bg="primary.light"
                  px="1rem"
                  ml="20px"
                  onClick={() => setOpen(false)}
                >
                  Cancel
                </Button>
              </FlexBox>
            </>

            <Box
              position="absolute"
              top="0.75rem"
              right="0.75rem"
              cursor="pointer"
            >
              <Icon
                className="close"
                color="primary"
                variant="small"
                onClick={() => setOpen(false)}
              >
                close
              </Icon>
            </Box>
          </Card>
        </Modal>
      </StyledProductCard7>
    </>
  );
};

export default ProductCard7;
