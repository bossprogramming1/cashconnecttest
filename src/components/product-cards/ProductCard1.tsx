import Currency from "@component/Currency";
import LazyImage from "@component/LazyImage";
import ProductIntro from "@component/products/ProductIntro";
// import { useAppContext } from "@context/app/AppContext";
// import useUserInf from "@customHook/useUserInf";
// import {
//   Check_Stock,
//   Customer_decrease_Quantity,
//   Customer_Increase_Quantity,
//   Customer_Order_Create,
//   Customer_Order_Item_By_Product_Id,
//   Customer_Order_Remove_Item,
//   // Product_Discount_By_Id,
// } from "@data/constants";
// import axios from "axios";
import Link from "next/link";
// import { useRouter } from "next/router";
import React, { useCallback, useState } from "react";
import { CSSProperties } from "styled-components";
import Box from "../Box";
// import Button from "../buttons/Button";
import Card, { CardProps } from "../Card";
import { Chip } from "../Chip";
import FlexBox from "../FlexBox";
import Icon from "../icon/Icon";
import Modal from "../modal/Modal";
import Rating from "../rating/Rating";
import { H3, H4, SemiSpan } from "../Typography";
import { StyledProductCard1 } from "./ProductCardStyle";
// import useWindowSize from "@hook/useWindowSize";
import Button from "@component/buttons/Button";

export interface ProductCard1Props extends CardProps {
  className?: string;
  style?: CSSProperties;
  imgUrl?: string;
  short_desc?: string;
  title?: string;
  flash?: string;
  price?: number;
  orginalPrice?: number;
  rating?: number;
  offPercent?: number;
  offValue?: number;
  id?: string | number;
  brand?: string | number;
  reviewCount?: string | number;
  condition?: string;
  stock?: boolean | null;
}

const ProductCard1: React.FC<ProductCard1Props> = ({
  id,
  imgUrl,
  short_desc,
  title,
  price,
  orginalPrice,
  brand,
  rating,
  reviewCount,
  condition,
  flash,
  offPercent,
  offValue,
  stock,
  ...props
}) => {
  const [open, setOpen] = useState(false);

  // const [sellablePrice, setsellablePrice] = useState(Number(price));
  // const [orginalPrice, setorginalPrice] = useState(0);
  // const [discountedPercent, setdiscountedPercent] = useState(0);
  // const [discountedValue, setdiscountedValue] = useState(0);
  console.log(
    "product Details",
    stock,
    id,
    imgUrl,
    short_desc,
    title,
    price,
    orginalPrice,
    brand,
    rating,
    reviewCount,
    condition,
    flash,
    offPercent,
    offValue,
    stock
  );
  // const router = useRouter();

  // const { dispatch, state } = useAppContext();

  // const [cartQuantity, setCartQuantity] = useState(0);
  // const [itemId, setItemId] = useState(0);
  // const [loading, setLoading] = useState(false);
  // const [stockQuantity, setStockQuantity] = useState(null);
  // const cartCanged = state.cart.chartQuantity;
  // const {  authTOKEN } = useUserInf();
  // const width = useWindowSize();

  // const isMobile = width < 768;

  const toggleDialog = useCallback(() => {
    setOpen((open) => !open);
  }, []);

  // useEffect(() => {
  //   if (cartCanged) {
  //     axios
  //       .get(`${Check_Stock}${id}`)

  //       .then((res) => {
  //         setStockQuantity(res.data.in_stock);
  //       })
  //       .catch(() => {});
  //   }
  // }, [cartCanged, id]);

  // useEffect(() => {
  //   axios
  //     .get(`${Product_Discount_By_Id}${id}`)
  //     .then((res) => {
  //       if (res.data.discounts?.discounted_price) {
  //         if (res.data.discounts?.discount_type === "percentage") {
  //           setsellablePrice(res.data.discounts?.discounted_price);
  //           setorginalPrice(Number(res.data.discounts?.product.unit_price));
  //           setdiscountedPercent(res.data.discounts?.discount_percent);
  //         } else {
  //           setsellablePrice(res.data.discounts?.discounted_price);
  //           setorginalPrice(Number(res.data.discounts?.product.unit_price));
  //           setdiscountedValue(res.data.discounts?.discount_value);
  //         }
  //       }
  //     })
  //     .catch(() => {});
  // }, [id]);

  // useEffect(() => {
  //   const orderId = localStorage.getItem("OrderId");

  //   if (orderId) {
  //     axios
  //       .get(`${Customer_Order_Item_By_Product_Id}${orderId}/${id}`, authTOKEN)
  //       .then((item) => {
  //         setItemId(item?.data?.order_item?.id);
  //         setCartQuantity(item?.data?.order_item?.quantity);
  //
  //       })
  //       .catch(() => setCartQuantity(0));
  //   }
  // }, [id, authTOKEN]);

  // useEffect(() => {
  //   const orderId = localStorage.getItem("OrderId");
  //   if (id && orderId) {
  //     if (state.cart.prductId == id && cartCanged) {
  //       axios
  //         .get(
  //           `${Customer_Order_Item_By_Product_Id}${orderId}/${id}`,
  //           authTOKEN
  //         )
  //         .then((item) => {
  //           setItemId(item?.data?.order_item?.id);
  //           setCartQuantity(item?.data?.order_item?.quantity);

  //         })
  //         .catch(() => {

  //           setCartQuantity(0);
  //         });
  //     }
  //   }
  // }, [cartCanged, id]);
  // useEffect(() => {
  //   const orderId = localStorage.getItem("OrderId");

  //   const fetchOrderItem = async () => {
  //     try {
  //       const response = await axios.get(
  //         `${Customer_Order_Item_By_Product_Id}${orderId}/${id}`,
  //         authTOKEN
  //       );
  //       const orderItem = response?.data?.order_item;

  //       if (orderItem) {
  //         setItemId(orderItem.id);
  //         setCartQuantity(orderItem.quantity);
  //       } else {
  //         setCartQuantity(0);
  //       }
  //     } catch (error) {
  //       setCartQuantity(0);
  //     }
  //   };

  //   if (orderId && id) {
  //     fetchOrderItem();
  //   }
  // }, [id, authTOKEN]);

  // useEffect(() => {
  //   const orderId = localStorage.getItem("OrderId");

  //   if (
  //     (id && orderId && state.cart.prductId === id && cartCanged) ||
  //     (id && orderId && cartCanged)
  //   ) {
  //     const fetchOrderItem = async () => {
  //       try {
  //         const response = await axios.get(
  //           `${Customer_Order_Item_By_Product_Id}${orderId}/${id}`,
  //           authTOKEN
  //         );
  //         const orderItem = response?.data?.order_item;

  //         if (orderItem) {
  //           setItemId(orderItem.id);
  //           setCartQuantity(orderItem.quantity);
  //         } else {
  //           setCartQuantity(0);
  //         }
  //       } catch (error) {
  //         setCartQuantity(0);
  //       }
  //     };

  //     fetchOrderItem();
  //   }
  // }, [cartCanged, id, state.cart.prductId, authTOKEN]);
  /// *** remove plus button as new requirements ***\\\
  // const handleCartAmountChange = (amount, action) => {
  //   const orderId = localStorage.getItem("OrderId");

  //   const dateObj: any = new Date();
  //   const currentDate =
  //     dateObj.getFullYear() +
  //     "-" +
  //     (dateObj.getMonth() + 1).toString().padStart(2, 0) +
  //     "-" +
  //     dateObj.getDate().toString().padStart(2, 0);

  //   const orderData = {
  //     product: id,
  //     quantity: 1,
  //     price: price,
  //     order_date: currentDate,
  //     branch: 1,
  //     user: user_id,
  //   };

  //   //add to cart
  //   if (action == "increase" && amount == 1) {
  //     // setLoading(true);
  //     dispatch({
  //       type: "CHANGE_ALERT",
  //       payload: {
  //         alertLoading: true,
  //       },
  //     });

  //     if (user_id) {
  //       axios
  //         .post(`${Customer_Order_Create}`, orderData, authTOKEN)
  //         .then((res) => {
  //           localStorage.setItem("OrderId", res.data.order_details.id);
  //           dispatch({
  //             type: "CHANGE_CART_QUANTITY",
  //             payload: { chartQuantity: Math.random(), prductId: id },
  //           });
  //           // setLoading(false);
  //           dispatch({
  //             type: "CHANGE_ALERT",
  //             payload: {
  //               alertLoading: false,
  //             },
  //           });
  //         })
  //         .catch(() => {
  //           // setLoading(false);
  //           dispatch({
  //             type: "CHANGE_ALERT",
  //             payload: {
  //               alertLoading: false,
  //             },
  //           });
  //         });
  //     } else {
  //       localStorage.setItem("backAfterLogin", `/product/${id}`);
  //       router.push({
  //         pathname: "/login",
  //       });
  //       // .then(() => router.reload());
  //     }
  //   }

  //   //increase
  //   else if (action == "increase") {
  //     // setLoading(true)

  //     dispatch({
  //       type: "CHANGE_ALERT",
  //       payload: {
  //         alertLoading: true,
  //       },
  //     });

  //     axios
  //       .put(
  //         `${Customer_Increase_Quantity}${orderId}/${itemId}`,
  //         orderData,
  //         authTOKEN
  //       )
  //       .then(() => {
  //         dispatch({
  //           type: "CHANGE_CART_QUANTITY",
  //           payload: {
  //             chartQuantity: Math.random(),
  //             prductId: id,
  //           },
  //         });
  //         // setLoading(false);
  //         dispatch({
  //           type: "CHANGE_ALERT",
  //           payload: {
  //             alertLoading: false,
  //           },
  //         });
  //       })
  //       .catch(() => {
  //         // setLoading(false);
  //         dispatch({
  //           type: "CHANGE_ALERT",
  //           payload: {
  //             alertLoading: false,
  //           },
  //         });
  //       });
  //   }

  //   //romove
  //   else if (amount == 0 && action == "decrease") {
  //     // setLoading(true);
  //     dispatch({
  //       type: "CHANGE_ALERT",
  //       payload: {
  //         alertLoading: true,
  //       },
  //     });

  //     axios
  //       .delete(`${Customer_Order_Remove_Item}${orderId}/${itemId}`, authTOKEN)
  //       .then(() => {
  //         // setLoading(false);
  //         dispatch({
  //           type: "CHANGE_ALERT",
  //           payload: {
  //             alertLoading: false,
  //           },
  //         });

  //         dispatch({
  //           type: "CHANGE_CART_QUANTITY",
  //           payload: { chartQuantity: Math.random() },
  //         });
  //       })
  //       .catch(() => {
  //         // setLoading(false);
  //         dispatch({
  //           type: "CHANGE_ALERT",
  //           payload: {
  //             alertLoading: false,
  //           },
  //         });
  //       });
  //   }

  //   //decrease
  //   else if (action == "decrease") {
  //     // setLoading(true);
  //     dispatch({
  //       type: "CHANGE_ALERT",
  //       payload: {
  //         alertLoading: true,
  //       },
  //     });

  //     axios
  //       .put(
  //         `${Customer_decrease_Quantity}${orderId}/${itemId}`,
  //         orderData,
  //         authTOKEN
  //       )
  //       .then(() => {
  //         // setLoading(false);
  //         dispatch({
  //           type: "CHANGE_ALERT",
  //           payload: {
  //             alertLoading: false,
  //           },
  //         });

  //         dispatch({
  //           type: "CHANGE_CART_QUANTITY",
  //           payload: {
  //             chartQuantity: Math.random(),
  //             prductId: id,
  //           },
  //         });
  //       })
  //       .catch(() => {
  //         // setLoading(false);
  //         dispatch({
  //           type: "CHANGE_ALERT",
  //           payload: {
  //             alertLoading: false,
  //           },
  //         });
  //       });
  //   }
  // };

  return (
    <StyledProductCard1 {...props}>
      {/* {loading && (
        <div
          style={{
            position: "fixed",
            height: "100%",
            width: "100%",
            top: "0px",
            left: "0px",
            display: "flex",
            justifyContent: "center",
            backgroundColor: " rgb(0 0 0 / 50%)",
            alignItems: "center",
            zIndex: 100,
          }}
        >
          <img
            style={{
              height: "100px",
              width: "100px",
              marginTop: "100pz",
            }}
            src="/assets/images/gif/loading.gif"
          />
        </div>
      )} */}
      <div className="image-holder">
        {!!offPercent && (
          <Chip
            position="absolute"
            bg="primary.main"
            color="primary.text"
            fontSize="10px"
            fontWeight="600"
            p="5px 10px"
            top="10px"
            left="10px"
            zIndex={2}
          >
            {offPercent}% off
          </Chip>
        )}
        {!!offValue && (
          <Chip
            position="absolute"
            bg="primary.main"
            color="primary.text"
            fontSize="10px"
            fontWeight="600"
            p="5px 10px"
            top="10px"
            left="10px"
            zIndex={2}
          >
            <b
              style={{
                fontWeight: 800,
                // fontSize: "20px",
              }}
            >
              ৳
            </b>
            &nbsp;
            {offValue} off
          </Chip>
        )}
        {stock === false && (
          <Icon
            style={{
              position: "absolute",
              top: 0,
              right: 0,
              zIndex: 2,
            }}
            variant="extralarge"
            color="auto"
          >
            tag
          </Icon>
        )}

        {/* {!isMobile && ( */}
        {/*
        <FlexBox className="extra-icons">
          <Icon
            color="secondary"
            variant="small"
            mb="0.5rem"
            onClick={toggleDialog}
          >
            eye-alt
          </Icon>

           <Icon className="favorite-icon outlined-icon" variant="small">
            heart
        
          </Icon>
          </FlexBox> */}
        {/* )} */}

        <Link href={`/product/${id}`} as={`/product/${id}`}>
          <a>
            <LazyImage
              src={imgUrl ? imgUrl : "/assets/images/logos/shopping-bag.svg"}
              width="100%"
              height="auto"
              layout="responsive"
              loader={() => imgUrl}
              alt={title}
            />
          </a>
        </Link>
      </div>
      <div className="details">
        {/* <FlexBox> */}
        {/* <Box flex="1 1 0" minWidth="0px" mr="0.5rem"> */}
        <Link href={`/product/${id}`} as={`/product/${id}`}>
          <a>
            <H3
              className="productTitle"
              fontSize="14px"
              textAlign="left"
              fontWeight="600"
              color="black"
              title={title}
            >
              {`${title}`}
            </H3>
          </a>
        </Link>

        <Rating value={rating || 0} outof={5} color="warn" readonly />

        {stock === false ? (
          <H3
            className="title"
            width="120px"
            fontSize="14px"
            textAlign="left"
            fontWeight="600"
            color="primary.main"
            mb="5px"
            title={title}
          >
            Out of Stock
          </H3>
        ) : (
          <SemiSpan
            fontWeight="bold"
            style={{
              color: "white",
              visibility: "hidden",
            }}
            mt="2px"
          >
            Available
          </SemiSpan>
        )}

        {/* <SemiSpan
              style={{
                visibility: "hidden",
              }}
              color="text.muted"
              fontWeight="600"
            >
              <del>
                <Currency>{orginalPrice?}</Currency>
              </del>
            </SemiSpan> */}
        <FlexBox>
          <SemiSpan mr="0.5rem" fontWeight="600" color="primary.main">
            <Currency>{price}</Currency>
          </SemiSpan>
          {!!orginalPrice && (
            <SemiSpan
              style={{ display: "flex", alignItems: "center" }}
              color="text.muted"
              fontSize="12px"
              fontWeight="600"
            >
              <del>
                <Currency>{orginalPrice}</Currency>
              </del>
            </SemiSpan>
          )}
        </FlexBox>
        {/* <FlexBox alignItems="center" mt={stock ? "10px" : "0px"}>
           
            </FlexBox> */}

        {/* </Box> */}
        <FlexBox
          // flexDirection="column-reverse"
          alignItems="flex-end"
          justifyContent="space-between"
          // width="30px"
          // mr={isMobile ? "-10px" : ""}
        >
          {/* <Button
              variant="outlined"
              className="icon-cart"
              color="primary"
              padding="3px"
              size="none"
              borderColor="primary.light"
              mr="5px"
              disabled={!stock}

              // onClick={toggleDialog}
            >
              <Icon className="favorite-icon outlined-icon" variant="small">
                heart
              </Icon>
            </Button> */}
          <H4
            display="flex"
            className="title"
            fontSize="13px"
            fontWeight="600"
            color={
              condition === "new" || condition === "New" || condition === "NEW"
                ? "primary.main"
                : "secondary.main"
            }
          >
            {condition ? condition.toUpperCase() : ""}
          </H4>
          <Button
            variant="outlined"
            className="icon-cart"
            color="primary"
            padding="1px"
            size="none"
            borderColor="primary.light"
            disabled={!stock}
            onClick={toggleDialog}
          >
            <Icon variant="medium">shopping-cart</Icon>
          </Button>
        </FlexBox>
        {/* <FlexBox
            flexDirection="column-reverse"
            alignItems="center"
            justifyContent={!!cartQuantity ? "flex-start" : "flex-start"}
            width="30px"
          >
            <Button
              variant="outlined"
              className="icon-cart"
              color="primary"
              padding="3px"
              size="none"
              borderColor="primary.light"
              disabled={!stock || stockQuantity === 0}
              onClick={() =>
                handleCartAmountChange(cartQuantity + 1, "increase")
              }
            >
              <Icon variant="small">plus</Icon>
            </Button>

            {cartQuantity ? (
              <Fragment>
                <SemiSpan color="text.primary" fontWeight="600">
                  {cartQuantity}
                </SemiSpan>
                <Button
                  variant="outlined"
                  className="icon-cart"
                  color="primary"
                  padding="3px"
                  size="none"
                  borderColor="primary.light"
                  // disabled={!stock}
                  onClick={() =>
                    handleCartAmountChange(cartQuantity - 1, "decrease")
                  }
                >
                  <Icon variant="small">minus</Icon>
                </Button>
              </Fragment>
            ) : (
              ""
            )}
          </FlexBox> */}
        {/* </FlexBox> */}
      </div>

      <Modal open={open} onClose={toggleDialog}>
        <Card p="1rem" style={{ width: "900px" }} position="relative">
          <ProductIntro
            {...props}
            imgUrl={[imgUrl]}
            short_desc={short_desc}
            title={title}
            price={price}
            orginalprice={orginalPrice}
            id={id}
            brand={brand}
            rating={rating}
            reviewCount={reviewCount}
            condition={condition}
            parse={""}
            eye={true}
          />
          <Box
            position="absolute"
            top="0.75rem"
            left="0.75rem"
            cursor="pointer"
          >
            <Icon
              className="close"
              color="primary"
              variant="small"
              onClick={toggleDialog}
            >
              close
            </Icon>
          </Box>
        </Card>
      </Modal>
    </StyledProductCard1>
  );
};

// ProductCard1.defaultProps = {
//   id: "324321",
//   title: "KSUS ROG Strix G15",
//   imgUrl: "",
//   off: 0,
//   price: 450,
//   rating: 0,
//   brand: "Unknown",
// };

export default ProductCard1;
