import Card from "@component/Card";
// import { Chip } from "@component/Chip";
import FlexBox from "@component/FlexBox";
import HoverBox from "@component/HoverBox";
import LazyImage from "@component/LazyImage";
import { SemiSpan } from "@component/Typography";
import useWindowSize from "@hook/useWindowSize";
import React from "react";

export interface ProductCard6Props {
  imgUrl: string;
  title: string;
  subtitle: string;
}

const ProductCard6: React.FC<ProductCard6Props> = ({ title, imgUrl }) => {
  const width = useWindowSize();
  const isMobile = width < 768;

  return (
    <Card
      width="100%"
      backgroundColor="none"
      position="relative"
      style={{ boxShadow: "none", height: "110px" }}
    >
      {/* <Chip
        // bg="white"
        textAlign="center"
        position="absolute"
        style={{ width: "100%" }}
        color="secondary.main"
        fontWeight="800"
        fontSize="14px"
        top="5.6rem"
        zIndex={2}
      >
        {title.length > 23 ? `${title.slice(0, 23)}..` : title}
      </Chip> */}

      <HoverBox
        style={{ marginLeft: "auto", marginRight: "auto" }}
        height={isMobile ? "60px" : "80px"}
        width={isMobile ? "60px" : "80px"}
        borderRadius={8}
      >
        <LazyImage
          src={imgUrl ? imgUrl : "/assets/images/logos/shopping-bag.svg"}
          layout="fill"
          objectFit="cover"
          loader={() => imgUrl}
        />
      </HoverBox>
      <FlexBox justifyContent="center">
        <SemiSpan
          textAlign="center"
          color="secondary.main"
          fontWeight="800"
          fontSize={isMobile ? "11px" : "14px"}
        >
          {title.length > 25 ? `${title.slice(0, 25)}..` : title}
        </SemiSpan>
      </FlexBox>
    </Card>
  );
};

export default ProductCard6;
