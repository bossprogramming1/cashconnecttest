import React from "react";
import Box from "./Box";
import CategorySectionHeader from "./CategorySectionHeader";
import Container from "./Container";
import useWindowSize from "@hook/useWindowSize";

export interface CategorySectionCreatorProps {
  iconName?: string;
  title?: string;
  seeMoreLink?: string;
}

const CategorySectionCreator: React.FC<CategorySectionCreatorProps> = ({
  iconName,
  seeMoreLink,
  title,
  children,
}) => {
  const width = useWindowSize();
  const isMobile = width < 768;
  return (
    <Box
      mb={title === "Top Categories" ? "0rem" : "1rem"}
      mt={
        title === "Top Categories" && isMobile
          ? "1rem"
          : title === "Top Categories" && !isMobile
          ? "2rem"
          : "0"
      }
      key={title}
    >
      <Container>
        {title && (
          <CategorySectionHeader
            title={title}
            seeMoreLink={seeMoreLink}
            iconName={iconName}
          />
        )}

        {children}
      </Container>
    </Box>
  );
};

export default CategorySectionCreator;
