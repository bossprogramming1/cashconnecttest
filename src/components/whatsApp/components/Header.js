import React, { useContext, useEffect, useState } from "react";
import CloseBtn from "./CloseBtn";
import HeadPhoneIcon from "./HeadPhoneIcon";
import WidgetContext from "./Context";
import Image from "@component/Image";

export default function Header(props) {
  const images = [
    "/assets/images/employee/1.jpg",
    "/assets/images/employee/2.jpg",
    "/assets/images/employee/3.jpg",
    "/assets/images/employee/4.jpg",
  ];

  const [currentImageIndex, setCurrentImageIndex] = useState(0);

  useEffect(() => {
    const intervalId = setInterval(() => {
      // Get a random index different from the current one
      let randomIndex = currentImageIndex;
      while (randomIndex === currentImageIndex) {
        randomIndex = Math.floor(Math.random() * images.length);
      }

      setCurrentImageIndex(randomIndex);
    }, 10000);

    return () => clearInterval(intervalId);
  }, [currentImageIndex]);

  const headerIconColor = props.headerIconColor
    ? props.headerIconColor
    : "rgb(100, 100, 100)";
  const headerIcon = images[currentImageIndex] ? (
    <Image
      src={images[currentImageIndex]}
      alt="chat_logo"
      // width="40"
    />
  ) : (
    <HeadPhoneIcon headerIconColor={headerIconColor} />
  );
  const headerIconBgColor = props.headerIconBgColor
    ? props.headerIconBgColor
    : "rgb(255,255,255)";
  const headerTxtColor = props.headerTxtColor
    ? props.headerTxtColor
    : "rgb(255,255,255)";
  const headerBgColor = props.headerBgColor
    ? props.headerBgColor
    : "rgb(7, 94, 84)";
  const headerTitle = props.headerTitle
    ? props.headerTitle
    : "Customer Support";
  const headerCaption = props.headerCaption ? props.headerCaption : "Online";
  const headerCaptionIcon = props.headerCaptionIcon ? (
    <Image src={props.headerCaptionIcon} alt="chat_logo" width="40" />
  ) : (
    ""
  );

  return (
    <div
      key={props}
      className="whatsapp_widget_header"
      style={{
        backgroundColor: headerBgColor,
        color: headerTxtColor,
      }}
    >
      <div className="whatsapp_widget_header_close_btn">
        <CloseBtn />
      </div>
      <div style={{ display: "flex", alignItems: "center" }}>
        <div
          className="whatsapp_widget_header_icon"
          style={{
            backgroundColor: headerIconBgColor,
          }}
        >
          {headerIcon}
        </div>
        <div>
          <div style={{ fontSize: 16 }}>{headerTitle}</div>
          <div style={{ display: "flex", alignItems: "center" }}>
            {headerCaptionIcon && (
              <span
                style={{
                  marginLeft: "-20px",
                  display: "flex",
                  marginRight: "-7px",
                }}
              >
                {headerCaptionIcon}
              </span>
            )}
            <span style={{ fontSize: 12 }}>{headerCaption}</span>
          </div>
        </div>
      </div>
    </div>
  );
}
