import React, { useContext, useEffect, useState } from "react";
import WidgetContext from "./Context";
import Icon from "./Icon";
import Pop from "./Pop";

export default function Widget(props) {
  const { isOpen, handleOpen } = useContext(WidgetContext);

  const dynamicStyles =
    props.position === "left"
      ? {
          alignItems: "flex-start",
          left: 15,
        }
      : {
          alignItems: "flex-end",
          right: 15,
        };

  const autoOpen = props.autoOpen ? props.autoOpen : false;
  const autoOpenTimer = props.autoOpenTimer ? props.autoOpenTimer : 3000;
  const widgetWidth = props.widgetWidth ? props.widgetWidth : "300px";
  const widgetWidthMobile = props.widgetWidthMobile
    ? props.widgetWidthMobile
    : "260px";

  const [timerId, setTimerId] = useState();

  // Set custom CSS variables based on widget width
  useEffect(() => {
    let root = document.querySelector(":root");
    root.style.setProperty("--widget-width", widgetWidth);
    root.style.setProperty("--widget-width-mobile", widgetWidthMobile);
  }, [widgetWidth, widgetWidthMobile]);

  // If auto open is set, start the timeout for auto-opening
  useEffect(() => {
    if (autoOpen) {
      const widgetAutoOpenTimeout = setTimeout(handleOpen, autoOpenTimer);
      setTimerId(widgetAutoOpenTimeout);
      return () => {
        clearTimeout(widgetAutoOpenTimeout);
      };
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [autoOpen, autoOpenTimer]);

  // If autoOpen is set and opened manually before setTimeout, clear the timeout
  useEffect(() => {
    if (autoOpen && isOpen) {
      clearTimeout(timerId);
    }
  }, [autoOpen, isOpen]);

  return (
    <div
      key={props}
      className="whatsapp_widget_wrapper"
      style={{
        bottom: 65,
        ...dynamicStyles,
      }}
    >
      <Pop {...props} />
      <Icon {...props} />
    </div>
  );
}
