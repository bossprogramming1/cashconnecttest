import Icon from "@component/icon/Icon";
import React, { useState } from "react";

const Footer = (props) => {
  const [isMessengerHovered, setIsMessengerHovered] = useState(false);
  const [isWhatsAppHovered, setIsWhatsAppHovered] = useState(false);

  const footerBgColor = props.footerBgColor || "rgb(245, 245, 245)";
  const btnTxtColor = props.btnTxtColor || "rgb(255, 255, 255)";
  const btnBgColor = props.btnBgColor || "rgb(79, 206, 93)";
  const phoneNo = props.phoneNo || false;
  const placeholder = props.placeholder || "Type a message..";

  const [message, setMessage] = useState(encodeURI(props.messageBoxTxt) || "");

  return (
    <div
      key={props}
      className="whatsapp_widget_footer"
      style={{
        backgroundColor: footerBgColor,
      }}
    >
      {/* Your input box (if needed) */}
      {/* {props.messageBox && (
        <div>
          <input
            type="text"
            onChange={(e) => setMessage(encodeURI(e.target.value))}
            className="whatsapp_widget_input"
            placeholder={placeholder}
            defaultValue={props.messageBoxTxt}
          />
        </div>
      )} */}

      {/* WhatsApp Button */}
      <a
        href={`https://wa.me/${phoneNo}?text=${message}`}
        onClick={(e) => {
          if (!phoneNo) {
            e.preventDefault();
            return alert("There is no phone number added to this widget!");
          }
        }}
        target="_blank"
        className="whatsapp_widget_footer_btn"
        rel="noreferrer"
        style={{
          color: isWhatsAppHovered ? "white" : btnTxtColor,
          backgroundColor: isWhatsAppHovered ? "#25d366" : btnBgColor,
          fontWeight: 600,
        }}
        onMouseEnter={() => setIsWhatsAppHovered(true)}
        onMouseLeave={() => setIsWhatsAppHovered(false)}
      >
        <Icon size="16px" defaultcolor="auto" mr="5px">
          whatsapp
        </Icon>{" "}
        WhatsApp
      </a>

      {/* Messenger Button */}
      <a
        href={`https://wa.me/${phoneNo}?text=${message}`}
        onClick={(e) => {
          if (!phoneNo) {
            e.preventDefault();
            return alert("There is no phone number added to this widget!");
          }
        }}
        target="_blank"
        className="whatsapp_widget_footer_btn"
        rel="noreferrer"
        style={{
          color: isMessengerHovered ? "white" : btnTxtColor,
          background: isMessengerHovered
            ? "linear-gradient(to right, #FF6968, #A334FA, #0695FF)"
            : btnBgColor,
          fontWeight: 600,
        }}
        onMouseEnter={() => setIsMessengerHovered(true)}
        onMouseLeave={() => setIsMessengerHovered(false)}
      >
        <Icon variant="small" defaultcolor="auto" mr="5px">
          messenger
        </Icon>{" "}
        Messenger
      </a>
    </div>
  );
};

export default Footer;
