import Currency from "@component/Currency";
import FlexBox from "@component/FlexBox";
import LazyImage from "@component/LazyImage";
import LoginPopup from "@component/LoginPopup";
import { useAppContext } from "@context/app/AppContext";
import useUserInf from "@customHook/useUserInf";
import {
  Customer_decrease_Quantity,
  Customer_Increase_Quantity,
  Customer_Order_Pending_Details,
  Customer_Order_Remove_Item,
} from "@data/constants";
import axios from "axios";
import Link from "next/link";
import React, { Fragment, useCallback, useEffect, useState } from "react";
import Button from "../buttons/Button";
import Divider from "../Divider";
import Icon from "../icon/Icon";
import Typography, { Paragraph } from "../Typography";
import Item from "./Item";
import { StyledMiniCart } from "./MiniCartStyle";

type MiniCartProps = {
  toggleSidenav?: () => void;
};

type CartItem = {
  id: any;
  quantity: any;
  price: any;
  product: any;
};

const MiniCart: React.FC<MiniCartProps> = ({ toggleSidenav }) => {
  const { dispatch, state } = useAppContext();
  const [cartProductList, setCartProductList] = useState([]);
  // const [reloadCart, setReloadCart] = useState(0);
  const [openLogin, setOpenLogin] = useState(false);
  const cartCanged = state.cart.chartQuantity;
  // const [loading, setLoading] = useState(false);

  const { user_id, isLogin, authTOKEN } = useUserInf();

  const closeLoginTab = () => {
    setOpenLogin(false);
  };

  useEffect(() => {
    const orderId = localStorage.getItem("OrderId");
    if (orderId) {
      axios
        .get(`${Customer_Order_Pending_Details}${orderId}`, {
          headers: {
            "Content-type": "application/json",
            Authorization: localStorage.getItem("jwt_access_token"),
          },
        })
        .then((res) => {
          if (res.data.order.order_items.length !== 0) {
            setCartProductList(res.data.order.order_items);
          }
        })
        .catch((_err) => {
          setCartProductList([]);
        });
    }
  }, [cartCanged]);

  const handleItemRemoval = (productIdToRemove) => {
    setCartProductList((prevCartProductList) => {
      const updatedCartProductList = prevCartProductList.filter(
        (item) => item.id !== productIdToRemove
      );

      if (updatedCartProductList.length === 0) {
        toggleSidenav();
        localStorage.removeItem("OrderId");
      }
      return updatedCartProductList;
    });
  };

  // useEffect(() => {
  //   if (cartCanged) {
  //   }
  // }, [cartCanged]);

  const handleCartAmountChange = useCallback(
    (product, action) => () => {
      console.log("sklfklsdaf", product);
      if (isLogin) {
        const orderId = localStorage.getItem("OrderId");

        if (orderId) {
          const item_id = product?.id;
          const orderData = {
            product_id: product?.product?.id,
            quantity: 1,
            price: product?.price,
            branch_id: 1,
            user_id: user_id,
            size: product?.size?.id,
            color: product?.color?.id,
          };

          if (action == "remove") {
            dispatch({
              type: "CHANGE_ALERT",
              payload: {
                alertLoading: true,
              },
            });

            axios
              .delete(
                `${Customer_Order_Remove_Item}${orderId}/${item_id}`,
                authTOKEN
              )
              .then((res) => {
                handleItemRemoval(item_id);

                if (res.status === 200) {
                  // setReloadCart(Math.random());

                  dispatch({
                    type: "CHANGE_CART_QUANTITY",
                    payload: {
                      chartQuantity: Math.random(),
                      prductId: product?.id,
                    },
                  });
                }
                dispatch({
                  type: "CHANGE_ALERT",
                  payload: {
                    alertLoading: false,
                  },
                });
              })
              .catch(() => {});
          } else if (action == "increase") {
            // setLoading(true);
            dispatch({
              type: "CHANGE_ALERT",
              payload: {
                alertLoading: true,
              },
            });

            axios
              .put(
                `${Customer_Increase_Quantity}${orderId}/${item_id}`,
                orderData,
                authTOKEN
              )
              .then(() => {
                // setReloadCart(Math.random());
                dispatch({
                  type: "CHANGE_CART_QUANTITY",
                  payload: {
                    chartQuantity: Math.random(),
                    prductId: product?.product?.id,
                  },
                });
                // setLoading(false);
                dispatch({
                  type: "CHANGE_ALERT",
                  payload: {
                    alertLoading: false,
                  },
                });
              })
              .catch(() => {
                // setLoading(false);
                dispatch({
                  type: "CHANGE_ALERT",
                  payload: {
                    alertLoading: false,
                  },
                });
              });
          } else if (action == "decrease") {
            // setLoading(true);
            dispatch({
              type: "CHANGE_ALERT",
              payload: {
                alertLoading: true,
              },
            });

            axios
              .put(
                `${Customer_decrease_Quantity}${orderId}/${item_id}`,
                orderData,
                authTOKEN
              )
              .then(() => {
                // setReloadCart(Math.random());
                dispatch({
                  type: "CHANGE_CART_QUANTITY",
                  payload: {
                    chartQuantity: Math.random(),
                    prductId: product?.product?.id,
                  },
                });
                // setLoading(false);
                dispatch({
                  type: "CHANGE_ALERT",
                  payload: {
                    alertLoading: false,
                  },
                });
              })
              .catch(() => {
                // setLoading(false);
                dispatch({
                  type: "CHANGE_ALERT",
                  payload: {
                    alertLoading: false,
                  },
                });
              });
          }
        }
      } else {
        setOpenLogin(true);
      }
    },
    [user_id, isLogin, authTOKEN]
  );

  const getTotalPrice = () => {
    return (
      cartProductList.reduce(
        (accumulator, item) => accumulator + item.price * item.quantity,
        0
      ) || 0
    );
  };

  return (
    <>
      {/* {loading && (
        <div
          style={{
            position: "fixed",
            height: "100%",
            width: "100%",
            top: "0px",
            left: "0px",
            display: "flex",
            justifyContent: "center",
            backgroundColor: " rgb(0 0 0 / 50%)",
            alignItems: "center",
            zIndex: 100,
          }}
        >
          <img
            style={{
              height: "100px",
              width: "100px",
              marginTop: "100pz",
            }}
            src="/assets/images/gif/loading.gif"
          />
        </div>
      )} */}
      <LoginPopup open={openLogin} closeLoginDialog={closeLoginTab} />
      <StyledMiniCart>
        <div className="cart-list">
          <FlexBox alignItems="center" m="0px 20px" height="74px">
            <Icon style={{ color: "#e02043" }} size="1.75rem">
              bag
            </Icon>
            <Typography fontWeight={600} fontSize="16px" ml="0.5rem">
              {cartProductList.length} item
            </Typography>
          </FlexBox>

          <Divider />

          {!!!cartProductList.length && (
            <FlexBox
              flexDirection="column"
              alignItems="center"
              justifyContent="center"
              height="calc(100% - 80px)"
            >
              <LazyImage
                src="/assets/images/logos/shopping-bag.svg"
                width="90px"
                height="100%"
              />
              <Paragraph
                mt="1rem"
                color="text.muted"
                textAlign="center"
                maxWidth="200px"
              >
                Your shopping bag is empty. Start shopping
              </Paragraph>
            </FlexBox>
          )}
          {cartProductList.map((item: CartItem) => {
            console.log("cartProductList", item);
            return (
              <Item
                item={item}
                handleCartAmountChange={handleCartAmountChange}
              />
            );
          })}
        </div>

        {!!cartProductList.length && (
          <Fragment>
            <Link href="/checkout" as="/checkout">
              <Button
                variant="contained"
                color="primary"
                m="1rem 1rem 0.75rem"
                onClick={toggleSidenav}
              >
                <Typography fontWeight={600} display="flex" textAlign="center">
                  Checkout Now (
                  <Currency>{getTotalPrice().toFixed(2)}</Currency>)
                </Typography>
              </Button>
            </Link>
            <Link href="/cart" as="/cart">
              <Button
                color="primary"
                variant="outlined"
                m="0px 1rem 0.75rem"
                onClick={toggleSidenav}
              >
                <Typography fontWeight={600}>View Cart</Typography>
              </Button>
            </Link>
          </Fragment>
        )}
      </StyledMiniCart>
    </>
  );
};

MiniCart.defaultProps = {
  toggleSidenav: () => {},
};

export default MiniCart;
