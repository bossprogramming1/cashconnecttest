import Box from "@component/Box";
import FlexBox from "@component/FlexBox";
import Grid from "@component/grid/Grid";
import NavbarLayout from "@component/layout/NavbarLayout";
import ProductDescription from "@component/products/ProductDescription";
import ProductFeatures from "@component/products/ProductFeatures";
import ProductIntro from "@component/products/ProductIntro";
import ProductReview from "@component/products/ProductReview";
import RelatedProducts from "@component/products/RelatedProducts";
import { H5 } from "@component/Typography";
import {
  BASE_URL,
  product_by_categoryId,
  Product_by_id,
} from "@data/constants";
import useWindowSize from "@hook/useWindowSize";
import axios from "axios";
import _ from "lodash";
import { GetServerSideProps } from "next";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";

const ProductDetails = ({
  id,
  title,
  price,
  imgUrl,
  brand,
  rating,
  initialReviewsQuantity,
  fullDes,
  fullFeatures,
  relatedProduct,
  condition,
  short_desc,
  orginalprice,
}) => {
  const [_reviewsQuantity, setreviewsQuantity] = useState(
    initialReviewsQuantity
  );

  const setNumOfReviews = (quantity = 0) => {
    setreviewsQuantity(quantity);
  };

  const [selectedOption, setSelectedOption] = useState("");
  const width = useWindowSize();
  const isMobile = width < 769;
  const router = useRouter();
  const review = router.query?.review;
  const features = router.query?.features;

  useEffect(() => {
    if (review) {
      setSelectedOption("review");
    }
    if (features) {
      setSelectedOption("features");
    }
  }, [review, features]);

  const handleOptionClick = (e) => () => {
    setSelectedOption(e);
  };

  return (
    <div>
      <ProductIntro
        title={title}
        price={price}
        imgUrl={[imgUrl]}
        id={id}
        brand={brand}
        rating={rating}
        reviewCount={initialReviewsQuantity}
        condition={condition}
        short_desc={short_desc}
        orginalprice={orginalprice}
        parse={""}
        eye={false}
      />

      {!isMobile && (
        <FlexBox
          borderBottom="1px solid"
          borderColor="gray.400"
          mt="30px"
          mb="2rem"
        >
          <H5
            className="cursor-pointer"
            mr="25px"
            p="4px 10px"
            color={selectedOption === "features" ? "primary.main" : "#113c64"}
            borderBottom={selectedOption === "features" && "2px solid"}
            borderColor="primary.main"
            onClick={handleOptionClick("features")}
          >
            Specification
          </H5>
          <H5
            className="cursor-pointer"
            mr="25px"
            p="4px 10px"
            color={
              selectedOption === "description" ? "primary.main" : "#113c64"
            }
            borderBottom={selectedOption === "description" && "2px solid"}
            borderColor="primary.main"
            onClick={handleOptionClick("description")}
          >
            Description
          </H5>
          <H5
            className="cursor-pointer"
            p="4px 10px"
            color={selectedOption === "review" ? "primary.main" : "#113c64"}
            onClick={handleOptionClick("review")}
            borderBottom={selectedOption === "review" && "2px solid"}
            borderColor="primary.main"
          >
            Review
            {/* {initialReviewsQuantity ? initialReviewsQuantity : ""} */}
          </H5>
        </FlexBox>
      )}
      {isMobile && (
        <Box
          borderBottom="1px solid"
          borderColor="gray.400"
          mt="30px"
          mb="2rem"
        >
          <Grid style container>
            <Grid item lg={4} sm={4} xs={4}>
              <H5
                className="cursor-pointer"
                p="4px 10px"
                color={
                  selectedOption === "features" ? "primary.main" : "text.muted"
                }
                borderBottom={selectedOption === "features" && "2px solid"}
                borderColor="primary.main"
                onClick={handleOptionClick("features")}
              >
                Specification
              </H5>
            </Grid>
            <Grid item lg={4} sm={4} xs={4}>
              <H5
                className="cursor-pointer"
                p="4px 10px"
                color={
                  selectedOption === "description"
                    ? "primary.main"
                    : "text.muted"
                }
                borderBottom={selectedOption === "description" && "2px solid"}
                borderWidth="fit"
                borderColor="primary.main"
                onClick={handleOptionClick("description")}
              >
                Description
              </H5>
            </Grid>
            <Grid item lg={4} sm={4} xs={4}>
              <H5
                display="flex"
                className="cursor-pointer"
                p="4px 10px"
                color={
                  selectedOption === "review" ? "primary.main" : "text.muted"
                }
                onClick={handleOptionClick("review")}
                borderBottom={selectedOption === "review" && "2px solid"}
                borderColor="primary.main"
              >
                {`Review ${
                  initialReviewsQuantity ? initialReviewsQuantity : ""
                }`}
              </H5>
            </Grid>
          </Grid>
        </Box>
      )}

      <Box mb="2rem">
        {selectedOption === "description" && (
          <ProductDescription fullDes={fullDes} parse={""} />
        )}
        {selectedOption === "features" && (
          <ProductFeatures fullFeatures={fullFeatures} parse={""} />
        )}
        {selectedOption === "review" && (
          <ProductReview product_id={id} setReviews={setNumOfReviews} />
        )}
      </Box>

      <RelatedProducts relatedProduct={relatedProduct} />
    </div>
  );
};

ProductDetails.layout = NavbarLayout;

export default ProductDetails;

export const getServerSideProps: GetServerSideProps = async ({ params }) => {
  try {
    const res = await axios.get(`${Product_by_id}${params.id}`);
    var data: any = await res.data;
  } catch (err) {
    var data: any = {};
  }

  try {
    const relatedProductRes = await fetch(
      `${product_by_categoryId}${data.category.id}?size=12`
    );
    var relatedProductjson = await relatedProductRes.json();
    var relatedProduct: any[] = await relatedProductjson.products;
  } catch (err) {
    var relatedProduct = [];
  }

  return {
    props: {
      id: params.id,
      title: data.name,
      price: data?.product_discount?.discounted_price || data?.unit_price,
      orginalprice: data?.unit_price || null,
      imgUrl: `${BASE_URL}${data?.thumbnail}`,
      brand: _.isObject(data?.brand) ? data?.brand?.name : data?.brand || "",
      fullDes: data?.full_desc,
      fullFeatures: data?.features,
      initialReviewsQuantity: data?.num_reviews,
      rating: data?.rating,
      condition:
        data?.condition === "used" ||
        data?.condition === "Used" ||
        data?.condition === "USED"
          ? "pre owned"
          : data?.condition
          ? data.condition
          : null,
      short_desc: data?.short_desc,
      relatedProduct,
    },
  };
};
