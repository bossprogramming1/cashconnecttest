import Button from "@component/buttons/Button";
import DashboardLayout from "@component/layout/FooterLayout";
import DashboardPageHeader from "@component/layout/DashboardPageHeader";
import { H1 } from "@component/Typography";

import Link from "next/link";
import React, { useEffect, useState } from "react";

import Grid from "@component/grid/Grid";
import BranchList from "@component/branchs/BranchList";
import { Find_Nearest_Branch_All } from "@data/constants";
import IconButton from "@component/buttons/IconButton";
import Icon from "@component/icon/Icon";

const Branches = () => {
  const [latitude, setLatitude] = useState(null);
  const [longitude, setLongitude] = useState(null);
  const [error, setError] = useState(null);
  const [isFind, setIsFind] = useState(false);
  const [nearestBranch, setnearestBranch] = useState([]);

  useEffect(() => {
    // Check if the Geolocation API is supported by the browser
    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition(
        (position) => {
          // Update the state with the current latitude and longitude
          setLatitude(position.coords.latitude);
          setLongitude(position.coords.longitude);
        },
        (error) => {
          // Handle any errors that occur during the location retrieval
          setError(error.message);
        }
      );
    } else {
      // Geolocation is not supported by the browser
      setError("Geolocation is not supported by your browser");
    }
  }, []);
  const findBranch = () => {
    fetch(
      `${Find_Nearest_Branch_All}?latitude=${latitude || 0}&longitude=${
        longitude || 0
      }`,
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
        },
      }
    )
      .then((response) => {
        if (!response.ok) {
          throw new Error(`HTTP error! Status: ${response.status}`);
        }
        return response.json();
      })
      .then((branch) => {
        console.log("API response:", branch);
        setIsFind(true);
        setnearestBranch(branch?.response);
        // if (branch?.branches) {
        //   console.log("Setting data to state...");
        //   setBranchList(branch?.branches);
        //   setTotalBranch(branch?.total_elements);
        //   setTotalPage(branch?.total_pages);
        // }
      })
      .catch((error) => {
        console.error("Error fetching data:", error);
      });
  };
  console.log("userLocation", latitude, longitude, error);
  return (
    <div>
      <Grid container spacing={6}>
        <Grid item lg={8} md={8} sm={12} xs={12}>
          <H1 color="#113c64" fontWeight="800" style={{ textAlign: "center" }}>
            FIND YOUR NEAREST <span style={{ color: "#e02043" }}> BRANCH </span>
          </H1>
        </Grid>
        <Grid item lg={4} md={4} sm={12} xs={12}>
          <H1
            color="#113c64"
            fontWeight="800"
            style={{
              textAlign: "center",
              display: "flex",
              justifyContent: "center",
            }}
          >
            {/* <Button
              color="primary"
              bg="primary.light"
              px="2rem"
              onClick={findBranch}
            >
              Find Nearest Branch
            </Button> */}
            <IconButton
              // size="small"
              type="button"
              color="primary"
              bg="primary.light"
              style={{ display: "flex" }}
              onClick={findBranch}
            >
              <Icon variant="small" defaultcolor="currentColor" mr="1rem">
                search-location
              </Icon>
              Find Nearest Branch
            </IconButton>
          </H1>
        </Grid>
      </Grid>
      <BranchList isFind={isFind} nearestBranch={nearestBranch} />
      <DashboardPageHeader
        button={
          <Link href="/" as="/">
            <Button color="primary" bg="primary.light" px="2rem">
              Back to home
            </Button>
          </Link>
        }
      />
    </div>
  );
};

Branches.layout = DashboardLayout;

export default Branches;
