import Button from "@component/buttons/Button";
import DashboardLayout from "@component/layout/FooterLayout";
import DashboardPageHeader from "@component/layout/DashboardPageHeader";
import TableRow from "@component/TableRow";
import { H3, H6 } from "@component/Typography";
import Link from "next/link";
import React from "react";
import Image from "@component/Image";
import Grid from "@component/grid/Grid";

const Profile = () => {
  return (
    <div>
      <DashboardPageHeader
        iconName="refund"
        title="Refunds and Returns"
        button={
          <Link href="/" as="/">
            <Button color="primary" bg="primary.light" px="2rem">
              Back to home
            </Button>
          </Link>
        }
      />
      <TableRow mb={"10px"}>
        <Grid container spacing={6}>
          <Grid item lg={6} md={6} sm={12} xs={12}>
            <H6
              p="2rem 1rem"
              color="#113c64"
              fontWeight="500"
              style={{ textAlign: "justify", paddingBottom: "0px" }}
            >
              It's a rare case for cashconnect where customers didn't get their
              products unharmed. But sometimes we may fail to fulfill your
              expectations, sometimes situations aren't by our side. But there
              is now a "Bond of Trust" between customers and cashconnect, So,
              for further ensuring and encouraging this "Bond of Trust"
              cashconnect.com brings you The "Happy Return" policy. Where
              customers can return their books or products only if there's
              something wrong (Torn, Pages missing, Wrong book/Product, etc)
              with it. In that case cashconnect will give you fresh products in
              return. Because we believe that happiness should be returned if
              that happiness can't give you a smile. So, We returned it with
              proper happiness. We always want to bring a smile in your face and
              make you happier. We call this policy of ours "Happy Return".{" "}
              <br /> <br />
              <strong>Accessories</strong>
              <br />
              🗹 For accessories, the advance amount is totally not refundable.{" "}
              <br /> 🗹 Only in the case of unavailability of the product or Out
              Of Stock status, We will refund the full amount.
              <br /> 🗹 If we ship the product and after that the customer wants
              to cancel the product, we will not give any refund and also
              customer will not be able to change to another product.
              <br /> 🗹 Please Notice, from the date of your order, You must make
              the request for refund within 3 working days. No request will be
              accepted after the mentioned period.
              <br />
              <br />
              <strong>Procedure of Device Refund</strong>
              <br />
              🗹 Only in the case of unavailability of the product or Out Of
              Stock status, We will refund the full amount. <br />
              🗹 Please Notice, from the date of your order, You must make the
              request for refund within 3 working days. No request will be
              accepted after the mentioned period.
              <br />
              <br />
            </H6>
          </Grid>
          <Grid item lg={6} md={6} sm={12} xs={12}>
            <Image
              src="/assets/images/customerCare/return.png"
              width="100%"
              p="2rem 1rem"
            ></Image>
          </Grid>
          <Grid item lg={12} sm={12} xs={12}>
            <div>
              <H6
                p="0rem 2.75rem"
                color="#113c64"
                fontWeight="500"
                style={{ textAlign: "justify" }}
              >
                <strong>Refund process for online gateway payment</strong>
                <br />
                🗹 If the payment is completed through SSL comm. Online gateway
                then the refund will be processed according to SSL Comm. Refund
                policy. <br />
                🗹 If the Customer wants instant refund (within 24 hours) after
                completing the payment through SSl comm. He/She will only get
                the product price as refund. Cash Connect will not take any
                liability of online gateway / EMI charge.
                <br />
                <br />
                <strong>What is the Refund Mode?</strong>
                <br />
                🗹 Refunds are made through BANK TRANSFER & MFS.
                <br />
                <br />
                <strong>
                  Can you request an exchange rather than a refund?
                </strong>
                <br />
                🗹 If you prefer to exchange your product, just inform our
                Customer Service. We will evaluate and guide you for the next
                steps.
                <br />
                🗹 If Cash Connect fails to deliver a product after confirming
                the order due to market unavailability, Cash Connect will refund
                the payment within 24 Hours
                <br />
                🗹 Customers will only get the product price as refund (website
                price). Cash Connect will not take any liability of any online
                gateway charge / EMI charge / MFS charge / Bank charge.
              </H6>
              <H3
                p="0 1rem"
                style={{ textAlign: "center" }}
                color="primary.main"
                fontWeight="800"
              >
                Return Policy{" "}
              </H3>

              <H6
                p="2rem 1rem"
                color="#113c64"
                fontWeight="500"
                style={{ textAlign: "justify" }}
              >
                We strive to ensure that every purchase and every product you
                make is right. After this, if for any reason there is a
                manufacturing fault in the product or the product does not work
                after receiving the product, the colour or size is different
                from the product ordered, there is an opportunity to exchange or
                return the product. In case of product missing or different
                product complaint, during delivery must understand the product
                in front of the delivery man or must record the unboxing video.
                Product missing in the package, product broken in courier or
                product different are not acceptable without recording unboxing
                video - so it is requested to record the product unboxing video
                to prevent misunderstanding.
                <br />
                We provide a 7-day replacement warranty on almost all products
                from Cash Connect and also on almost all products there is a
                specified warranty period which is effective from the day of
                receiving the product.
                <br />
                Below is the list of cases where returns, exchanges, warranties
                and refunds are not applicable:
                <ul>
                  <li>If the product is burnt or physically damaged</li>
                  <li>Any kind of software or digital products</li>
                  <li>Clearance sale products</li>
                  <li>If the product's intact seal or sticker is removed</li>
                  <li>Under garment items</li>
                  <li>
                    Any kind of accessories or chargers or adapters with the
                    product
                  </li>
                  <li>Any gift item or prize that is given free of charge</li>
                  <li>
                    If the product has no scratches or stains or is in resalable
                    condition
                  </li>
                  <li>
                    Compatibility issues with any third party hardware or device
                    or app or software that is not a default feature of the
                    product.
                  </li>
                </ul>
                Check the product as soon as possible and if you find any
                problem, please make a written complaint to us. In this case,
                email will be given top priority, but for small problems or how
                to operate a product, please call the call center or inbox us on
                Facebook for help. <br />
                Can take if any product needs to be returned, it must be
                properly boxed or in a separate bag (no tape on the product box)
                with all papers, boxes, accessories, warranty cards, stickers,
                labels, gift items etc. provided along with it and courier to
                our office address. Should be done or sent by yourself or
                someone else. In this case, if there is an opportunity to pick
                up the product from your location, we will try, but in that
                case, you have to pay the courier charge in advance.
                <br />
                Product returns must be in resalable condition, product returns
                will not be accepted if any parts are missing or the box is
                damaged. If everything is fine after receiving the product,
                after checking the product, if everything is fine, refund will
                be arranged within 72 hours.
                <br />
                If there is any fault in the product, we will bear the delivery
                charge (Tk 50 inside Dhaka and any additional courier charge
                outside Dhaka Tk 100 will have to be paid by the buyer) but in
                case of change of mind or dislike or other cases courier charge
                and other payment settlement The customer has to bear the entire
                charge.
                <br />
                At the time of refund, if the customer received any cashback and
                gift on that order, it will be taken back from the customer. The
                remaining amount will be refunded after deducting the cashback
                amount.
                <br />
                You can contact our call center-: +88 09613 821 172 (10 am to 07
                pm)
                <br />
                Thanks.
              </H6>

              <H3
                p="0 1rem"
                style={{ textAlign: "center" }}
                color="primary.main"
                fontWeight="800"
              >
                Warranty Claim for Smart Devices
              </H3>

              <H6
                p="2rem 1rem"
                color="#113c64"
                fontWeight="500"
                style={{ textAlign: "justify" }}
              >
                <ul>
                  <li>
                    Active / Inactive status of the device and visible issues
                    (scratches/defects/color/box accessories) should be verified
                    by the buyer before leaving the outlet. Later no complaint
                    regarding this will be entertained. Asked to make a video
                    while unboxing if necessary.
                  </li>
                  <li>
                    Recently, there are some software and hardware related
                    problems in the devices of different companies which are
                    being faced by the users of all the countries of the world
                    which can only be solved by the manufacturing company.
                  </li>
                  <li>
                    Cash Connect Authority will not take any responsibility if
                    there is a problem related to Display/Hardware/Battery
                    Issue/Over Heating on the phone due to Operating System
                    Update.
                  </li>
                  <li>
                    Make sure to check whether your phone is blacklisted or not
                    before purchasing the phone.
                  </li>
                  <li>
                    10 days warranty for all visible issues except display-touch
                    / software issue is applicable for Smartphone/Tab/TV only.
                  </li>
                  <li>
                    Also getting 1 (one) year free service warranty for
                    Smartphone/Tab/TV (Parts/Hardware excluded) i.e. if the
                    device is fixed only through servicing without modification,
                    there will be no charge, but if the parts need to be
                    replaced, the buyer must pay the cost. Have to do
                    Installation Charge will be borne by Cash Connect. Notably
                    in this case the servicing parts will depend on availability
                    in the market.
                    <br />
                    **Warranty claim may take 3 to 45 working days maximum due
                    to pandemic and special complications.**
                  </li>
                </ul>
                <br />
              </H6>
              <H3
                p="0 1rem"
                style={{ textAlign: "center" }}
                color="primary.main"
                fontWeight="800"
              >
                Accessories Warranty Claim
              </H3>

              <H6
                p="2rem 1rem"
                color="#113c64"
                fontWeight="500"
                style={{ textAlign: "justify" }}
              >
                <ul>
                  <li>
                    In case of warranty claim of Accessories (Smart Watch, Air
                    Pods, Power Adapter, Cable, Bluetooth Speaker etc.) with
                    Official Warranty, it is mandatory to keep the box and
                    warranty sticker. In this case it may take 3 to 45 working
                    days to claim the warranty.
                  </li>
                  <li>
                    3 days warranty is applicable for non-warranty products
                    only. (Excluding product display / touch issue, liquid
                    damage, physical damage, product box damage)
                  </li>
                </ul>
                <br />
              </H6>
              <H3
                p="0 1rem"
                style={{ textAlign: "center" }}
                color="primary.main"
                fontWeight="800"
              >
                Special Note: The following conditions apply to both devices and
                accessories
              </H3>

              <H6
                p="2rem 1rem"
                color="#113c64"
                fontWeight="500"
                style={{ textAlign: "justify" }}
              >
                <ul>
                  <li>
                    Products sold are non-negotiable. If there is no visible
                    problem with the purchased device or if you don't like it
                    for any reason, you can exchange it according to Cash
                    Connect' exchange policy. In no way can the purchased device
                    be exchanged for another product.
                  </li>
                  <li>
                    The same product may have different software restrictions or
                    hardware changes based on different Sales Regions, and due
                    to production batch differences, the new product may have
                    some differences with your previously used product, such as:
                    fittings / display color / headphone sound quality /
                    packaging, etc. All these events will not be covered by
                    warranty replacement.
                  </li>
                  <li>
                    Products covered by warranty will be resolved within a
                    specified period of time. If a product is beyond repair only
                    in that case the product will be replaced.
                  </li>
                  <li>
                    Replacement warranty is applicable for one time only. But in
                    that case (scratches/defects/color/accessories and box) all
                    must be intact.
                  </li>
                  <li>
                    In the case of some brands, the buyer has to go to their own
                    official service center according to the brand's own
                    warranty policy to take all the warranty benefits, such as
                    Edifier & products like Cash Connect, to make a warranty
                    claim through Cash Connect, the customer must provide more
                    time.
                  </li>
                  <li>
                    If there is any defect in the product received through the
                    courier, then to make a warranty claim without using the
                    product, it must be reported to our customer support on the
                    day of receipt of the order / within 24 hours. (+88 09613
                    821 172)
                  </li>
                  <li>
                    Unboxing video must start from opening the Cash Connect own
                    box/sticker when purchasing the product through courier, and
                    it is requested to make a video from Unboxing to Connection
                    to facilitate understanding whether the product works
                    properly. So that it can be presented as evidence in future
                    if any error occurs.
                  </li>
                  <li>
                    No warranty claim shall be entertained after expiry of
                    warranty period.
                  </li>
                  <li>
                    Any refund will be processed within 3 (three) working days.
                    Please note, the method of refund (Bank Transfer / Mobile
                    Banking) will be determined by Cash Connect.
                  </li>
                  <li>
                    Cash Connect reserves the right to change/modify/extend any
                    policy at any time without prior notice.
                  </li>
                  <br />
                  **To make any product warranty claim, you have to come to our
                  customer care in person with the invoice/bill and the warranty
                  sticker on the box.**
                  <br />
                  **Warranty claims may require 3 to 45 business days due to
                  epidemics and special complications.**
                  <br />I bought the product from Cash Connect after being aware
                  of all the rules and regulations mentioned above.
                </ul>
                <br />
              </H6>

              <H3
                p="0 1rem"
                style={{ textAlign: "center" }}
                color="primary.main"
                fontWeight="800"
              >
                Cancellation Policy
              </H3>

              <H6
                p="2rem 1rem"
                color="#113c64"
                fontWeight="500"
                style={{ textAlign: "justify" }}
              >
                Cash connect retains the right to cancel any order at its sole
                discretion prior to dispatch and for any reason which may
                include, but not limited to, the product being mispriced, out of
                stock, expired, defective, malfunctioned, and containing
                incorrect information or description arising out of technical or
                typographical error or for any other reason.
                <br />
              </H6>
            </div>
          </Grid>
        </Grid>
      </TableRow>
    </div>
  );
};

Profile.layout = DashboardLayout;

export default Profile;

export async function getServerSideProps() {
  // This is an empty async function to satisfy the requirement of getServerSideProps
  return { props: {} };
}
