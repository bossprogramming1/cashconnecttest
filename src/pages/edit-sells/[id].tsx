import Avatar from "@component/avatar/Avatar";
import Box from "@component/Box";
import Button from "@component/buttons/Button";
import CheckBox from "@component/CheckBox";
import Grid from "@component/grid/Grid";
import Hidden from "@component/hidden/Hidden";
import Icon from "@component/icon/Icon";
import Image from "@component/Image";
import CustomerDashboardLayout from "@component/layout/CustomerDashboardLayout";
import DashboardPageHeader from "@component/layout/DashboardPageHeader";
import Radio from "@component/radio/Radio";
import TextField from "@component/text-field/TextField";
import TextArea from "@component/textarea/TextArea";
import Typography from "@component/Typography";
import { useAppContext } from "@context/app/AppContext";
import useUserInf from "@customHook/useUserInf";
import {
  BASE_URL,
  Purchase_Items_By_Purchase_Id,
  Purchase_Req_By_Id,
  Purshase_Update,
} from "@data/constants";
import { required } from "@data/data";
import useWindowSize from "@hook/useWindowSize";
import axios from "axios";
import { useFormik } from "formik";
import jsonToFormData from "helper/jsonToFormData";
import _ from "lodash";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import * as yup from "yup";

type Iimage = any[];
type TIMG = any[];

function onlineSellList() {
  const [items, setItems] = useState([{}]);
  const [contact_type, setContact_type] = useState("email");
  const [isSubscribe, setIsSubscribe] = useState(false);
  const [_reRender, setReRender] = useState(0);
  const [previewImage, setPreviewImage] = useState<Iimage>([[]]);
  const [images, setImages] = useState<TIMG>([[]]);
  const [loading, setLoading] = useState(false);
  const width = useWindowSize();
  const { dispatch } = useAppContext();
  const { authTOKEN, user_id } = useUserInf();
  const isMobile = width <= 768;

  const router = useRouter();
  const { id } = router.query;

  console.log("router", router.query);

  useEffect(() => {
    previewImage.map((img, id) => {
      if (_.isEmpty(img)) {
        setFieldValue(`item_image${id}`, null);
      } else {
        setFieldValue(`item_image${id}`, "imageUploaded");
      }
    });
  }, [previewImage]);

  useEffect(() => {
    if (id) {
      axios
        .get(`${Purchase_Items_By_Purchase_Id}${id}`)
        .then((res) => {
          setItems(res?.data?.purchase_request_items);
          const updatedPreviewImages = [...previewImage]; // Create a copy of previewImage state
          res?.data?.purchase_request_items.forEach((itm, idx) => {
            setFieldValue(`item_name${idx}`, itm.name);
            setFieldValue(`item_id${idx}`, itm.id);
            setFieldValue(`item_price${idx}`, itm.unit_price);
            setFieldValue(`item_description${idx}`, itm?.description);

            // Handle multiple images for each item
            const images = itm.purchase_request_item_images.map(
              (image) => `${BASE_URL}${image.image}`
            );
            updatedPreviewImages[idx] = images;
          });
          setPreviewImage(updatedPreviewImages); // Update the state with new image data
          console.log("items", res?.data?.purchase_request_items);
        })
        .catch(() => {});

      axios.get(`${Purchase_Req_By_Id}${id}`).then((res) => {
        const data = [];
        data.push(res?.data);
        console.log("userInfo", res?.data);

        const responseData = res?.data || {}; // Ensure res.data is an object

        Object.entries(responseData).forEach(([key, value]) => {
          // Iterate over each key-value pair in the response data
          setFieldValue(key, value);
        });
        setContact_type(res?.data?.contact_type);
        setIsSubscribe(res?.data?.is_subscribed);
      });
    }
  }, [id, authTOKEN]);
  //submit purchase data
  const handleFormSubmit = async (values) => {
    const authTOKEN = {
      headers: {
        "Content-type": "multipart/form-data",
        Authorization: localStorage.getItem("jwt_access_token"),
      },
    };
    let purchaseData = {
      first_name: values.first_name,
      last_name: values.last_name,
      contact_no: `${values.contact_no}`,
      id: values.id,
      purchase_status: values?.purchase_status?.id,
      email: values.email,
      invoice_no: values.invoice_no,
      is_cancelled: values.is_cancelled,
      is_online: values.is_online,
      is_verified: values.is_verified,
      request_date: values.request_date,
      total_price: values.total_price,
      transfer_status: values.transfer_status,
      updated_by: values.updated_by.id,
      street_address: values.street_address,
      contact_type: contact_type,
      vendor: user_id,
      is_subscribed: isSubscribe,
      items: [],
    };

    let Items = [];
    items.map((_itm, id) => {
      let Item = {
        item_name: values[`item_name${id}`],
        item_price: values[`item_price${id}`],
        item_description: values[`item_description${id}`],
        id: values[`item_id${id}`],
        item_quantity: 1,
        images: images[id],
      };
      Items.push(Item);
    });
    purchaseData.items = Items;
    console.log("purchaseData", purchaseData, values);

    const [PurchaseDataToFormData] = jsonToFormData(purchaseData);

    setLoading(true);

    axios
      .put(`${Purshase_Update}${id}`, PurchaseDataToFormData, authTOKEN)
      .then((res) => {
        setLoading(false);
        if (res?.data?.purchase_request?.purchase_request_items?.length) {
          router.push("/sells");
          dispatch({
            type: "CHANGE_ALERT",
            payload: {
              alertValue: `updated successfully`,
              alerType: "success",
            },
          });
        } else if (res?.data?.user_exists) {
          dispatch({
            type: "CHANGE_ALERT",
            payload: {
              alertValue: `${values.email} is already exist`,
              alerType: "warning",
            },
          });
        } else {
          dispatch({
            type: "CHANGE_ALERT",
            payload: {
              alertValue: "someting went wrong",
              alerType: "error",
            },
          });
        }
      })
      .catch(() => {
        setLoading(false);
        dispatch({
          type: "CHANGE_ALERT",
          payload: {
            alertValue: "someting went wrong",
            alerType: "error",
          },
        });
      });
  };

  const handleContactTypeChange = ({ target: { name } }) => {
    setContact_type(name);
  };

  const handleSubscribeChecked = (e) => {
    setIsSubscribe(e.target.checked);
  };

  //add a new item
  const addItem = () => {
    let newItems = items;
    newItems.push({});
    setItems(newItems);

    let newImg: any = [...previewImage];
    newImg.push([]);
    setPreviewImage(newImg);

    let newImage: any = [...images];
    newImage.push([]);
    setImages(newImage);

    setReRender(Math.random());
  };

  //remove specific item
  const removeItem = (id) => {
    let NewItems = [...items];
    NewItems.splice(id, 1);
    setItems(NewItems);

    let newImg: any = [...previewImage];
    newImg.splice(id, 1);
    setPreviewImage(newImg);

    let newImage: any = [...images];
    newImage.splice(id, 1);
    setImages(newImage);

    setFieldValue(`item_name${id}`, "");
    setFieldValue(`item_price${id}`, "");
    setFieldValue(`item_description${id}`, "");

    items.map((_data, idx) => {
      if (idx >= id) {
        setFieldValue(`item_name${idx}`, values[`item_name${idx + 1}`]);
        setFieldValue(`item_price${idx}`, values[`item_price${idx + 1}`]);
        setFieldValue(
          `item_description${idx}`,
          values[`item_description${idx + 1}`]
        );
        if (items.length - 1 == idx) {
          setFieldValue(`item_name${idx + 1}`, "");
          setFieldValue(`item_price${idx + 1}`, "");
          setFieldValue(`item_description${idx + 1}`, "");
        }
      }
    });

    setReRender(Math.random());
  };

  const cancelAImage = (itemId, imgId) => {
    let newPreImgs = [...previewImage];
    newPreImgs[itemId].splice(imgId, 1);
    setPreviewImage(newPreImgs);

    let newImages = [...images];
    newImages[itemId].splice(imgId, 1);
    setImages(newImages);

    setReRender(Math.random());
  };

  //validation for item
  var itemShema = {};
  items.map((_itm, id) => {
    itemShema = {
      ...itemShema,
      [`item_name${id}`]: yup
        .string()
        .required("Item name is required")
        .nullable(required),
      [`item_price${id}`]: yup
        .number()
        .required("Item price is required")
        .nullable(required),
      [`item_description${id}`]: yup
        .string()
        .required("Item description is required")
        .nullable(required),
      [`item_image${id}`]: yup
        .string()
        .required("Please add an image")
        .nullable(required),
    };
  });

  var checkoutSchema = yup.object().shape({
    ...itemShema,
    first_name: yup.string().required("required").nullable(required),
    last_name: yup.string().required("required").nullable(required),
    contact_no: yup.string().required("required").nullable(required),
  });

  const {
    values,
    errors,
    touched,
    handleChange,
    handleBlur,
    handleSubmit,
    setFieldValue,
  } = useFormik({
    initialValues: initialValues,
    validationSchema: checkoutSchema,
    onSubmit: handleFormSubmit,
  });

  return (
    <>
      {loading && (
        <div
          style={{
            position: "fixed",
            height: "100%",
            width: "100%",
            top: "0px",
            left: "0px",
            display: "flex",
            justifyContent: "center",
            backgroundColor: " rgb(0 0 0 / 50%)",
            alignItems: "center",
            zIndex: 100,
          }}
        >
          <Image
            style={{
              height: "100px",
              width: "100px",
              marginTop: "100pz",
            }}
            src="/assets/images/gif/loading.gif"
          />
        </div>
      )}

      <DashboardPageHeader
        title="Update Your Items Info"
        iconName="bag_filled"
      />

      <form onSubmit={handleSubmit}>
        <Box
          mt="60px"
          py="25px"
          px="25px"
          // mx={`${width < 950 || "100px"}`}
          boxShadow="0 1px 2px rgba(0,0,0,.08), 0 4px 12px rgba(0,0,0,.05)"
          justifyContent="center"
          alignItems="flex-start"
          display="flex"
          flexDirection="column"
        >
          <Grid container horizontal_spacing={6} vertical_spacing={4}>
            <Grid item md={12} xs={12}>
              <Box alignItems="center" display="flex" flexDirection="column">
                <Typography textAlign="center" fontSize="25px" fontWeight="700">
                  Your personal details
                </Typography>
              </Box>
            </Grid>
            <Grid item md={6} xs={12}>
              <TextField
                name="first_name"
                label="First Name"
                fullwidth
                onBlur={handleBlur}
                onChange={handleChange}
                value={values.first_name || ""}
                errorText={touched.first_name && errors.first_name}
                style={{ cursor: "no-drop" }}
                readOnly
              />
            </Grid>

            <Grid item md={6} xs={12}>
              <TextField
                name="last_name"
                label="Last Name"
                fullwidth
                onBlur={handleBlur}
                onChange={handleChange}
                value={values.last_name || ""}
                errorText={touched.last_name && errors.last_name}
                style={{ cursor: "no-drop" }}
                readOnly
              />
            </Grid>

            <Grid item md={6} xs={12}>
              <TextField
                mt="1rem"
                name="contact_no"
                label="Contact Number"
                fullwidth
                onBlur={handleBlur}
                onChange={handleChange}
                value={values.contact_no || ""}
                errorText={touched.contact_no && errors.contact_no}
                style={{ cursor: "no-drop" }}
                readOnly
              />
            </Grid>

            <Grid item md={6} xs={12}>
              <TextField
                mt="1rem"
                name="email"
                type="email"
                label="Email Address"
                fullwidth
                onBlur={handleBlur}
                onChange={handleChange}
                value={values.email || ""}
                errorText={touched.email && errors.email}
                // style={{ cursor: "no-drop" }}
                // readOnly
              />
            </Grid>
            <Grid item md={6} xs={12}>
              <TextField
                name="street_address"
                mt="20px"
                label="Type your address / suburb to find your nearest store"
                fullwidth
                onBlur={handleBlur}
                onChange={handleChange}
                value={values.street_address || ""}
                errorText={touched.street_address && errors.street_address}
              />
            </Grid>
            <Grid item md={6} xs={12}>
              <Box
                style={{
                  display: "flex",
                  flexDirection: "column",
                  justifyContent: "flex-end",
                }}
                mt="20px"
              >
                <Box
                  // mx={`${width < 950 || "100px"}`}
                  // pb="40px"
                  // bg="#dcdcdc"
                  // boxShadow="0px -10px 10px #ababab"

                  justifyContent="flex-start"
                  alignItems="flex-start"
                  display="flex"
                  flexDirection="column"
                >
                  <Typography
                    textAlign="center"
                    // fontSize="25px"
                    // fontWeight="700"
                    // mt="50px"
                  >
                    How would you like us to contact you?
                  </Typography>

                  <Box
                    display="flex"
                    // justifyContent="space-evenly"
                    width={"100%"}
                    // flexWrap="wrap"
                  >
                    <Radio
                      name="email"
                      mb="1.5rem"
                      color="secondary"
                      ml="6px"
                      marginRight={0}
                      width={10}
                      height={10}
                      checked={contact_type === "email"}
                      label={
                        <Typography
                          textAlign="center"
                          fontWeight="500"
                          fontSize="16px"
                        >
                          Email
                        </Typography>
                      }
                      onChange={handleContactTypeChange}
                    />
                    <Radio
                      name="sms"
                      mb="1.5rem"
                      color="secondary"
                      ml="6px"
                      checked={contact_type === "sms"}
                      marginRight={0}
                      label={
                        <Typography
                          textAlign="center"
                          fontWeight="500"
                          fontSize="16px"
                        >
                          SMS
                        </Typography>
                      }
                      onChange={handleContactTypeChange}
                    />

                    <Radio
                      name="cell"
                      mb="1.5rem"
                      color="secondary"
                      ml="6px"
                      checked={contact_type === "cell"}
                      marginRight={0}
                      label={
                        <Typography
                          textAlign="center"
                          fontWeight="500"
                          fontSize="16px"
                        >
                          Cell
                        </Typography>
                      }
                      onChange={handleContactTypeChange}
                    />
                  </Box>
                </Box>
              </Box>
            </Grid>
            <Grid item md={12} xs={12}>
              <Box
                // pt="40px"
                // pb="100px"
                // mx={`${width < 950 || "100px"}`}
                // boxShadow="0px -10px 10px 0px #ababab"
                justifyContent="center"
                alignItems="center"
                display="flex"
                flexDirection="column"
              >
                <Box
                  alignItems="center"
                  display="flex"
                  flexDirection="column"
                  width="100%"
                >
                  <Typography
                    textAlign="center"
                    fontSize="25px"
                    fontWeight="700"
                    mt="20px"
                  >
                    Tell us about your items
                  </Typography>
                  <Typography
                    textAlign="center"
                    fontSize="16px"
                    // fontWeight="600"
                    // mt="12px"
                  >
                    Provide as much detail as possible to help us give you an
                    accurate quote.
                  </Typography>
                </Box>
                {items.map((_item, idx) => {
                  return (
                    <>
                      <Box
                        // mt="60px"
                        // pb="50px"
                        // bg="#dcdcdc"
                        boxShadow="0 1px 2px rgba(0,0,0,.08), 0 4px 12px rgba(0,0,0,.05)"
                        padding={isMobile ? "20px" : "50px"}
                        justifyContent="center"
                        alignItems="center"
                        display="flex"
                        flexDirection="column"
                        width="100%"
                        position="relative"
                      >
                        {items.length !== 1 && (
                          <div
                            style={{
                              position: "absolute",
                              top: "10px",
                              right: "20px",
                              zIndex: 1,
                              color: "#113c64",
                            }}
                          >
                            <p>Item {idx + 1}</p>
                          </div>
                        )}
                        <Box width="100%">
                          <TextField
                            name={`item_name${idx}`}
                            label="Item Name"
                            placeholder="Describe your item for us. Eg. Make: Samsung Model: S9 SM-G960F Colour: Lilac Storage: 64gb*"
                            fullwidth
                            onBlur={handleBlur}
                            onChange={handleChange}
                            value={values[`item_name${idx}`] || ""}
                            errorText={errors[`item_name${idx}`]}
                          />
                        </Box>
                        <Box width="100%" mt="25px">
                          <TextField
                            name={`item_price${idx}`}
                            label="Item Price"
                            fullwidth
                            onBlur={handleBlur}
                            onChange={handleChange}
                            value={values[`item_price${idx}`] || ""}
                            errorText={errors[`item_price${idx}`]}
                          />
                        </Box>

                        <Box width="100%" mt="25px">
                          <TextArea
                            name={`item_description${idx}`}
                            label="Item Description"
                            fullwidth
                            rows={4}
                            aria-multiline
                            onBlur={handleBlur}
                            onChange={handleChange}
                            value={values[`item_description${idx}`] || ""}
                            errorText={errors[`item_description${idx}`]}
                          />
                        </Box>

                        {errors[`item_image${idx}`] && (
                          <p style={{ color: "red", marginBottom: "-20px" }}>
                            Image is required
                          </p>
                        )}

                        <Box
                          width="100%"
                          mt="25px"
                          display="flex"
                          justifyContent="space-evenly"
                          flexWrap="wrap"
                        >
                          {previewImage[idx]?.map((src, id) => {
                            return (
                              <>
                                <Box
                                  display="flex"
                                  width="fit-content"
                                  position="relative"
                                >
                                  <div
                                    id="cancelIcon"
                                    style={{
                                      position: "absolute",
                                      top: "-10px",
                                      right: "-10px",
                                      zIndex: 1,
                                      color: "red",
                                    }}
                                  >
                                    <Icon
                                      onClick={() => {
                                        cancelAImage(idx, id);
                                      }}
                                    >
                                      cancel
                                    </Icon>
                                  </div>
                                  <Avatar
                                    float="left"
                                    radius={10}
                                    ml="15px"
                                    src={src}
                                    size={100}
                                    // loader={() => previewImage}
                                  />
                                </Box>
                              </>
                            );
                          })}
                        </Box>

                        <Box width="100%" mt="10px">
                          <Box
                            display="flex"
                            alignItems="center"
                            justifyContent="center"

                            // height="100vh" // Adjust the height as needed
                          >
                            <label
                              htmlFor={`itemImg${idx}`}
                              style={{ cursor: "pointer" }}
                            >
                              <Icon size="50px">
                                add_photo_alternate_outlined
                              </Icon>
                            </label>
                          </Box>
                          <Box
                            display="flex"
                            justifyContent="center"
                            fontWeight={600}
                          >
                            Upload Image
                          </Box>
                          <Hidden>
                            <input
                              multiple
                              className="hidden"
                              onChange={async (e) => {
                                const reader: any = new FileReader();
                                reader.onload = () => {
                                  if (reader.readyState === 2) {
                                    let newImg = [...previewImage];
                                    newImg[idx].push(reader.result);
                                    setPreviewImage(newImg);
                                  }
                                };
                                reader.readAsDataURL(e.target.files[0]);

                                const file = e.target.files[0];
                                let newImgFile = [...images];

                                newImgFile[idx].push(file);

                                setImages(newImgFile);
                                // onChange(file);
                              }}
                              id={`itemImg${idx}`}
                              accept="image/*"
                              type="file"
                            />
                          </Hidden>
                        </Box>

                        {items.length !== 1 && (
                          <Button
                            mt="50px"
                            mb="1.65rem"
                            variant="contained"
                            color="error"
                            onClick={() => removeItem(idx)}
                          >
                            Remove Item
                          </Button>
                        )}
                      </Box>
                    </>
                  );
                })}
                <CheckBox
                  checked={isSubscribe}
                  label="Subscribe to our newsletter"
                  color="secondary"
                  disabled
                  mt="50px"
                  mb="1rem"
                  onChange={(e) => handleSubscribeChecked(e)}
                />
              </Box>
            </Grid>
            <Grid item md={12} xs={12}>
              <Box
                mx={`${width < 950 || "100px"}`}
                pb="40px"
                // bg="#dcdcdc"
                // boxShadow="0px -10px 10px #ababab"
                justifyContent="space-between"
                // alignItems="center"
                display="flex"
                flexDirection="row"
              >
                <Button
                  mt="20px"
                  mb="1.65rem"
                  variant="contained"
                  color="secondary"
                  onClick={addItem}
                >
                  Add Another Item
                </Button>

                <Button
                  type="submit"
                  mt="20px"
                  mb="1.65rem"
                  // disabled={!isSubscribe}
                  variant="contained"
                  color="secondary"
                >
                  Update
                </Button>
              </Box>
            </Grid>
          </Grid>
        </Box>
      </form>
    </>
  );
}

const initialValues = {
  first_name: "",
  last_name: "",
  contact_no: "+880",
  email: "",
};

onlineSellList.layout = CustomerDashboardLayout;

export default onlineSellList;

export async function getServerSideProps() {
  // This is an empty async function to satisfy the requirement of getServerSideProps
  return { props: {} };
}
