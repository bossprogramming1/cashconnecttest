import React, { useEffect, useState, useMemo } from "react";
import axios from "axios";
import AppLayout from "../components/layout/AppLayout";
import Container from "@component/Container";
import Skeleton from "@component/skeleton";
import useWindowSize from "@hook/useWindowSize";
import WhatsAppWidget from "@component/whatsApp";
import Section1 from "../components/home/Section1";
import Section2 from "../components/home/Section2";
import Section3 from "../components/home/Section3";
import Section4 from "../components/home/Section4";
import Section5 from "../components/home/Section5";
import Section7 from "../components/home/Section7";
import Section10 from "../components/home/Section10";
import Section11 from "../components/home/Section11";
// import Section12 from "../components/home/Section12";
import Section13 from "@component/home/Section13";
import getFormattedProductData from "@helper/getFormattedProductData";
import useUserInf from "@customHook/useUserInf";
import {
  Slider_All,
  Category_Top_All,
  Product_Flash_Deals,
  Product_Top_Rated,
  Product_Arrival,
  Product_Discount,
  Product_For_You,
  Category_With_Product_Brand,
  Category_Wth_Name_Img,
  Get_Pending_Order_After_Login,
} from "@data/constants";

const IndexPage = () => {
  const [apiCallsMade, setApiCallsMade] = useState(false);
  const [_sectionShow, setSectionShow] = useState(false);

  const { isLogin } = useUserInf();
  const width = useWindowSize();
  const isMobile = width < 768;

  const [sliderList, setSliderList] = useState([]);
  const [topCategoryList, setTopCategoryList] = useState([]);
  const [flashDealsList, setFlashDealsList] = useState([]);
  const [topRatedList, setTopRatedList] = useState([]);
  const [newArrivalList, setNewArrivalList] = useState([]);
  const [bigDiscountList, setBigDiscountList] = useState([]);
  const [categoriesList, setCategoriesList] = useState([]);
  const [moreForYouList, setMoreForYouList] = useState([]);
  const [categoryWithProductBrandList, setCategoryWithProductBrandList] =
    useState([]);

  const memoizedApiCallsMade = useMemo(() => apiCallsMade, [apiCallsMade]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const [sliderRes, topCategoryRes, flashDealsRes] = await Promise.all([
          axios.get(Slider_All),
          axios.get(`${Category_Top_All}?page=${1}&size=${100}`),
          axios.get(`${Product_Flash_Deals}?page=${1}&size=${6}`),
        ]);

        const sliderList = sliderRes.data.homepage_sliders.sort(
          (a, b) => a.serial_number - b.serial_number
        );
        const topCategoryList = topCategoryRes.data.categories;
        const flashDealsLists = flashDealsRes.data.products;
        const flashDealsList = await getFormattedProductData(flashDealsLists);

        setSliderList(sliderList);
        setTopCategoryList(topCategoryList);
        setFlashDealsList(flashDealsList);
      } catch (error) {
        console.error("Error fetching data:", error);
        // Handle error for data fetching
      }
    };

    fetchData();
  }, []);

  useEffect(() => {
    const handleScroll = () => {
      if (!memoizedApiCallsMade && window.scrollY > 0) {
        handleScrollToCategoryWithProductBrandList();
        setApiCallsMade(true);
      }
    };

    window.addEventListener("scroll", handleScroll);

    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, [memoizedApiCallsMade]);

  useEffect(() => {
    if (isLogin) {
      axios
        .get(Get_Pending_Order_After_Login, {
          headers: {
            "Content-type": "application/json",
            Authorization: localStorage.getItem("jwt_access_token"),
          },
        })
        .then((res) => {
          if (res.data.id) {
            localStorage.setItem("OrderId", res.data.id);
          } else {
            localStorage.removeItem("OrderId");
          }
        })
        .catch(() => {
          localStorage.removeItem("OrderId");
        });
    }
  }, [isLogin]);

  const handleScrollToCategoryWithProductBrandList = async () => {
    try {
      setSectionShow(true);

      const [
        topRatedRes,
        newArrivalRes,
        categoryWithProductBrandRes,
        bigDiscountRes,
        moreForYouRes,
        categoriesRes,
      ] = await Promise.all([
        axios.get(`${Product_Top_Rated}?page=${1}&size=${4}`),
        axios.get(`${Product_Arrival}?page=${1}&size=${6}`),
        axios.get(`${Category_With_Product_Brand}?page=${1}&size=${10}`),
        axios.get(`${Product_Discount}?page=${1}&size=${6}`),
        axios.get(`${Product_For_You}?page=${1}&size=${24}`),
        axios.get(`${Category_Wth_Name_Img}?page=${1}&size=${12}`),
      ]);

      const topRatedLists = topRatedRes.data.products;
      const topRatedList = await getFormattedProductData(
        topRatedLists,
        "TopRated"
      );

      const newArrivalLists = newArrivalRes.data.products;
      const newArrivalList = await getFormattedProductData(
        newArrivalLists,
        "Arrivals"
      );

      const categoryWithProductBrandList =
        categoryWithProductBrandRes.data.categories_with_products_and_brands;
      const bigDiscountLists = bigDiscountRes.data.products;
      const bigDiscountList = await getFormattedProductData(
        bigDiscountLists,
        "bigdiscount"
      );

      const moreForYouLists = moreForYouRes.data.products;
      const moreForYouList = await getFormattedProductData(moreForYouLists);

      const categoriesList = categoriesRes.data.categories;

      setTopRatedList(topRatedList);
      setNewArrivalList(newArrivalList);
      setCategoryWithProductBrandList(categoryWithProductBrandList);
      setBigDiscountList(bigDiscountList);
      setMoreForYouList(moreForYouList);
      setCategoriesList(categoriesList);
    } catch (error) {
      console.error("Error fetching additional data:", error);
      // Handle error for additional data fetching
    }
  };

  return (
    <main>
      {sliderList?.length !== 0 ? (
        <Section1 sliderList={sliderList} />
      ) : (
        <Container>
          <Skeleton height={isMobile ? 126 : 450} count={1} />
        </Container>
      )}
      {topCategoryList.length !== 0 ? (
        <Section3 topCategoryList={topCategoryList} />
      ) : (
        <Container mt="1rem">
          <Skeleton height={isMobile ? 146 : 150} count={1} />
        </Container>
      )}
      {flashDealsList?.length !== 0 ? (
        <Section2 flashDealsList={flashDealsList} />
      ) : (
        <Container mt="1rem">
          <Skeleton height={342} count={1} />
        </Container>
      )}
      {/* {topRatedList?.length !== 0 ? ( */}
      <Section4 topRatedList={topRatedList} />
      {/* ) : (
        <Container mt="1rem">
          <Skeleton height={342} count={1} />
        </Container>
      )} */}
      {newArrivalList?.length !== 0 ? (
        <Section5 newArrivalList={newArrivalList} />
      ) : (
        <Container mt="1rem">
          <Skeleton height={342} count={1} />
        </Container>
      )}

      <Section13 bigDiscountList={bigDiscountList} />

      <Section7 categoryWithProductBrandList={categoryWithProductBrandList} />
      {moreForYouList?.length !== 0 ? (
        <Section11 moreForYouList={moreForYouList} />
      ) : (
        <Container mt="1rem">
          <Skeleton height={342} count={1} />
        </Container>
      )}
      <Section10 categoriesList={categoriesList} />

      {/* {sectionShow && <Section12 />} */}
      {/* <Chat /> */}
      <WhatsAppWidget
        phoneNo="8801852041302"
        position="right"
        widgetWidth="300px"
        widgetWidthMobile="260px"
        // autoOpen={true}
        // autoOpenTimer={5000}
        messageBox={true}
        messageBoxTxt="Hi Team, is there any related service available ?"
        messageBoxColor="white"
        iconSize="60"
        iconColor="white"
        iconBgColor="#e02043"
        // headerIcon={images[currentImageIndex]}
        // headerIconColor="pink"
        headerTxtColor="black"
        headerBgColor="#f0f2f5"
        headerTitle="Cash Connect"
        headerCaption="Online"
        headerCaptionIcon="/assets/images/gif/online.gif"
        bodyBgColor="#efeae2"
        chatPersonName="Support"
        chatMessage={
          <>
            Hi there 👋 <br />
            <br /> Let us know if we can help you with anything at all.
          </>
        }
        footerBgColor="#f0f2f5"
        placeholder="Type a message.."
        btnBgColor="white"
        btnTxt="Start Chat"
        btnTxtColor="black"
      />
    </main>
  );
};

IndexPage.layout = AppLayout;

export default IndexPage;
