import AddressEditor from "@component/address/AddressEditor";
import DashboardLayout from "@component/layout/CustomerDashboardLayout";
import React from "react";

const AddressUpdater = () => {
  return <AddressEditor />;
};

AddressUpdater.layout = DashboardLayout;

export default AddressUpdater;

export async function getServerSideProps() {
  // This is an empty async function to satisfy the requirement of getServerSideProps
  return { props: {} };
}
