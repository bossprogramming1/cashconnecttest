import Currency from "@component/Currency";
import LazyImage from "@component/LazyImage";
import LoginPopup from "@component/LoginPopup";
import TextArea from "@component/textarea/TextArea";
import { useAppContext } from "@context/app/AppContext";
import useUserInf from "@customHook/useUserInf";
import {
  Customer_Order_Comment,
  Customer_Order_Pending_Details,
} from "@data/constants";
import axios from "axios";
import { useFormik } from "formik";
import { useRouter } from "next/router";
import React, { Fragment, useEffect, useState } from "react";
import Box from "../components/Box";
import Button from "../components/buttons/Button";
import { Card1 } from "../components/Card1";
import Divider from "../components/Divider";
import FlexBox from "../components/FlexBox";
import Grid from "../components/grid/Grid";
import Image from "@component/Image";
import CheckoutNavLayout from "../components/layout/CheckoutNavLayout";
import ProductCard7 from "../components/product-cards/ProductCard7";
import Modal from "@component/modal/Modal";
import Card from "@component/Card";
import Typography, { H2, Paragraph } from "../components/Typography";
import useWindowSize from "@hook/useWindowSize";
import Icon from "@component/icon/Icon";

type CartItem = {
  id: any;
  quantity: any;
  price: any;
  product: any;
  condition: string;
  size: any;
  color_image: any;
  color: any;
};

const Cart = () => {
  const [cartProductList, setCartProductList] = useState<CartItem[]>([]);
  const [_reloadCart, setReloadCart] = useState(0);
  const [openLogin, setOpenLogin] = useState(false);
  const { state, dispatch } = useAppContext();
  const cartCanged = state.cart.chartQuantity;
  const [open, setOpen] = useState(false);

  const width = useWindowSize();
  const isMobile = width < 768;
  const router = useRouter();

  const { order_Id, authTOKEN, isLogin } = useUserInf();

  const openModal = () => {
    setOpen(true);
  };

  const closeLoginTab = () => {
    setOpenLogin(false);
  };

  const getTotalPrice = () => {
    return (
      cartProductList.reduce(
        (accumulator, item) => accumulator + item.price * item.quantity,
        0
      ) || 0
    );
  };

  const addComment = () => {
    if (isLogin) {
      if (order_Id) {
        const comment = {
          comment: values.comment,
        };

        axios
          .post(`${Customer_Order_Comment}${order_Id}`, comment, authTOKEN)
          .then(() => {
            router.push("/checkout");
          })
          .catch(() => {});
      }
    } else {
      setOpenLogin(true);
    }
  };

  useEffect(() => {
    const OrderId = localStorage.getItem("OrderId");
    if (OrderId) {
      axios
        .get(`${Customer_Order_Pending_Details}${OrderId}`, {
          headers: {
            "Content-type": "application/json",
            Authorization: localStorage.getItem("jwt_access_token"),
          },
        })
        .then((res) => {
          setCartProductList(
            res.data.order?.order_items.length !== 0
              ? res.data.order?.order_items
              : []
          );
          setFieldValue("comment", res.data.order?.comment);
        })
        .catch(() => {
          dispatch({
            type: "CHANGE_CART_QUANTITY",
            payload: {
              chartQuantity: 0,
            },
          });
          setCartProductList([]);
        });
    }
  }, [cartCanged]);

  // useEffect(() => {
  //   if (order_Id) {
  //     axios
  //       .get(`${Customer_Order_Pending_Details}${order_Id}`, {
  //         headers: {
  //           "Content-type": "application/json",
  //           Authorization: localStorage.getItem("jwt_access_token"),
  //         },
  //       })
  //       .then((res) => {
  //
  //         setCartProductList(
  //           res.data.order?.order_items.length !== 0
  //             ? res.data.order?.order_items
  //             : []
  //         );
  //         setFieldValue("comment", res.data.order?.comment);
  //       })
  //       .catch(() => {
  //
  //       });
  //   }
  // }, [cartCanged]);

  const runReloadCart = () => {
    setReloadCart(Math.random());

    axios
      .get(`${Customer_Order_Pending_Details}${order_Id}`, {
        headers: {
          "Content-type": "application/json",
          Authorization: localStorage.getItem("jwt_access_token"),
        },
      })
      .then((res) => {
        setCartProductList(
          res.data.order?.order_items.length !== 0
            ? res.data.order?.order_items
            : []
        );
        setFieldValue("comment", res.data.order?.comment);
      })
      .catch(() => {
        setCartProductList([]);
      });
  };
  // const removeCart = () => {
  //   setCartProductList([]);
  //   dispatch({
  //     type: "CHANGE_CART_QUANTITY",
  //     payload: {
  //       chartQuantity: 0,
  //     },
  //   });
  // };

  const initialValues = {
    comment: "",
  };
  const checkoutSchema = null;
  const handleFormSubmit = () => {};

  const {
    values,
    errors,
    touched,
    handleChange,
    handleBlur,
    // handleSubmit,
    setFieldValue,
  } = useFormik({
    initialValues: initialValues,
    validationSchema: checkoutSchema,
    onSubmit: handleFormSubmit,
  });

  return (
    <>
      {cartProductList.length !== 0 ? (
        <Fragment>
          <LoginPopup open={openLogin} closeLoginDialog={closeLoginTab} />

          <Grid container spacing={6}>
            <Grid item lg={8} md={8} xs={12}>
              {cartProductList.map((item) => (
                <ProductCard7
                  key={item.id}
                  mb="1.5rem"
                  // removeCart={removeCart}
                  runReloadCart={runReloadCart}
                  openModal={openModal}
                  {...item}
                />
              ))}
            </Grid>
            <Grid item lg={4} md={4} xs={12}>
              <Card1>
                <FlexBox
                  justifyContent="space-between"
                  alignItems="center"
                  mb="1rem"
                >
                  <Typography color="gray.600">Total:</Typography>
                  <FlexBox alignItems="flex-end">
                    <Typography fontSize="18px" fontWeight="600" lineHeight="1">
                      <Currency>{getTotalPrice().toFixed(2)}</Currency>
                    </Typography>
                    {/* <Typography fontWeight="600" fontSize="14px" lineHeight="1">
                  00
                </Typography> */}
                  </FlexBox>
                </FlexBox>

                <Divider mb="1rem" />

                <FlexBox alignItems="center" mb="1rem">
                  <Typography fontWeight="600" mr="10px">
                    Additional Comments
                  </Typography>
                  <Box p="3px 10px" bg="primary.light" borderRadius="3px">
                    <Typography fontSize="12px" color="primary.main">
                      Note
                    </Typography>
                  </Box>
                </FlexBox>

                <TextArea
                  rows={6}
                  name="comment"
                  onBlur={handleBlur}
                  onChange={handleChange}
                  value={values.comment || ""}
                  errorText={touched.comment && errors.comment}
                  fullwidth
                  mb="1rem"
                />

                <Divider mb="1rem" />

                <Button
                  variant="contained"
                  color="primary"
                  onClick={addComment}
                  fullwidth
                >
                  Checkout Now
                </Button>
              </Card1>
            </Grid>
          </Grid>
        </Fragment>
      ) : (
        <FlexBox
          flexDirection="column"
          alignItems="center"
          justifyContent="center"
          height="calc(100% - 100px)"
          mb="260px"
          mt="260px"
        >
          <LazyImage
            src="/assets/images/logos/shopping-bag.svg"
            width="90px"
            height="100%"
          />
          <Paragraph
            mt="1rem"
            color="text.muted"
            textAlign="center"
            maxWidth="200px"
          >
            Your shopping bag is empty. Start shopping
          </Paragraph>
        </FlexBox>
      )}
      <Modal open={open} onClose={() => setOpen(false)}>
        <Card
          p="1rem"
          position="relative"
          width={isMobile ? "73%" : "fit-content"}
        >
          <>
            <Image
              key={open ? "gif-open" : "gif-closed"}
              style={{
                height: "auto",
                width: "150px", // Make the width 100% for responsiveness
                marginLeft: "auto",
                marginRight: "auto",
                marginTop: "-30px",
              }}
              src={`/assets/images/gif/done.gif`}
              onLoad={() =>
                setTimeout(() => {
                  setOpen(false);
                }, 2000)
              }
            />
            <H2
              mt="-30px"
              fontSize={isMobile ? "20px" : "25px"}
              textAlign="center"
            >
              Item has been successfully removed
            </H2>
          </>

          <Box
            position="absolute"
            top="0.75rem"
            right="0.75rem"
            cursor="pointer"
          >
            <Icon
              className="close"
              color="primary"
              variant="small"
              onClick={() => setOpen(false)}
            >
              close
            </Icon>
          </Box>
        </Card>
      </Modal>
    </>
  );
};

Cart.layout = CheckoutNavLayout;

export default Cart;

export async function getServerSideProps() {
  // This is an empty async function to satisfy the requirement of getServerSideProps
  return { props: {} };
}
