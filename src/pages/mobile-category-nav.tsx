import Accordion from "@component/accordion/Accordion";
import AccordionHeader from "@component/accordion/AccordionHeader";
import Box from "@component/Box";
import Grid from "@component/grid/Grid";
import Header from "@component/header/Header";
import Icon from "@component/icon/Icon";
import MobileCategoryImageBox from "@component/mobile-category-nav/MobileCategoryImageBox";
import { MobileCategoryNavStyle } from "@component/mobile-category-nav/MobileCategoryNavStyle";
import MobileNavigationBar from "@component/mobile-navigation/MobileNavigationBar";
import Typography, { Paragraph } from "@component/Typography";
import useFormattedCategoryData from "@customHook/useFormattedCategoryData";
import {
  BASE_URL,
  Category_All_With_Child,
  Category_Top_All,
} from "@data/constants";
import axios from "axios";
import _ from "lodash";
import Link from "next/link";
import React, { Fragment, useEffect, useState } from "react";

const MobileCategoryNav = () => {
  const [category, setCategory] = useState(null);
  const [suggestedList, setSuggestedList] = useState([]);
  const [subCategoryList, setSubCategoryList] = useState([]);
  const [selectedTopCategoy, setSelectedTopCategoy] = useState(false);
  const [formattedCategoryData, setFormattedCategoryData] =
    useFormattedCategoryData();
  const [selectedItem, setSelectedItem] = useState(null);

  const handleItemClick = (item) => {
    setSelectedItem(item);
  };

  useEffect(() => {
    fetch(`${Category_All_With_Child}`)
      .then((res) => res.json())
      .then((data) => {
        const modifyData = [...data.categories];
        setFormattedCategoryData(modifyData);
        setSelectedTopCategoy(true);
      })
      .catch(() => {});
  }, []);

  const handleCategoryClick = (cat) => () => {
    let menuData = cat.menuData;
    if (menuData) {
      setSubCategoryList(menuData.categories || menuData);
      setSelectedTopCategoy(false);
    } else setSubCategoryList([]);
    setCategory(cat);
  };
  const handleTopCategoryClick = () => () => {
    setSelectedTopCategoy(true);
    setSubCategoryList([]);
    // let menuData = cat.menuData;
    // if (menuData) {
    //   setSubCategoryList(menuData.categories || menuData);
    // } else setSubCategoryList([]);
    // setCategory(cat);
  };

  useEffect(() => {
    axios
      .get(`${Category_Top_All}?page=${1}&size=${600}`)
      .then((res) => {
        setSuggestedList(res.data?.categories);
      })
      .catch(() => {});
  }, []);

  return (
    <MobileCategoryNavStyle>
      <Header className="header" />
      <div className="main-category-holder">
        <Box
          className="top-category"
          style={{
            backgroundColor: selectedTopCategoy ? "white" : "#DAE1E7",
            color: selectedTopCategoy ? "#e02043" : "#2B3445",
            borderTop: selectedTopCategoy ? "2px solid #DAE1E7" : "0",
          }}
          onClick={handleTopCategoryClick()}
        >
          <Typography
            className=""
            textAlign="left"
            fontSize="11px"
            lineHeight="1"
          >
            Top Categories
          </Typography>
        </Box>
        {formattedCategoryData.map((item) => (
          <Box
            className="main-category-box"
            style={{
              backgroundColor:
                category?.href === item.href && !selectedTopCategoy
                  ? "white"
                  : "#DAE1E7",
              color:
                category?.href === item.href && !selectedTopCategoy
                  ? "#e02043"
                  : "#2B3445",
            }}
            // borderLeft={`${
            //   category?.href === item.href && !selectedTopCategoy ? "3" : "0"
            // }px solid`}
            onClick={handleCategoryClick(item)}
            key={item?.id || item.title}
          >
            <Icon size="28px" mb="0.5rem" src={item.icon}></Icon>
            <Typography
              className=""
              textAlign="center"
              fontSize="11px"
              lineHeight="1"
            >
              {item.title}
            </Typography>
          </Box>
        ))}
      </div>
      <Box className="container">
        {selectedTopCategoy ? (
          <>
            <Typography fontWeight="600" fontSize="15px" mb="1rem">
              Recommended Categories
            </Typography>
            <Box mb="2rem">
              <Grid container spacing={3}>
                {suggestedList.map((item, ind) => (
                  <Grid item lg={5} md={5} sm={4} xs={3} key={item?.id || ind}>
                    <Link
                      href={`/product/search/product_by_category?categoryId=${item.id}`}
                      as={`/product/search/product_by_category?categoryId=${item.id}`}
                    >
                      <a>
                        <MobileCategoryImageBox
                          title={item?.name}
                          imgUrl={`${BASE_URL}${item?.image}`}
                        />
                      </a>
                    </Link>
                  </Grid>
                ))}
              </Grid>
            </Box>
          </>
        ) : (
          ""
        )}

        {category?.menuComponent === "MegaMenu1" ? (
          subCategoryList.map((item, ind) => {
            return !_.isEmpty(item.subCategories) ? (
              <Fragment key={item?.id || ind}>
                <Accordion>
                  <AccordionHeader px="0px" py="10px">
                    <Typography
                      style={{ color: "#2B3445" }}
                      fontWeight="600"
                      fontSize="15px"
                    >
                      {item.title}
                    </Typography>
                  </AccordionHeader>
                  <Box mb="0.5rem" mt="0.5rem">
                    <Grid container spacing={3}>
                      {item.subCategories?.map((item, ind) => (
                        <Grid
                          item
                          lg={12}
                          md={12}
                          sm={12}
                          xs={12}
                          key={item?.id || ind}
                        >
                          <div onClick={() => handleItemClick(item)}>
                            <Link href={item.href} as={item.href}>
                              <a>
                                <Typography
                                  style={{
                                    color:
                                      selectedItem === item
                                        ? "#e02043"
                                        : "#2B3445",
                                  }}
                                  fontWeight="500"
                                  fontSize="14px"
                                  // className="ellipsis"
                                  textAlign="left"
                                  // mt="0.5rem"
                                  ml="1rem"
                                >
                                  {item?.title}
                                </Typography>
                                {/* <MobileCategoryImageBox {...item} /> */}
                              </a>
                            </Link>
                          </div>
                        </Grid>
                      ))}
                    </Grid>
                  </Box>
                </Accordion>
              </Fragment>
            ) : (
              <Link href={item.href} key={item?.id || ind} as={item.href}>
                <Paragraph
                  className="cursor-pointer"
                  fontSize="14px"
                  fontWeight="600"
                  py="6px"
                >
                  {item.title}
                </Paragraph>
              </Link>
            );
          })
        ) : (
          <Box mb="0.5rem">
            <Grid container spacing={3}>
              {subCategoryList.map((item, ind) => (
                <Grid
                  item
                  lg={12}
                  md={12}
                  sm={12}
                  xs={12}
                  key={item?.id || ind}
                >
                  <Link href={item?.href} as={item?.href}>
                    <a>
                      {/* <MobileCategoryImageBox {...item} /> */}

                      <Typography
                        style={{ color: "#2B3445" }}
                        fontWeight="500"
                        fontSize="13px"
                        // className="ellipsis"
                        textAlign="left"
                        // mt="0.5rem"
                      >
                        {item?.title}
                      </Typography>
                    </a>
                  </Link>
                </Grid>
              ))}
            </Grid>
          </Box>
        )}
      </Box>
      <MobileNavigationBar />
    </MobileCategoryNavStyle>
  );
};

export default MobileCategoryNav;

export async function getServerSideProps() {
  // This is an empty async function to satisfy the requirement of getServerSideProps
  return { props: {} };
}
