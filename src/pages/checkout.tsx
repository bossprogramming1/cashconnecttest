import useUserInf from "@customHook/useUserInf";
import { Customer_Order_Pending_Details } from "@data/constants";
import axios from "axios";
import React, { useEffect, useState } from "react";
import CheckoutForm from "../components/checkout/CheckoutForm";
import CheckoutSummary from "../components/checkout/CheckoutSummary";
import Grid from "../components/grid/Grid";
import CheckoutNavLayout from "../components/layout/CheckoutNavLayout";
// import useWindowSize from "@hook/useWindowSize";
import Link from "next/link";
import Button from "@component/buttons/Button";

const Checkout = () => {
  const [subtotal, setSubtotal] = useState(null);
  const [shippingPrice, setShippingPrice] = useState(null);
  const [taxPrice, setTaxPrice] = useState(null);
  const [discountAmount, setDiscountAmount] = useState(null);
  const [submitForm, setFormSubmit] = useState(false);
  const [confurmSubmitForm, setComfirmFormSubmit] = useState(false);
  const [commentText, setCommentText] = useState("");

  const { order_Id } = useUserInf();
  // const width = useWindowSize();
  // const isMobile = width < 900;

  useEffect(() => {
    if (order_Id) {
      axios
        .get(`${Customer_Order_Pending_Details}${order_Id}`, {
          headers: {
            "Content-type": "application/json",
            Authorization: localStorage.getItem("jwt_access_token"),
          },
        })
        .then((res) => {
          const { order } = res.data;
          setSubtotal(order.net_amount);
          setShippingPrice(order.shipping_price);
          setTaxPrice(order.tax_price);
          setDiscountAmount(order.discount_amount);
          setCommentText(order?.comment || "");
        })
        .catch(() => {});
    }
  }, [order_Id]);
  const handleEnableSubmit = (value) => {
    setFormSubmit(value);
  };

  return (
    <>
      <Grid container spacing={6}>
        <Grid item lg={8} md={12} xs={12}>
          <CheckoutForm
            handleEnableSubmit={handleEnableSubmit}
            confurmSubmitForm={confurmSubmitForm}
          />
        </Grid>
        <Grid item lg={4} md={12} xs={12}>
          <CheckoutSummary
            Subtotal={subtotal}
            Shipping={shippingPrice}
            Tax={taxPrice}
            Discount={discountAmount}
            commentText={commentText}
            confurmSubmitForm={confurmSubmitForm}
          />
        </Grid>
      </Grid>
      <Grid container spacing={7}>
        <Grid item sm={6} xs={12}>
          <Link href="/cart" as="/cart">
            <Button variant="outlined" color="primary" type="button" fullwidth>
              Back to Cart
            </Button>
          </Link>
        </Grid>
        <Grid item sm={6} xs={12}>
          <Button
            variant="contained"
            color="primary"
            disabled={!submitForm}
            style={{ cursor: submitForm ? "pointer" : "no-drop" }}
            fullwidth
            onClick={() => {
              setComfirmFormSubmit(true);
            }}
          >
            Place Order
          </Button>
        </Grid>
      </Grid>
    </>
  );
};

Checkout.layout = CheckoutNavLayout;

export default Checkout;

export async function getServerSideProps() {
  // This is an empty async function to satisfy the requirement of getServerSideProps
  return { props: {} };
}
