import Avatar from "@component/avatar/Avatar";
import Box from "@component/Box";
// import Button from "@component/buttons/Button";
import Currency from "@component/Currency";
import FlexBox from "@component/FlexBox";
import Grid from "@component/grid/Grid";
import Typography, { H6 } from "@component/Typography";
import { BASE_URL } from "@data/constants";
import { useRouter } from "next/router";
import React from "react";

function Item({ item }) {
  const router = useRouter();

  // useEffect(() => {
  //   // axios.get(`${Product_Color_By_Product_Id}${item?.product?.id}`).then(res => {
  //   //
  //   //     setColor(res?.data?.name)
  //   // }).catch(() => {
  //   // axios.get(`${Product_Size_By_Product_Id}${item?.product?.id}`).then(res => {
  //   //
  //   //     setSize(res?.data?.name)
  //   // }).catch(() => {
  // }, []);

  return (
    <Grid container spacing={6} key={item?.product?.id}>
      <Grid item lg={4} md={12} xs={12}>
        <FlexBox alignItems="center">
          <Avatar src={`${BASE_URL}${item?.product?.thumbnail}`} size={64} />
          <Box ml="20px">
            <H6 my="0px">{item?.product?.name}</H6>
          </Box>
        </FlexBox>
      </Grid>
      <Grid display="flex" item lg={4} md={6} xs={6}>
        <FlexBox alignItems="center">
          <Typography fontSize="14px" color="text.muted" display="flex">
            <Currency>{item?.price}</Currency> x {item?.quantity}
          </Typography>
          <Typography fontSize="14px" color="text.muted">
            {`${item?.color?.name ? item?.color?.name : ""}  ${
              item?.size?.name ? `[${item?.size?.name}]` : ""
            }`}
          </Typography>
        </FlexBox>
      </Grid>
      <Grid item lg={4} md={6} xs={6}>
        <div
          // alignItems="center"
          onClick={() => {
            router.push({
              pathname: `/product/${item?.product?.id}`,
              query: { review: "write" },
            });
          }}
        >
          <Typography
            variant="text"
            color="primary"
            style={{
              whiteSpace: "nowrap",
              color: "#e02043",
              fontWeight: 700,
              cursor: "pointer",
            }}
            fontSize="14px"
          >
            Write a Review
          </Typography>
        </div>
      </Grid>
    </Grid>
  );
}

export default Item;

export async function getServerSideProps() {
  // This is an empty async function to satisfy the requirement of getServerSideProps
  return { props: {} };
}
