import Avatar from "@component/avatar/Avatar";
import Box from "@component/Box";
import Button from "@component/buttons/Button";
import Capcha from "@component/Capcha";
import Card from "@component/Card";
import Grid from "@component/grid/Grid";
import Icon from "@component/icon/Icon";
import Image from "@component/Image";
import CustomerDashboardLayout from "@component/layout/CustomerDashboardLayout";
import DashboardPageHeader from "@component/layout/DashboardPageHeader";
import Select from "@component/Select";
import TextField from "@component/text-field/TextField";
import TextArea from "@component/textarea/TextArea";
import Typography from "@component/Typography";
import { useAppContext } from "@context/app/AppContext";
import useUserInf from "@customHook/useUserInf";
import {
  Customer_By_Id,
  Ticket_Create,
  Ticket_Department_All,
  Ticket_Priority_All,
} from "@data/constants";
import { allowedExtensions, ticketfileExtension } from "@data/data";
import axios from "axios";
import { useFormik } from "formik";
import jsonToFormData from "helper/jsonToFormData";
import Link from "next/link";
import { useRouter } from "next/router";
import React, { useEffect, useRef, useState } from "react";
import * as yup from "yup";

function OpenTicket() {
  const [previewImage, setPreviewImage] = useState([]);
  const [images, setImages] = useState([]);
  const [departmets, setDepartmets] = useState([]);
  const [priorities, setPriorities] = useState([]);
  const [isValidCapha, setIsValidCapha] = useState(false);
  const [loading, setLoading] = useState(false);
  const [errorText, setErrorText] = useState("");
  const [_reRender, setReRender] = useState(0);

  const { user_id, authTOKEN } = useUserInf();
  const inputFileRef = useRef(null);

  const { dispatch } = useAppContext();
  const router = useRouter();

  function capitalizeWords(arr) {
    return arr.map((word) => {
      const firstLetter = word.name.charAt(0).toUpperCase();
      const rest = word.name.slice(1).toLowerCase();

      return { name: firstLetter + rest, id: word.id };
    });
  }
  const handleFormSubmit = async (values) => {
    const invalidFiles = images.filter((file) => {
      const extension = file.name.split(".").pop();
      return !allowedExtensions.includes(`.${extension}`);
    });

    if (invalidFiles.length > 0) {
      setErrorText(
        `Invalid file(s): ${invalidFiles.map((file) => file.name).join(", ")}`
      );
      return;
    }
    if (isValidCapha) {
      const data = {
        ...values,
        ticket_department:
          typeof values.ticket_department != "object"
            ? values?.ticket_department
            : values?.ticket_department?.id,
        ticket_priority:
          typeof values.ticket_priority != "object"
            ? values?.ticket_priority
            : values?.ticket_priority?.id,
        // user: user_id,
        file: images,
      };
      const [ticketFormData] = jsonToFormData(data);

      setLoading(true);

      axios
        .post(`${Ticket_Create}`, ticketFormData, authTOKEN)
        .then((res) => {
          setLoading(false);
          setPreviewImage([]);
          setImages([]);
          if (inputFileRef.current) {
            inputFileRef.current.value = "";
          }
          if (res?.status === 201) {
            dispatch({
              type: "CHANGE_ALERT",
              payload: {
                alertValue: "success...",
              },
            });
            router.push("/support-tickets");
          } else {
            dispatch({
              type: "CHANGE_ALERT",
              payload: {
                alertValue: "something went wrong",
                alerType: "error",
              },
            });
          }
        })
        .catch(() => {
          setLoading(false);
          dispatch({
            type: "CHANGE_ALERT",
            payload: {
              alertValue: "something went wrong",
              alerType: "error",
            },
          });
        });
    }
  };

  const cancelAImage = (imgId) => {
    let newPreImgs = [...previewImage];
    newPreImgs.splice(imgId, 1);
    setPreviewImage(newPreImgs);

    let newImages = [...images];
    newImages.splice(imgId, 1);
    setImages(newImages);

    setReRender(Math.random());
  };

  useEffect(() => {
    setTimeout(() => {
      setErrorText("");
    }, 5000);
  }, [errorText]);

  useEffect(() => {
    if (user_id) {
      axios
        .get(`${Customer_By_Id}${user_id}`)
        .then((res) => {
          setFieldValue(
            "name",
            `${res?.data?.first_name}${" "}${res?.data?.last_name}`
          );
          setFieldValue("email", res?.data?.email);
        })
        .catch(() => {});

      axios
        .get(`${Ticket_Department_All}`)
        .then((res) => {
          setDepartmets(capitalizeWords(res?.data?.ticket_departments));
        })
        .catch(() => {});

      axios
        .get(`${Ticket_Priority_All}`)
        .then((res) => {
          setPriorities(capitalizeWords(res?.data?.ticket_priorities));
        })
        .catch(() => {});
    }
  }, [user_id]);

  const {
    values,
    errors,
    touched,
    handleChange,
    handleBlur,
    handleSubmit,
    setFieldValue,
  } = useFormik({
    initialValues: initialValues,
    validationSchema: checkoutSchema,
    onSubmit: handleFormSubmit,
  });

  return (
    <>
      {loading && (
        <div
          style={{
            position: "fixed",
            height: "100%",
            width: "100%",
            top: "0px",
            left: "0px",
            display: "flex",
            justifyContent: "center",
            backgroundColor: " rgb(0 0 0 / 50%)",
            alignItems: "center",
            zIndex: 100,
          }}
        >
          <Image
            style={{
              height: "50px",
              width: "50px",
              marginTop: "100pz",
            }}
            src="/assets/images/gif/loading.gif"
          />
        </div>
      )}
      <DashboardPageHeader
        title="Open Ticket"
        iconName="support"
        button={
          <Link href="/support-tickets" as="/support-tickets">
            <a>
              <Button color="primary" bg="primary.light" px="2rem">
                Back To Support Ticket List
              </Button>
            </a>
          </Link>
        }
      />
      <Card p="30px">
        <form onSubmit={handleSubmit}>
          <Grid container spacing={6}>
            <Grid item sm={6} xs={12}>
              <TextField
                name="name"
                label="Name"
                placeholder="Name"
                fullwidth
                readOnly
                onBlur={handleBlur}
                onChange={handleChange}
                value={values.name || ""}
                errorText={touched.name && errors.name}
                style={{ cursor: "no-drop" }}
              />
            </Grid>
            <Grid item sm={6} xs={12}>
              <TextField
                name="email"
                label="Email Address"
                placeholder="Email"
                fullwidth
                readOnly
                onBlur={handleBlur}
                onChange={handleChange}
                value={values.email || ""}
                errorText={touched.email && errors.email}
                style={{ cursor: "no-drop" }}
              />
            </Grid>

            <Grid item xs={12}>
              <TextField
                name="subject"
                label="Subject"
                placeholder="Subject"
                fullwidth
                onBlur={handleBlur}
                onChange={handleChange}
                value={values.subject || ""}
                errorText={touched.subject && errors.subject}
              />
            </Grid>

            <Grid item sm={6} xs={12}>
              <Select
                label="Department"
                placeholder="Select Department"
                options={departmets}
                value={values.ticket_department || ""}
                onChange={(ticket_department) => {
                  setFieldValue("ticket_department", ticket_department);
                }}
                errorText={
                  touched.ticket_department && errors.ticket_department
                }
              />
            </Grid>
            <Grid item sm={6} xs={12}>
              <Select
                label="Priority"
                placeholder="Select Priority"
                options={priorities}
                value={values.ticket_priority || ""}
                onChange={(ticket_priority) => {
                  setFieldValue("ticket_priority", ticket_priority);
                }}
                errorText={touched.ticket_priority && errors.ticket_priority}
              />
            </Grid>

            <Grid item xs={12}>
              <TextArea
                name="message"
                label="Message"
                placeholder="Message"
                rows={6}
                fullwidth
                onBlur={handleBlur}
                onChange={handleChange}
                value={values.message || ""}
                errorText={touched.message && errors.message}
              />
            </Grid>

            <Grid item xs={12}>
              <Box
                display="flex"
                flexDirection="column"
                justifyContent="center"
                alignItems="flex-start"
                minHeight="150px"
                paddingLeft="20px"
                border="1px dashed"
                borderColor="gray.400"
                borderRadius="10px"
                transition="all 250ms ease-in-out"
                style={{ outline: "none" }}
              >
                <Typography
                  color="gray.700"
                  fontSize="20px"
                  fontWeight="bold"
                  mb="15px"
                  mt="15px"
                >
                  Attachment
                </Typography>

                <input
                  multiple
                  onChange={async (e) => {
                    const reader: any = new FileReader();
                    reader.onload = () => {
                      if (reader.readyState === 2) {
                        let newImg = [...previewImage];

                        newImg.push(reader.result);
                        setPreviewImage(newImg);
                      }
                    };
                    reader.readAsDataURL(e.target.files[0]);

                    const file = e.target.files[0];
                    let newImgFile = [...images];

                    newImgFile.push(file);

                    setImages(newImgFile);
                    // onChange(file);
                  }}
                  ref={inputFileRef}
                  id="profile-image"
                  accept={ticketfileExtension}
                  type="file"
                />

                {errorText && (
                  <Typography mt="5px" mb="5px" color="error.main">
                    {errorText}
                  </Typography>
                )}

                <Typography mt="5px" mb="5px">
                  {`(Allowed File Extensions: ${ticketfileExtension.replace(
                    /\./g,
                    ""
                  )}`}
                </Typography>
                <Box
                  width="80%"
                  mt="25px"
                  display="flex"
                  style={{ gap: "20px" }}
                  justifyContent="center"
                  flexWrap="wrap"
                >
                  {previewImage?.map((src, id) => {
                    // Get the file extension
                    const extension = images[id]?.name
                      ?.split(".")
                      ?.pop()
                      ?.toLowerCase();

                    // Define the icons for different file types
                    let icon = null;
                    switch (extension) {
                      case "pdf":
                        icon = "pdf-file";
                        break;
                      case "doc":
                      case "docx":
                        icon = "word-file";
                        break;
                      case "txt":
                        icon = "txt-file";
                        break;
                      case "xls":
                      case "xlsx":
                        icon = "xls-file";
                        break;
                      // Add more cases for other file types
                      default:
                        icon = "word-file";
                        break;
                    }

                    return (
                      <>
                        <Box
                          display="flex"
                          width="fit-content"
                          position="relative"
                        >
                          <div
                            id="cancelIcon"
                            style={{
                              position: "absolute",
                              top: "-10px",
                              right: "-10px",
                              zIndex: 1,
                              color: "red",
                            }}
                          >
                            <Icon
                              onClick={() => {
                                cancelAImage(id);
                              }}
                            >
                              cancel
                            </Icon>
                          </div>

                          {extension === "pdf" ||
                          extension === "doc" ||
                          extension === "docx" ||
                          extension === "txt" ||
                          extension === "xls" ||
                          extension === "xlsx" ? (
                            <Icon
                              variant="extralarge"
                              style={{
                                cursor: "pointer",
                              }}
                            >
                              {icon}
                            </Icon>
                          ) : (
                            <Avatar
                              float="left"
                              radius={10}
                              src={src}
                              size={50}
                            />
                          )}
                        </Box>
                      </>
                    );
                  })}
                </Box>
              </Box>
            </Grid>

            <Grid item xs={12}>
              <Box
                display="flex"
                flexDirection="column"
                justifyContent="center"
                alignItems="flex-start"
                minHeight="150px"
                paddingLeft="20px"
                paddingBottom="10px"
                border="1px dashed"
                borderColor="gray.400"
                borderRadius="10px"
                transition="all 250ms ease-in-out"
                style={{ outline: "none" }}
              >
                <Typography
                  color="gray.700"
                  fontSize="25px"
                  fontWeight="bold"
                  mb="15px"
                  mt="10px"
                >
                  Spam Bot Varification
                </Typography>

                <Typography mb="15px">
                  Please enter the character you see in the image below into the
                  text box provider. This is required to prevent automatic
                  submissions.
                </Typography>

                <Capcha setValid={setIsValidCapha} />
              </Box>
            </Grid>
          </Grid>
          <Button mt="25px" variant="contained" color="primary" type="submit">
            Submit
          </Button>
        </form>
      </Card>
    </>
  );
}

const initialValues = {
  name: "",
  email: "",
  subject: "",
  ticket_department: "",
  ticket_priority: "",
  message: "",
};

const checkoutSchema = yup.object().shape({
  name: yup.string().required("required"),
  // email: yup.string().required("required"),
  subject: yup.string().required("required"),
  ticket_department: yup.object().required("required"),
  ticket_priority: yup.object().required("required"),
  message: yup.string().required("required"),
});

OpenTicket.layout = CustomerDashboardLayout;

export default OpenTicket;

export async function getServerSideProps() {
  // This is an empty async function to satisfy the requirement of getServerSideProps
  return { props: {} };
}
